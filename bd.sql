-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-12-2019 a las 04:20:21
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_agente` int(11) NOT NULL,
  `title` text NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `estado` varchar(34) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`id`, `id_cliente`, `id_agente`, `title`, `start`, `end`, `estado`) VALUES
(1, 1, 10, 'gfdgd', '2019-12-17 03:33:00', '2019-12-17 18:06:00', 'cxzcz'),
(2, 3, 10, 'CITA', '2019-12-18 14:01:00', '2019-12-18 14:00:00', ''),
(3, 4, 10, 'esfsdrfsdfsvr astvr tdtasa', '2019-12-18 03:33:00', '2019-12-18 04:44:00', ''),
(4, 1, 10, 'cvvxxv', '2019-12-20 13:00:00', '2019-12-20 14:00:00', ''),
(5, 1, 10, 'dffdg', '2019-12-20 16:44:00', '2019-12-20 17:55:00', ''),
(6, 1, 10, 'fssdfsdff', '2019-12-27 03:33:00', '2019-12-27 16:44:00', ''),
(7, 5, 10, 'dsfdsf', '2019-12-26 16:04:00', '2019-12-26 19:07:00', ''),
(8, 29, 10, 'eee', '2019-12-20 15:02:00', '2019-12-20 16:00:00', ''),
(9, 30, 10, 'lkihjh', '2019-12-14 14:01:00', '2019-12-14 15:00:00', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crm_clientes`
--

CREATE TABLE `crm_clientes` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` int(11) NOT NULL,
  `telefono` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_registro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `crm_clientes`
--

INSERT INTO `crm_clientes` (`id`, `nombre`, `dni`, `telefono`, `email`, `direccion`, `fecha_registro`) VALUES
(1, 'eduardo', 2311231, '1321313', 'codepascasio@gmail.com', 'mmnbbhj', '2019-12-20 04:02:35'),
(2, 'eduardo', 86868, '988979798', 'code@gmail.com', 'kjhj', '2019-12-20 04:14:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_registro`
--

CREATE TABLE `email_registro` (
  `id` int(11) NOT NULL,
  `id_cliente` int(10) NOT NULL,
  `id_agente` int(10) NOT NULL,
  `asunto` text NOT NULL,
  `contenido` text NOT NULL,
  `id_envio` int(11) NOT NULL,
  `fecha_envio` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `email_registro`
--

INSERT INTO `email_registro` (`id`, `id_cliente`, `id_agente`, `asunto`, `contenido`, `id_envio`, `fecha_envio`) VALUES
(1, 1, 10, 'dsda', 'uuyguyg', 0, '2019-12-04 09:03:25'),
(2, 1, 10, 'gcgfgd', 'gfgfdgfd', 0, '2019-12-10 10:44:01'),
(3, 2, 10, 'dsdsdas', 'dasdasdas', 1, '2019-12-10 11:07:50'),
(4, 1, 10, 'dsdsdas', 'dasdasdas', 1, '2019-12-10 11:07:50'),
(5, 4, 10, 'fsdfs', 'fsdfsfsd', 8, '2019-12-14 09:23:45'),
(6, 3, 10, 'fsdfs', 'fsdfsfsd', 8, '2019-12-14 09:23:45'),
(7, 2, 10, 'fsdfs', 'fsdfsfsd', 8, '2019-12-14 09:23:45'),
(8, 1, 10, 'fsdfs', 'fsdfsfsd', 8, '2019-12-14 09:23:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_cita`
--

CREATE TABLE `estado_cita` (
  `id` int(11) NOT NULL,
  `estado` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_cita`
--

INSERT INTO `estado_cita` (`id`, `estado`) VALUES
(1010, 'Pendiente'),
(2020, 'No asistió'),
(3030, 'Asistió');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_agente`
--

CREATE TABLE `historial_agente` (
  `id` int(11) NOT NULL,
  `id_agente` int(11) NOT NULL,
  `tabla_afectada` text NOT NULL,
  `id_objeto_afectado` int(11) NOT NULL,
  `movimiento` text NOT NULL,
  `fecha_registro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `historial_agente`
--

INSERT INTO `historial_agente` (`id`, `id_agente`, `tabla_afectada`, `id_objeto_afectado`, `movimiento`, `fecha_registro`) VALUES
(1, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-14 08:53:13'),
(2, 10, 'sms_registros', 17, 'se envio un mensaje sms', '2019-12-14 09:18:44'),
(3, 10, 'whatsapp_registros', 7, 'se envio un mensaje whatsapp', '2019-12-14 09:19:22'),
(4, 10, 'citas', 6, 'Registrar Cita', '2019-12-14 09:21:00'),
(5, 10, 'crm_clientes', 1, 'Registrar Cita', '2019-12-14 09:21:00'),
(6, 10, 'marketing', 8, 'Envio un mensaje masivo a 4', '2019-12-14 09:23:45'),
(7, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-15 10:45:53'),
(8, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-15 10:46:04'),
(9, 10, 'crm_clientes', 5, 'Registro Nuevo Cliente', '2019-12-15 10:55:14'),
(10, 10, 'citas', 7, 'Registro Cita una cita', '2019-12-15 10:55:14'),
(11, 10, 'crm_clientes', 5, 'Se actualizaron datos', '2019-12-15 10:55:41'),
(12, 10, 'crm_clientes', 6, 'Registro Nuevo Cliente', '2019-12-15 10:58:50'),
(13, 10, 'crm_clientes', 7, 'Registro Nuevo Cliente', '2019-12-15 10:59:08'),
(14, 10, 'crm_clientes', 8, 'Registro Nuevo Cliente', '2019-12-15 10:59:17'),
(15, 10, 'crm_clientes', 9, 'Registro Nuevo Cliente', '2019-12-15 10:59:28'),
(16, 10, 'crm_clientes', 10, 'Registro Nuevo Cliente', '2019-12-15 10:59:35'),
(17, 10, 'crm_clientes', 11, 'Registro Nuevo Cliente', '2019-12-15 10:59:47'),
(18, 10, 'crm_clientes', 12, 'Registro Nuevo Cliente', '2019-12-15 11:00:14'),
(19, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-15 11:00:42'),
(20, 10, 'crm_clientes', 13, 'Registro Nuevo Cliente', '2019-12-15 11:01:03'),
(21, 10, 'crm_clientes', 14, 'Registro Nuevo Cliente', '2019-12-15 11:04:04'),
(22, 10, 'crm_clientes', 15, 'Registro Nuevo Cliente', '2019-12-15 11:04:10'),
(23, 10, 'crm_clientes', 16, 'Registro Nuevo Cliente', '2019-12-15 11:04:18'),
(24, 10, 'crm_clientes', 17, 'Registro Nuevo Cliente', '2019-12-15 11:04:26'),
(25, 10, 'crm_clientes', 18, 'Registro Nuevo Cliente', '2019-12-15 11:04:35'),
(26, 10, 'crm_clientes', 19, 'Registro Nuevo Cliente', '2019-12-15 11:04:43'),
(27, 10, 'crm_clientes', 20, 'Registro Nuevo Cliente', '2019-12-15 11:04:55'),
(28, 10, 'crm_clientes', 21, 'Registro Nuevo Cliente', '2019-12-15 11:05:22'),
(29, 10, 'crm_clientes', 22, 'Registro Nuevo Cliente', '2019-12-15 11:06:21'),
(30, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:06:21'),
(31, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:08:03'),
(32, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:08:03'),
(33, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:09:23'),
(34, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:09:27'),
(35, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:09:28'),
(36, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:09:28'),
(37, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:10:41'),
(38, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:10:43'),
(39, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:10:44'),
(40, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:10:44'),
(41, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-15 11:11:29'),
(42, 10, 'crm_clientes', 23, 'Registro Nuevo Cliente', '2019-12-15 11:11:54'),
(43, 10, 'crm_clientes', 24, 'Registro Nuevo Cliente', '2019-12-15 11:12:39'),
(44, 10, 'crm_clientes', 25, 'Registro Nuevo Cliente', '2019-12-15 11:14:17'),
(45, 10, 'crm_clientes', 26, 'Registro Nuevo Cliente', '2019-12-15 11:15:35'),
(46, 10, 'crm_clientes', 27, 'Registro Nuevo Cliente', '2019-12-15 11:16:16'),
(47, 10, 'crm_clientes', 28, 'Registro Nuevo Cliente', '2019-12-15 11:17:09'),
(48, 10, 'mensajes_programados', 44, 'Se creo un mensaje Programado por el Agente', '2019-12-15 11:40:00'),
(49, 10, 'usuarios', 10, 'Cerro sesion', '2019-12-15 05:44:04'),
(50, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-16 02:31:16'),
(51, 10, 'crm_clientes', 29, 'Registro Nuevo Cliente', '2019-12-16 02:32:13'),
(52, 10, 'citas', 8, 'Registro Cita una cita', '2019-12-16 02:32:14'),
(53, 10, 'citas', 3, 'Actualizo una cita con el id 3', '2019-12-16 02:32:27'),
(54, 10, 'mensajes_programados', 51, 'Se creo un mensaje Programado por el Agente', '2019-12-16 02:32:42'),
(55, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-16 07:26:20'),
(56, 10, 'crm_clientes', 30, 'Registro Nuevo Cliente', '2019-12-16 07:27:20'),
(57, 10, 'citas', 9, 'Registro Cita una cita', '2019-12-16 07:27:20'),
(58, 10, 'mensajes_programados', 58, 'Se creo un mensaje Programado por el Agente', '2019-12-16 07:27:42'),
(59, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-16 07:35:34'),
(60, 10, 'lead', 1, 'actualizo el estado del leads a En futuro', '2019-12-16 07:35:52'),
(61, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-16 07:59:11'),
(62, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-16 08:13:40'),
(63, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-16 09:21:00'),
(64, 10, 'usuarios', 10, 'Cerro sesion', '2019-12-16 06:01:52'),
(65, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-17 12:05:53'),
(66, 10, 'usuarios', 10, 'Cerro sesion', '2019-12-16 06:06:34'),
(67, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-18 09:29:11'),
(68, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-18 09:29:35'),
(69, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-18 09:34:51'),
(70, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-18 09:34:51'),
(71, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 01:52:07'),
(72, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 01:54:44'),
(73, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 02:28:11'),
(74, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 04:23:53'),
(75, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 05:36:11'),
(76, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 07:47:25'),
(77, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 11:39:03'),
(78, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 11:48:55'),
(79, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 11:48:56'),
(80, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 11:49:02'),
(81, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:52:19'),
(82, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:52:19'),
(83, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:52:38'),
(84, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:52:42'),
(85, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:52:52'),
(86, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:52:59'),
(87, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:53:03'),
(88, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:53:07'),
(89, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:53:12'),
(90, 10, 'lead', 1, 'actualizo el estado del leads a Petición de Contacto', '2019-12-19 11:54:12'),
(91, 10, 'lead', 1, 'actualizo el estado del leads a Petición de Contacto', '2019-12-19 11:54:12'),
(92, 10, 'lead', 1, 'actualizo el estado del leads a Petición de Contacto', '2019-12-19 11:54:28'),
(93, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:57:08'),
(94, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:57:08'),
(95, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:57:27'),
(96, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:57:30'),
(97, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:57:34'),
(98, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 11:58:55'),
(99, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 11:58:56'),
(100, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 11:59:11'),
(101, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 11:59:17'),
(102, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 11:59:21'),
(103, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 11:59:23'),
(104, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 11:59:30'),
(105, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 11:59:44'),
(106, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:01:59'),
(107, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:01:59'),
(108, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 12:02:39'),
(109, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:03:10'),
(110, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:03:10'),
(111, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:03:23'),
(112, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:03:27'),
(113, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:06:31'),
(114, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:06:31'),
(115, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:06:39'),
(116, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:06:44'),
(117, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:06:56'),
(118, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:07:03'),
(119, 10, 'lead', 1, 'actualizo el estado del leads a Citado: Sin Comfirmar', '2019-12-19 12:07:07'),
(120, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:11:56'),
(121, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:11:56'),
(122, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:12:08'),
(123, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:12:11'),
(124, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:12:14'),
(125, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:12:18'),
(126, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:12:22'),
(127, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:12:24'),
(128, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:12:31'),
(129, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 12:12:52'),
(130, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:14:58'),
(131, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:14:58'),
(132, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 12:16:40'),
(133, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 12:16:40'),
(134, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 12:16:50'),
(135, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 12:18:17'),
(136, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 12:18:21'),
(137, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 12:19:15'),
(138, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 12:19:19'),
(139, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 12:19:41'),
(140, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 12:20:05'),
(141, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:32:58'),
(142, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:32:58'),
(143, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:33:04'),
(144, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:33:07'),
(145, 10, 'lead', 1, 'actualizo el estado del leads a En futuro', '2019-12-19 12:36:12'),
(146, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 12:37:06'),
(147, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 12:37:07'),
(148, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:37:40'),
(149, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:37:47'),
(150, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:37:50'),
(151, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:37:53'),
(152, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:37:56'),
(153, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:38:00'),
(154, 10, 'lead', 1, 'actualizo el estado del leads a En futuro', '2019-12-19 12:46:35'),
(155, 10, 'lead', 1, 'actualizo el estado del leads a En futuro', '2019-12-19 12:46:35'),
(156, 10, 'lead', 1, 'actualizo el estado del leads a En futuro', '2019-12-19 12:46:40'),
(157, 10, 'lead', 1, 'actualizo el estado del leads a En futuro', '2019-12-19 12:46:46'),
(158, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:47:27'),
(159, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:47:27'),
(160, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:47:34'),
(161, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:47:37'),
(162, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:47:45'),
(163, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:47:45'),
(164, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:47:48'),
(165, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 12:47:51'),
(166, 10, 'lead', 1, 'actualizo el estado del leads a No Intersado', '2019-12-19 01:04:03'),
(167, 10, 'lead', 1, 'actualizo el estado del leads a No Intersado', '2019-12-19 01:04:03'),
(168, 10, 'lead', 1, 'actualizo el estado del leads a No Intersado', '2019-12-19 01:04:14'),
(169, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 08:07:44'),
(170, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 09:18:05'),
(171, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 09:18:05'),
(172, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 09:18:37'),
(173, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 09:37:18'),
(174, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 10:13:02'),
(175, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-19 10:17:16'),
(176, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-19 10:44:07'),
(177, 10, 'lead', 1, 'actualizo el estado del leads a Confirmado', '2019-12-19 11:20:35'),
(178, 10, 'lead', 1, 'actualizo el estado del leads a En futuro', '2019-12-19 11:20:40'),
(179, 10, 'lead', 1, 'actualizo el estado del leads a Citado', '2019-12-19 11:20:49'),
(180, 10, 'lead', 1, 'actualizo el estado del leads a En futuro', '2019-12-19 11:47:47'),
(181, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-19 11:48:09'),
(182, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-20 12:22:24'),
(183, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-20 12:22:54'),
(184, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-20 01:23:38'),
(185, 10, 'whatsapp_registros', 8, 'se envio un mensaje whatsapp', '2019-12-20 03:05:01'),
(186, 10, 'whatsapp_registros', 9, 'se envio un mensaje whatsapp', '2019-12-20 03:05:21'),
(187, 10, 'whatsapp_registros', 10, 'se envio un mensaje whatsapp', '2019-12-20 03:05:21'),
(188, 10, 'whatsapp_registros', 11, 'se envio un mensaje whatsapp', '2019-12-20 03:06:26'),
(189, 10, 'whatsapp_registros', 12, 'se envio un mensaje whatsapp', '2019-12-20 03:31:10'),
(190, 10, 'whatsapp_registros', 13, 'se envio un mensaje whatsapp', '2019-12-20 03:32:14'),
(191, 10, 'crm_clientes', 1, 'Registro Nuevo Cliente', '2019-12-20 04:02:35'),
(192, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-20 04:03:32'),
(193, 10, 'lead', 1, 'actualizo el estado del leads a Fallida', '2019-12-20 04:10:11'),
(194, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-20 04:10:21'),
(195, 10, 'lead', 1, 'actualizo el estado del leads a Pendiente: No Contesta', '2019-12-20 04:12:13'),
(196, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-20 04:12:58'),
(197, 10, 'crm_clientes', 2, 'Registro Nuevo Cliente', '2019-12-20 04:14:23'),
(198, 10, 'whatsapp_registros', 14, 'se envio un mensaje whatsapp', '2019-12-20 04:15:33'),
(199, 10, 'whatsapp_registros', 15, 'se envio un mensaje whatsapp', '2019-12-20 04:15:36'),
(200, 10, 'whatsapp_registros', 16, 'se envio un mensaje whatsapp', '2019-12-20 04:16:00'),
(201, 10, 'whatsapp_registros', 17, 'se envio un mensaje whatsapp', '2019-12-20 04:16:00'),
(202, 10, 'whatsapp_registros', 18, 'se envio un mensaje whatsapp', '2019-12-20 04:16:10'),
(203, 10, 'usuarios', 10, 'Inicio sesion', '2019-12-20 04:20:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_cliente`
--

CREATE TABLE `historial_cliente` (
  `id` int(11) NOT NULL,
  `id_agente` int(11) NOT NULL,
  `tabla_afectada` text NOT NULL,
  `id_objeto_afectado` int(11) NOT NULL,
  `movimiento` text NOT NULL,
  `titulo` text NOT NULL,
  `fecha_registro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `historial_cliente`
--

INSERT INTO `historial_cliente` (`id`, `id_agente`, `tabla_afectada`, `id_objeto_afectado`, `movimiento`, `titulo`, `fecha_registro`) VALUES
(1, 10, 'crm_clientes', 1, 'Recibio un mensaje whatsapp', 'fsfsdfsdf', '2019-12-20 04:15:33'),
(2, 10, 'crm_clientes', 1, 'Recibio un mensaje whatsapp', 'fsdfsdfsd', '2019-12-20 04:15:37'),
(3, 10, 'crm_clientes', 2, 'Recibio un mensaje whatsapp', 'fsdfsdfdsfs', '2019-12-20 04:16:00'),
(4, 10, 'crm_clientes', 2, 'Recibio un mensaje whatsapp', 'fsdfsdfdsfs', '2019-12-20 04:16:00'),
(5, 10, 'crm_clientes', 1, 'Recibio un mensaje whatsapp', 'fsdfsdfsdf', '2019-12-20 04:16:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lead`
--

CREATE TABLE `lead` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `nombre_lead` text NOT NULL,
  `telefono` int(11) NOT NULL,
  `email` text NOT NULL,
  `estado` varchar(34) NOT NULL,
  `origen` text NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lead`
--

INSERT INTO `lead` (`id`, `id_cliente`, `nombre_lead`, `telefono`, `email`, `estado`, `origen`, `fecha`) VALUES
(1, 1, '', 0, '', 'Pendiente: No Contesta', '', '2019-12-02 12:31:33'),
(10, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', 'Citado', 'origen ejemplo', '2019-12-19 11:15:30'),
(11, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:31'),
(12, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:31'),
(13, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:53'),
(14, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:53'),
(15, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:53'),
(16, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:54'),
(17, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:54'),
(18, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:54'),
(19, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:54'),
(20, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:54'),
(21, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:55'),
(22, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:55'),
(23, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:55'),
(24, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:55'),
(25, 2, 'Juan ejemplo', 21, 'juanejemplo@correo.com', '', 'origen ejemplo', '2019-12-19 11:15:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `llamadas`
--

CREATE TABLE `llamadas` (
  `id` int(11) NOT NULL,
  `id_agente` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `asunto` text NOT NULL,
  `fecha_registro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marketing`
--

CREATE TABLE `marketing` (
  `id` int(11) NOT NULL,
  `mensaje` text NOT NULL,
  `tipo` varchar(25) NOT NULL,
  `catgoria_mensaje` varchar(25) NOT NULL,
  `alcance` int(10) NOT NULL,
  `fecha_envio` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `marketing`
--

INSERT INTO `marketing` (`id`, `mensaje`, `tipo`, `catgoria_mensaje`, `alcance`, `fecha_envio`) VALUES
(1, 'dasdasdas', 'email', 'masivo', 2, '2019-12-10 11:07:50'),
(2, 'tertretert', 'whatsapp', 'masivo', 2, '2019-12-10 11:08:19'),
(3, 'gfdgdgdg', 'sms', 'masivo', 2, '2019-12-10 11:08:31'),
(4, 'dgdfdgfdg', 'sms', 'masivo', 2, '2019-12-10 11:08:41'),
(5, 'dgdfdgfdg', 'sms', 'masivo', 2, '2019-12-10 11:08:46'),
(6, 'dsadsadsad', 'sms', 'masivo', 4, '2019-12-12 04:08:54'),
(7, 'dsadsadsaddsadsadsa', 'sms', 'masivo', 4, '2019-12-12 04:09:05'),
(8, 'fsdfsfsd', 'email', 'masivo', 4, '2019-12-14 09:23:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes_programados`
--

CREATE TABLE `mensajes_programados` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_cita` int(11) NOT NULL,
  `id_agente` int(11) NOT NULL,
  `fecha_envio` datetime NOT NULL,
  `estado` varchar(30) NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mensajes_programados`
--

INSERT INTO `mensajes_programados` (`id`, `id_cliente`, `id_cita`, `id_agente`, `fecha_envio`, `estado`, `url`) VALUES
(1, 1, 1, 10, '2019-12-14 03:33:00', '', ''),
(2, 1, 1, 10, '2019-12-16 03:33:00', '', ''),
(3, 1, 1, 10, '2019-12-16 21:33:00', '', ''),
(4, 1, 1, 10, '2019-12-20 03:33:00', '', ''),
(5, 1, 1, 10, '2019-12-18 03:33:00', '', ''),
(6, 1, 1, 10, '2019-12-17 09:33:00', '', ''),
(7, 3, 2, 10, '2019-12-15 14:01:00', '', ''),
(8, 3, 2, 10, '2019-12-17 14:01:00', '', ''),
(9, 3, 2, 10, '2019-12-18 08:01:00', '', ''),
(10, 3, 2, 10, '2019-12-21 14:01:00', '', ''),
(11, 3, 2, 10, '2019-12-19 14:01:00', '', ''),
(12, 3, 2, 10, '2019-12-18 20:01:00', '', ''),
(13, 4, 3, 10, '2019-12-15 03:33:00', '', ''),
(14, 4, 3, 10, '2019-12-17 03:33:00', '', ''),
(15, 4, 3, 10, '2019-12-17 21:33:00', '', ''),
(16, 4, 3, 10, '2019-12-21 03:33:00', '', ''),
(17, 4, 3, 10, '2019-12-19 03:33:00', '', ''),
(18, 4, 3, 10, '2019-12-18 09:33:00', '', ''),
(19, 4, 3, 10, '2019-12-19 15:01:00', '', 'dadasdas'),
(20, 1, 4, 10, '2019-12-17 13:00:00', '', ''),
(21, 1, 4, 10, '2019-12-19 13:00:00', '', ''),
(22, 1, 4, 10, '2019-12-20 07:00:00', '', ''),
(23, 1, 4, 10, '2019-12-23 13:00:00', '', ''),
(24, 1, 4, 10, '2019-12-21 13:00:00', '', ''),
(25, 1, 4, 10, '2019-12-20 19:00:00', '', ''),
(26, 1, 5, 10, '2019-12-17 16:44:00', '', ''),
(27, 1, 5, 10, '2019-12-19 16:44:00', '', ''),
(28, 1, 5, 10, '2019-12-20 10:44:00', '', ''),
(29, 1, 5, 10, '2019-12-23 16:44:00', '', ''),
(30, 1, 5, 10, '2019-12-21 16:44:00', '', ''),
(31, 1, 5, 10, '2019-12-20 22:44:00', '', ''),
(32, 1, 6, 10, '2019-12-24 03:33:00', '', ''),
(33, 1, 6, 10, '2019-12-26 03:33:00', '', ''),
(34, 1, 6, 10, '2019-12-26 21:33:00', '', ''),
(35, 1, 6, 10, '2019-12-30 03:33:00', '', ''),
(36, 1, 6, 10, '2019-12-28 03:33:00', '', ''),
(37, 1, 6, 10, '2019-12-27 09:33:00', '', ''),
(38, 5, 7, 10, '2019-12-23 16:04:00', '', ''),
(39, 5, 7, 10, '2019-12-25 16:04:00', '', ''),
(40, 5, 7, 10, '2019-12-26 10:04:00', '', ''),
(41, 5, 7, 10, '2019-12-29 16:04:00', '', ''),
(42, 5, 7, 10, '2019-12-27 16:04:00', '', ''),
(43, 5, 7, 10, '2019-12-26 22:04:00', '', ''),
(44, 1, 1, 10, '0000-00-00 00:00:00', '', ''),
(45, 29, 8, 10, '2019-12-17 15:02:00', '', ''),
(46, 29, 8, 10, '2019-12-19 15:02:00', '', ''),
(47, 29, 8, 10, '2019-12-20 09:02:00', '', ''),
(48, 29, 8, 10, '2019-12-23 15:02:00', '', ''),
(49, 29, 8, 10, '2019-12-21 15:02:00', '', ''),
(50, 29, 8, 10, '2019-12-20 21:02:00', '', ''),
(51, 5, 7, 10, '2019-12-20 03:01:00', '', 'sasa'),
(52, 30, 9, 10, '2019-12-11 14:01:00', '', ''),
(53, 30, 9, 10, '2019-12-13 14:01:00', '', ''),
(54, 30, 9, 10, '2019-12-14 08:01:00', '', ''),
(55, 30, 9, 10, '2019-12-17 14:01:00', '', ''),
(56, 30, 9, 10, '2019-12-15 14:01:00', '', ''),
(57, 30, 9, 10, '2019-12-14 20:01:00', '', ''),
(58, 30, 9, 10, '2019-12-17 14:01:00', '', 'dsfdsf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `papelera`
--

CREATE TABLE `papelera` (
  `id` int(11) NOT NULL,
  `tabla` text CHARACTER SET utf8 NOT NULL,
  `id_afectado` int(11) NOT NULL,
  `fecha_eliminacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id` int(11) NOT NULL,
  `permiso` text NOT NULL,
  `categoria` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id`, `permiso`, `categoria`) VALUES
(343453, 'login', 'general'),
(343454, 'Registrar clientes', 'clientes'),
(343455, 'Enviar Correo a un cliente', 'clientes'),
(343456, 'Enviar whatsapp a un cliente', 'clientes'),
(343457, 'Enviar sms a un cliente', 'clientes'),
(343458, 'Listar Clientes', 'clientes'),
(343459, 'Listar marketing', 'marketing'),
(343460, 'Enviar Email Masivo', 'marketing'),
(343461, 'Enviar Whatsapp Masivo', 'marketing'),
(343462, 'Enviar Sms Masivo', 'marketing'),
(343463, 'Listar citas', 'citas'),
(343464, 'Crear Citas', 'citas'),
(343465, 'Listar Leads', 'leads'),
(343466, 'Crear Estados en los Leads', 'leads'),
(343467, 'eliminar marketing', 'marketing'),
(343468, 'listar Servicios', 'servicios'),
(343469, 'crear Servicios', 'servicios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_usuarios`
--

CREATE TABLE `permisos_usuarios` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_permiso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos_usuarios`
--

INSERT INTO `permisos_usuarios` (`id`, `id_usuario`, `id_permiso`) VALUES
(1, 10, 343453),
(2, 10, 343454),
(3, 10, 343453),
(4, 10, 343456),
(5, 10, 343458),
(6, 10, 343459),
(7, 10, 343463),
(8, 10, 343465),
(9, 10, 343460),
(10, 10, 343461),
(11, 10, 343464),
(12, 10, 343462),
(13, 10, 343457),
(14, 10, 343455),
(15, 12, 343466),
(16, 12, 343465),
(17, 12, 343462),
(18, 12, 343464),
(19, 12, 343461),
(20, 12, 343460),
(21, 12, 343459),
(22, 12, 343456),
(23, 12, 343457),
(24, 12, 343458),
(25, 12, 343463),
(26, 12, 343455),
(27, 12, 343453),
(28, 12, 343454),
(29, 10, 343467),
(31, 10, 343468),
(32, 10, 343469),
(33, 10, 343466);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios_contratados`
--

CREATE TABLE `servicios_contratados` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_agente` int(11) NOT NULL,
  `asunto` text NOT NULL,
  `precio` varchar(100) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `servicios_contratados`
--

INSERT INTO `servicios_contratados` (`id`, `id_cliente`, `id_agente`, `asunto`, `precio`, `fecha_creacion`) VALUES
(5, 10, 1, 'dsdsadsad', '43243', '2019-12-11 00:00:00'),
(6, 1, 10, 'fdfsdfdsf', '5656565', '2019-12-20 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sms_registros`
--

CREATE TABLE `sms_registros` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_agente` int(10) NOT NULL,
  `mensaje` text NOT NULL,
  `id_envio` int(11) NOT NULL,
  `fecha_envio` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sms_registros`
--

INSERT INTO `sms_registros` (`id`, `id_cliente`, `id_agente`, `mensaje`, `id_envio`, `fecha_envio`) VALUES
(1, 1, 10, 'dsada', 0, '2019-12-02'),
(2, 1, 10, 'fsdfsdfs', 0, '2019-12-10'),
(3, 2, 10, 'gfdgdgdg', 3, '2019-12-10'),
(4, 1, 10, 'gfdgdgdg', 3, '2019-12-10'),
(5, 2, 10, 'dgdfdgfdg', 4, '2019-12-10'),
(6, 1, 10, 'dgdfdgfdg', 4, '2019-12-10'),
(7, 2, 10, 'dgdfdgfdg', 5, '2019-12-10'),
(8, 1, 10, 'dgdfdgfdg', 5, '2019-12-10'),
(9, 4, 10, 'dsadsadsad', 6, '2019-12-12'),
(10, 3, 10, 'dsadsadsad', 6, '2019-12-12'),
(11, 2, 10, 'dsadsadsad', 6, '2019-12-12'),
(12, 1, 10, 'dsadsadsad', 6, '2019-12-12'),
(13, 4, 10, 'dsadsadsaddsadsadsa', 7, '2019-12-12'),
(14, 3, 10, 'dsadsadsaddsadsadsa', 7, '2019-12-12'),
(15, 2, 10, 'dsadsadsaddsadsadsa', 7, '2019-12-12'),
(16, 1, 10, 'dsadsadsaddsadsadsa', 7, '2019-12-12'),
(17, 4, 10, 'tertert', 0, '2019-12-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_de_filtros`
--

CREATE TABLE `tipos_de_filtros` (
  `id` int(10) NOT NULL,
  `nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipos_de_filtros`
--

INSERT INTO `tipos_de_filtros` (`id`, `nombre`) VALUES
(0, 'No cumplida'),
(1, 'Modificada'),
(2, 'Cancelada'),
(3, 'Cumplida');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` text NOT NULL,
  `pass` text NOT NULL,
  `nombre` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `pass`, `nombre`) VALUES
(10, 'admin', '$2a$07$asxx54ahjppf45sd87a5auRajNP0zeqOkB9Qda.dSiTb2/n.wAC/2', 'eduardo berastegui');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `whatsapp_registros`
--

CREATE TABLE `whatsapp_registros` (
  `id` int(11) NOT NULL,
  `id_cliente` int(10) NOT NULL,
  `id_agente` int(10) NOT NULL,
  `mensaje` text NOT NULL,
  `id_envio` int(11) NOT NULL,
  `fecha_envio` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `whatsapp_registros`
--

INSERT INTO `whatsapp_registros` (`id`, `id_cliente`, `id_agente`, `mensaje`, `id_envio`, `fecha_envio`) VALUES
(1, 1, 10, 'dasd', 0, '2019-12-02 12:44:49'),
(2, 1, 10, 'gfdgdgfd', 0, '2019-12-10 10:43:31'),
(3, 1, 10, 'dasdasda', 0, '2019-12-10 11:01:36'),
(4, 2, 10, 'tertretert', 2, '2019-12-10 11:08:19'),
(5, 1, 10, 'tertretert', 2, '2019-12-10 11:08:19'),
(6, 4, 10, 'yufyufyu', 0, '2019-12-14 07:51:35'),
(7, 3, 10, '6etretertte', 0, '2019-12-14 09:19:22'),
(8, 2, 10, 'xvxcvxcvxcvxcvxc', 0, '2019-12-20 03:05:00'),
(9, 2, 10, 'fsdfsfsdfsdf', 0, '2019-12-20 03:05:21'),
(10, 2, 10, 'fsdfsfsdfsdf', 0, '2019-12-20 03:05:21'),
(11, 2, 10, 'dsadad', 0, '2019-12-20 03:06:26'),
(12, 2, 10, 'fdfsdfs', 0, '2019-12-20 03:31:10'),
(13, 2, 10, 'resrsresrsere', 0, '2019-12-20 03:32:14'),
(14, 1, 10, 'fsfsdfsdf', 0, '2019-12-20 04:15:33'),
(15, 1, 10, 'fsdfsdfsd', 0, '2019-12-20 04:15:36'),
(16, 2, 10, 'fsdfsdfdsfs', 0, '2019-12-20 04:16:00'),
(17, 2, 10, 'fsdfsdfdsfs', 0, '2019-12-20 04:16:00'),
(18, 1, 10, 'fsdfsdfsdf', 0, '2019-12-20 04:16:10');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `crm_clientes`
--
ALTER TABLE `crm_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `email_registro`
--
ALTER TABLE `email_registro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_cita`
--
ALTER TABLE `estado_cita`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historial_agente`
--
ALTER TABLE `historial_agente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historial_cliente`
--
ALTER TABLE `historial_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lead`
--
ALTER TABLE `lead`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `llamadas`
--
ALTER TABLE `llamadas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `marketing`
--
ALTER TABLE `marketing`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensajes_programados`
--
ALTER TABLE `mensajes_programados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `papelera`
--
ALTER TABLE `papelera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permisos_usuarios`
--
ALTER TABLE `permisos_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios_contratados`
--
ALTER TABLE `servicios_contratados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sms_registros`
--
ALTER TABLE `sms_registros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `whatsapp_registros`
--
ALTER TABLE `whatsapp_registros`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `crm_clientes`
--
ALTER TABLE `crm_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `email_registro`
--
ALTER TABLE `email_registro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `historial_agente`
--
ALTER TABLE `historial_agente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT de la tabla `historial_cliente`
--
ALTER TABLE `historial_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `lead`
--
ALTER TABLE `lead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `llamadas`
--
ALTER TABLE `llamadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `marketing`
--
ALTER TABLE `marketing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `mensajes_programados`
--
ALTER TABLE `mensajes_programados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `papelera`
--
ALTER TABLE `papelera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3434591;

--
-- AUTO_INCREMENT de la tabla `permisos_usuarios`
--
ALTER TABLE `permisos_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `servicios_contratados`
--
ALTER TABLE `servicios_contratados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sms_registros`
--
ALTER TABLE `sms_registros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `whatsapp_registros`
--
ALTER TABLE `whatsapp_registros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
