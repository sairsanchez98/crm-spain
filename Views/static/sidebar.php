
        <nav class="hk-nav hk-nav-light ">
            <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
            <div class="nicescroll-bar">
                <div class="navbar-nav-wrap">


             

                <ul class="navbar-nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard" >
                                <span class="feather-icon"><img id="espacio" src="Assets/dist/img/dashboard.png" width="30" ></span>
                                <span class="nav-link-text">Dashboard</span>
                            </a>
                        </li>
                        <div v-for="permiso in permisosUsuarioLogued">
                            <div v-if="permiso.id=='343458' && permiso.activado">
                             <li class="nav-item ">
                               <a class="nav-link link-with-badge" href="clientes">
                                 <span class="feather-icon"><img  id="espacio"src="Assets/dist/img/clientes.png" width="30" ></span>
                                 <span class="nav-link-text">Clientes</span>
                                
                               </a>
                             </li>
                            </div>
                        </div>  
      
                        <div v-for="permiso in permisosUsuarioLogued">
                            <div v-if="permiso.id=='343465' && permiso.activado">
                                <li class="nav-item">
                                    <a class="nav-link link-with-badge" href="leads">
                                        <span class="feather-icon"><img id="espacio" src="Assets/dist/img/leads.png" width="30" ></span>
                                        <span class="nav-link-text">Leads</span>
                                        
                                    </a>
                                </li>
                            </div>
                        </div>
                        <div v-for="permiso in permisosUsuarioLogued">
                            <div v-if="permiso.id=='343463' && permiso.activado">
                                <li class="nav-item">
                                    <a class="nav-link" href="citas">
                                        <span class="feather-icon"><img id="espacio" src="Assets/dist/img/citas.png" width="30" ></span>
                                        <span class="nav-link-text">Citas</span>
                                    </a>
                                </li>
                            </div>
                        </div>
                        <div v-for="permiso in permisosUsuarioLogued">
                            <div v-if="permiso.id=='343459' && permiso.activado">
                                <li class="nav-item">
                                    <a class="nav-link" href="marketing">
                                        <span class="feather-icon"><img id="espacio" src="Assets/dist/img/marketing.png" width="30" ></span>
                                        <span class="nav-link-text">Marketing</span>
                                    </a>
                                </li>
                            </div>
                        </div>
                        <div v-for="permiso in permisosUsuarioLogued">
                            <div v-if="permiso.id=='343458' && permiso.activado">
                                <li class="nav-item">
                                    <a class="nav-link" href="agentes">
                                        <span class="feather-icon"><img id="espacio" src="Assets/dist/img/agentes.png" width="30" ></span>
                                        <span class="nav-link-text">Agentes</span>
                                    </a>
                                </li>
                            </div>
                        </div>
                        <div v-for="permiso in permisosUsuarioLogued">
                            <div v-if="permiso.id=='343458' && permiso.activado">
                                <li class="nav-item">
                                    <a class="nav-link" href="historial">
                                        <span class="feather-icon"><img id="espacio" src="Assets/dist/img/historial.png" width="30" ></span>
                                        <span class="nav-link-text">Historial</span>
                                    </a>
                                </li>
                            </div>
                        </div>
                    </ul>
                    <ul class="navbar-nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#maps_drp">
                                <span class="feather-icon"><img id="espacio" src="Assets/dist/img/servicios.png" width="30" ></span>
                                <span class="nav-link-text">Servicios</span>
                            </a>
                            <ul id="maps_drp" class="nav flex-column collapse collapse-level-1">
                                <li class="nav-item">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="servicios">Registrar</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="vector-map.html">acción 2</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    
                 

                </div>
            </div>
        </nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
<script src="Controllers/js/ctrlSidebar.js"></script>
