<?php ?>
<div class="hk-pg-wrapper hk-auth-wrapper" id="log-in">
				

				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12 pa-0">
							<div class="auth-form-wrap pt-xl-0 pt-70">
								<div class="auth-form w-xl-30 w-lg-55 w-sm-75 w-100">
                                    
                                <div class="container mb-10">
                                    <div class="row">
                                        <div class="col-md-12 content-center">
                                            <h4  class="display-4 text-center mb-10 "><img class="img-fluid mr-1" width="46" src="Assets/dist/img/arraytic-logo.png">CRM</h4>            
                                        </div>
                                    </div>
                                </div>
                                

									<form @keyup.enter="Login" >
										<div class="form-group">
											<input v-model="user" @click="alert=false" class="form-control" placeholder="usuario" type="text">
										</div>
										
										<div class="form-group mb-18">
                                            <div class="input-group">
                                                
                                                <input v-model="pass" @click="alert=false" type="password" id="contrasena" class="form-control" placeholder="Contraseña" >
                                                <div class="input-group-prepend">
                                                    <button @click="showPass" class="btn btn-outline-light" type="button">
														<i  class="fa fa-eye-slash"  v-show="showpass"></i>
														<i  class="fa fa-eye"  v-show="!showpass"></i>
													</button>
                                                </div>
                                            </div>
                                        </div>
										<div v-show="alert" class="alert alert-danger" role="alert">
                                        	<p v-text="alert"></p>
                                    	</div>
										<button v-if="!Loader" :class="[ButtonColorlogin,'btn-block']" type="button" @click="Login()">{{textoBtnLogin}}</button>
										<button v-if="Loader" class="btn btn-primary btn-block" type="button" @click="Login()">
											Cargando <img src="Assets/dist/img/loading.gif" width="15">
										</button>
										
										
									
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

<script src="Controllers/js/ctrlLogin.js"></script>