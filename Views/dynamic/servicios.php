<?php ?>
<!-- Container -->
<div class="container-fluid" id ="servicios">
                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="emailapp-wrap">
            
                            <div class="email-box">
                            <div class="emailapp-left card  card-refresh ">
                            <div class="refresh-container" :style="{ display:loaderLeft }">
                                <div class="loader-pendulums"></div>
                            </div>
                            <header>
                                <a @click="HabilitarComponente('lista')" href="javascript:void(0)" class="emailapp-sidebar-move">
                                    <span class="feather-icon"><i data-feather="list"></i></span>
                                </a>
                                <span class="">Servicios     </span>
                                <a  @click="RegistrarServicio('form')" href="javascript:void(0)" class="email-compose" data-toggle="modal" data-target="#exampleModalEmail">
                                    <span class="feather-icon"><i data-feather="plus"></i></span>
                                </a>
                            </header>
                           
                            <form role="search" class="email-search" v-show="ListaServicios">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="feather-icon"><i data-feather="search"></i></span>
                                    </div>
                                    <input v-model="buscar" @keyup="Buscar()" type="text"  class="form-control" placeholder="Buscar...">
                                </div>
                            </form>
                            <div v-if="mensajeNoLista" style="height:400px;"  >
                                <div  class="col-sm">
                                <p style="color:red">No puedes ver La lista de usuarios</p>
                                </div>
                                
                            </div>
                            
                            <!-- LISTA DE CONVENIOS-->
                            <div  class="emailapp-emails-list" v-if="ListaServicios" >
                                <div class="nicescroll-bar" >
                                    <div v-for="(item,index) in  ClientesQueMuestran">
                                    <a href="javascript:void(0);"  @click="MostrarServicios(item.id_cliente, item.id_agente, item.id)" class="media">
                                        <div class="media-body">
                                            <div>
                                       
                                                <div class="email-head">Asunto :{{item.asunto}} </div>
                                                
                                                <div class="email-subject"> Precio: {{'$' + item.precio}}</div>
                                                <!-- <div class="email-text">
                                                    <p>So how did the classical Latin become so incoherent? According to McClintock.</p>
                                                </div> -->
                                            </div>
                                            <div>
                                                <div class="last-email-details">
                                                  <p style="font-size:15px;">Fecha:  {{item.fecha_creacion}}</p>
                                                </div>
                                                <!-- <span class="email-star"><span class="feather-icon"><i data-feather="star"></i></span></span> -->
                                            </div>
                                        </div>
                                    </a>
                                    <div class="email-hr-wrap">
                                        <hr>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FORMULARIO DE REGISTRO  -->
                            <section v-show="formRegistrarClientes" class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Registrar servicio</h5>
                                <div class="row">
                                    <div class="col-sm">
                                
                                        <form>
                                            <div class="refresh-container" :style="{ display:loaderLeft }">
                                                <div class="loader-pendulums"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Teléfono</label>
                                                    <input v-model="telefono"  @click="Alert_telefono = 'border-default' " :class="['form-control', Alert_telefono ]"   type="number">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Valor del Servicio ofrecido</label>
                                                    <input v-model="precio" type="number" class="form-control"  >
                                                </div>
                                                <div class="col-lg-6">     
                                                        <label for="id">Fecha</label>
                                                        <input type="date" v-model="fecha_servicio" class="form-control" id="id" >
                                                    </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Descripción del servicio ofrecido</label>
                                                    <textarea v-model="servicio_ofrecido" class="form-control"  ></textarea>
                                                </div>
                                            </div>


                                            <hr>
                                            <button @click="RegistrarServicio('Procesar')" class="btn btn-primary" type="button">Registrar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                                <!--FORMULARIO REGISTRO-->
                           
                           
                        </div>






                        <div class="emailapp-right">
                            <header>
                                <a id="back_email_list" href="javascript:void(0)" class="back-email-list">
                                    <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                </a>
                                
                                <div class="email-options-wrap" v-show="habilitar">
          
                                 <a href="javascript:void(0)" @click="HabilitarComponente('infoServicio')" class=""><span class="feather-icon"><img src="Assets/dist/img/llamadas.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="HabilitarComponente('historial')" class=""><span class="feather-icon"><img src="Assets/dist/img/historial_.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="AlertEliminarCliente=true" class=""><span class="feather-icon"><img src="Assets/dist/img/borrar.png" width="30" ></span></a>
                                
                                    
                                </div>
                            </header>
                            
                            
                            <div class="email-body">
                                
                                      
                               
                                <div class="nicescroll-bar" >
                                <div class="email-subject-head">
                                            <h4>Servicio<br> {{cliente_s}}
                                            </h4>
                                            
                               </div>
                               <hr>
                               <div class="col-sm-12" v-show="AlertEliminarCliente" >
                                            <div class="alert alert-warning alert-wth-icon alert-dismissible fade show" role="alert">
                                                <span class="alert-icon-wrap"><i class="zmdi zmdi-alert-circle-o"></i></span>
                                                <h5 class="alert-heading">¿Desea mover este Cliente a la papelera?</h5>
                                                <button @click="AlertEliminarCliente=false" class="btn btn-secondary mt-20 mr-5">Cancelar</button>
                                                <button @click="MoverAlaPapelera('procesar')"  class="btn btn-primary mt-20">Eliminar</button>
                                                <button @click="AlertEliminarCliente=false" type="button" class="close" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div> 
                                        </div>


    
                   <!--EDITAR CLIENTE-->
               
                   <div class="row" v-for="cliente in Cliente" v-if="editar_">
                              
                              <section class="col-lg-12  hk-sec-wrapper">
                                                      <h4 class="hk-sec-title mb-25"><img src="Assets/dist/img/editar.png" width="30" > Modificar Servicio</h4>
                                                      <div class="row">
                                                          <div class="col-12">
                                                              <div class="row" v-for="cliente in Cliente">
                                                                  <div class="col-md-12 col-sm-12">
      
                                                                      <form>
                                                                          
                                                                        
      
                                                                          
                                                                          <div class = "row">
                                                                              <div class="form-group col-md-6">
                                                                                  <label >DNI</label>
                                                                                  <input v-model="cliente.dni"  class="form-control" type="number">
                                                                              </div>
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Nombre</label>
                                                                                  <input v-model="cliente.nombre" class="form-control"  type="text">
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group">
                                                                                  <label >Email</label>
                                                                                  <input v-model="cliente.email" class="form-control" type="text">
                                                                          </div>
      
                                                                          <div class = "row">
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Teléfono</label>
                                                                                  <input v-model="cliente.telefono" class="form-control" type="text">
                                                                              </div>
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Dirección</label>
                                                                                  <input v-model="cliente.direccion" class="form-control"  type="text">
                                                                              </div> 
                                                                             
                                                                          </div>
      
                                                                     
                                                                          <hr>
                                                                          <center><button @click="HabilitarComponente('lista')" class="btn btn-dark" type="button">Cancelar</button>
                                                                        <button @click.prevent="EditarCliente('editar')" class="btn btn-primary" type="button">Guardar Cambios</button></center>
      
                                                                      </form>
                                                  
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                              </section>
                                
      
      
                                                                   
                              </div>
                   <!--EDITAR CLIENTE-->
            <!-- INFORMACIÓN DEL CLIENTE  -->
                    <div v-if="infoServicio" >
                                        
        
                                            <div class="email-head">
                                                <div class="media">
                                                    <div class="media-body">
                                                        


                                                    <section class="hk-sec-wrapper">
                                                        <div class="row">
                                                            <div class="col-sm">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul class="list-group mb-15">
                                                                           
                                                                 
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Cantidad: {{Cliente.cantidad}}</label>
                                                                            </li>
                                                                            <li class="list-group-item d-flex ">
                                                                        <label> Asunto:</label> <br> <br> <br>
                                                                          <textarea name=""style="width:500px; height:200px; " value="" id="" cols="50" rows="12">{{Cliente[0].asunto}}
                                                                        </textarea>   
                                                                        
                                                                        </li>
                                                                   

                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Fecha De creación: {{Cliente.fecha}} </label>
                                                                            </li>
                                                                          
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul class="list-group mb-15">
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Nombre Cliente:{{Cliente.nombre_cliente}} </label>
                                                                           </li>
                                                                    
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Telefono: {{Cliente.telefono_cliente}} </label>
                                                                            </li>
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Nombre Agente: {{Cliente.nombre_agente}} </label>
                                                                            </li>
                                                                        
                                                                            
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>




                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-light">
                        </div>
                    <!-- INFORMACIÓN DEl ESPECIALISTA -->
                                    
                                </div>
                            </div>
                        </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
                <!-- /Row -->

            </div>



<!-- /Container -->

<script src="Controllers/js/ctrlServicios.js"></script>