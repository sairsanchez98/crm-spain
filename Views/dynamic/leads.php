<?php ?>
<!-- Container -->
<style>

#f:hover{
  color:#3ebccd
}</style>
<div class="container-fluid" id ="leads">
                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="emailapp-wrap">
            
                            <div class="email-box">
                            <div class="emailapp-left card  card-refresh ">
                            <div class="refresh-container" :style="{ display:loaderLeft }">
                                <div class="loader-pendulums"></div>
                            </div>
                            <header>
                                <a @click="HabilitarComponente('lista')" href="javascript:void(0)" class="emailapp-sidebar-move">
                                    <span class="feather-icon"><i data-feather="list"></i></span>
                                </a>
                                <span class="">Leads</span>
                                <a  @click="RegistrarCliente('form')" href="javascript:void(0)" class="email-compose" data-toggle="modal" data-target="#exampleModalEmail">
                                    <!--span class="feather-icon"><i data-feather="plus"></i></-->
                                </a>
                            </header>
                           
                            <form role="search" class="email" v-show="ListaLeads">
                                <div class="row">
                                <div class="col-lg-12">
                                <!--div class="form-group" style="margin:10px;">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="inputGroupSelect01">Filtros</label>
                                                </div>
                                                <select class="form-control custom-select" id="inputGroupSelect01"  @change="MostrarLeads(false)"  v-model="filtrar_estado">
                                                <option value="todos"  >Todos</option>
                                                  <option v-for="i in filtros" :value="i.nombre">{{i.nombre}}</option>
                                                </select>
                                            </div>
                                        </div-->
                                    <div class="input-grounp">
                                          
                                                <div class="form-group" style="margin:10px;">
                                            <div class="input-group">
                                              
                                            <div class="input-group-append">
                                                    <button class="btn btn-primary dropdown-toggle"   type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                    <div class="dropdown-menu">
                                                    <option style="cursor:pointer" @click="  filtrarEstado('todos')" >Todos</option>
                                                  <option style="cursor:pointer" @click="  filtrarEstado('Numero erroneo')" >Numero erroneo</option>
                                                  <option style="cursor:pointer" @click="  filtrarEstado('No Intersado')" >No Intersado</option>
                                                  <option style="cursor:pointer" @click="  filtrarEstado('Citado')" >Citado</option>
                                                  <option style="cursor:pointer" @click="  filtrarEstado('Citado: Recordatorio 1')" >Citado: Recordatorio 1</option>
                                                  <option style="cursor:pointer" @click=" filtrarEstado('Citado Recordatorio 2')" >Citado Recordatorio 2</option>
                                                  <option style="cursor:pointer" @click="  filtrarEstado('Citado: Sin Comfirmar')" >Citado: Sin Comfirmar</option>
                                                  <option style="cursor:pointer" @click=" filtrarEstado('Confirmado')" >Confirmado</option>
                                                  <option style="cursor:pointer" @click="  filtrarEstado('Fallida')" >Fallida</option>
                                                  <option style="cursor:pointer" @click=" filtrarEstado('En futuro')" >En futuro</option>
                                                  <option style="cursor:pointer" @click=" filtrarEstado('Pendiente: No Contesta')" >Pendiente: No Contesta</option>
                                                  <option style="cursor:pointer" @click="  filtrarEstado('Petición de Contacto')" >Petición de Contacto</option>
                                                 
                                                   
                                                    
                                                    </div>
                                                </div>
                                                <input type="date" @change="filtrar()" v-model="fecha_inicio" class="form-control">
                                                <input type="date" @change="filtrar()" v-model="fecha_final" class="form-control">
                                              
                                            </div>
                                        </div>
                                               
                                    </div>
                                    </div>
                                <br> <br>
                                   
                                        
                                      
                                               <!--div class="input-group-prepend">
                                        <span class="feather-icon"><i data-feather="search"></i></span>
                                    </div>
                                    <input v-model="buscar" @keyup="Buscar()" type="text"  class="form-control" placeholder="Buscar..."-->
                                </div>
                                
                             
                            </form>
                            <div v-show="mensajeNoLista" class="mt-20 col-lg-12 col-md-6 col-sm-6 col-xs-12 mb-30">
                                    <div class="card border-danger">
                                        <div class="card-header">Clientes</div>
                                        <div class="card-body text-danger">
                                            <h5 class="card-title text-danger">PERMISO NO AUTORIZADO</h5>
                                            <p class="card-text">Usted no se encuentra autorizado para ingresar a este módulo</p>
                                        </div>
                                    </div>
                                </div>
                            
                            
                            <!-- LISTA DE CONVENIOS-->
                            <div  class="emailapp-emails-list" v-if="ListaLeads" >
                                <div class="nicescroll-bar" >
                      
           <hr>
            <div class="container" v-if="ListaLeads" >

             
                <!-- Row -->
             
                    <div class="col-xl-12" >
                        <section class="">
                           <div class="row" >
                                <div class="col-sm">
                                    <div class="table-wrap">
                                        <table id="datable_1"  class="table table-hover  responsive">
                                            <thead>
                                                <tr>
                                                    <th>Datos de los Leads</th>
                                                  
                                                
                                                </tr>
                                            </thead>
                                            <tbody >
                                            
                                            <!--tr v-for="(item,index) in  leads">
                                              <td>
                                              <a href="javascript:void(0);"  @click="MostrarLeads(item.id_leads)" class="media">
                                       
                                       <div class="media-body">
                                                       
         
                                               <div>
                                                   <div class="email-head">Fecha:  {{item.fecha}} </div>
                                                   <div class="email-subject"> Estado {{item.estado}}</div>
                                                    <div >
                                                       <p style="color:black">Telefono Cliente asociado: {{item.cliente.telefono}}</p>
                                                   </div>
                                               </div>
                                               <div>
                                                   <div class="last-email-details">
                                                    
                                                   </div>
                                                   </div>
                                           </div>
                                       </a>
                                       <div class="email-hr-wrap">
                                           <hr>
                                       </div>
                                              </td>
                                            </tr-->
                                             
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Datos de los Leads</th>
                                                    
                                                
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
              
                <!-- /Row -->

            </div>
            <hr>
            <!-- /Container -->

            <!-- Footer -->

            <!-- /Footer -->

                                
                                </div>
                            </div>
 
                           
                           
                        </div>






                        <div class="emailapp-right">
                            <header>
                                <a id="back_email_list" href="javascript:void(0)" class="back-email-list">
                                    <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                </a>
                                
                                <div class="email-options-wrap" v-show="habilitar">
          
                                 <a href="javascript:void(0)" @click="HabilitarComponente('infoCliente')" class=""><span class="feather-icon"><img src="Assets/dist/img/llamadas.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="HabilitarComponente('historial')" class=""><span class="feather-icon"><img src="Assets/dist/img/historial_.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="AlertEliminarCliente=true" class=""><span class="feather-icon"><img src="Assets/dist/img/borrar.png" width="30" ></span></a>
                                
                                    
                                </div>
                            </header>
                            
                            
                            <div class="email-body">
                                
                                      
                               
                                <div class="nicescroll-bar" >
                        
                              
                               <div class="col-sm-12" v-show="AlertEliminarCliente" >
                                            <div class="alert alert-warning alert-wth-icon alert-dismissible fade show" role="alert">
                                                <span class="alert-icon-wrap"><i class="zmdi zmdi-alert-circle-o"></i></span>
                                                <h5 class="alert-heading">¿Desea mover este Cliente a la papelera?</h5>
                                                <button @click="AlertEliminarCliente=false" class="btn btn-secondary mt-20 mr-5">Cancelar</button>
                                                <button @click="MoverAlaPapelera('procesar')"  class="btn btn-primary mt-20">Eliminar</button>
                                                <button @click="AlertEliminarCliente=false" type="button" class="close" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div> 
                                        </div>


    
                              <!--EDITAR CLIENTE-->
               
                              <div class="row" v-for="cliente in Cliente" v-if="editar_">
                              
                              <section class="col-lg-12  hk-sec-wrapper">
                                                      <h4 class="hk-sec-title mb-25"><img src="Assets/dist/img/editar.png" width="30" > Modificar Estado Del Leads</h4>
                                                      <div class="row">
                                                          <div class="col-12">
                                                              <div class="row" v-for="cliente in Cliente">
                                                                  <div class="col-md-12 col-sm-12">
      
                                                                      <form>
                                                                          
                                                                        
      
                                                                          
                                                                          <div class = "row">
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Id leads</label>
                                                                                  <input v-model="cliente.id" disabled class="form-control" type="number">
                                                                              </div>
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Nombre Cliente</label>
                                                                                  <input v-model="cliente.nombre" disabled class="form-control"  type="text">
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group">
                                                                                  <label >Email Del  Cliente</label>
                                                                                  <input v-model="cliente.email_cl" disabled class="form-control" type="text">
                                                                          </div>
      
                                                                          <div class = "row">
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Teléfono Del Cliente</label>
                                                                                  <input v-model="cliente.telefono_cl" disabled class="form-control" type="text">
                                                                              </div>
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Estado Del Leads</label>
                                                                                  <div class="col-lg-12">
                                                                                    <div class="input-grounp">
                                                                                        <select class="form-control custom-select" v-model="cliente.estado">
                                                                                                <option v-for="i in filtros" :value="i.nombre">{{i.nombre}}</option>
                                                                                        </select>
                                                                                    </div>
                                                                                  </div>
                                                                                               
                                                                              </div> 
                                                                             
                                                                          </div>
      
                                                                     
                                                                          <hr>
                                                                          <center><button @click="HabilitarComponente('lista')" class="btn btn-dark" type="button">Cancelar</button>
                                                                        <button @click.prevent="EditarEstadoLeads('editar')" class="btn btn-primary" type="button">Guardar Cambios</button></center>
      
                                                                      </form>
                                                  
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                              </section>
                                
      
      
                                                                   
                              </div>
      
                    <!-- INFORMACIÓN DEl ESPECIALISTA -->
                                    
                                </div>
                            </div>
                        </div>
                                
                                
                                
            








                            </div>
                           
                        </div>
                    </div>
                </div>
                <!-- /Row -->

            </div>



<!-- /Container -->



<!-- /Container -->

<script src="Controllers/js/ctrlLeads.js"></script>