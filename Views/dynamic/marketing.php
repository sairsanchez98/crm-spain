<?php ?>
<!-- Container -->


<div class="container-fluid" id ="marketing">
                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12 ">
                        <div class="emailapp-wrap">
            
                            <div class="email-box">
                            <div class="emailapp-left col-xs-12 card  card-refresh ">
                            <div class="refresh-container" :style="{ display:loaderLeft }">
                                <div class="loader-pendulums"></div>
                            </div>
                            <header>
                                <a @click="HistorialMarketing()" href="javascript:void(0)" class="emailapp-sidebar-move">
                                    <span class="feather-icon"><i data-feather="list"></i></span>
                                </a>
                                <span class=""> Marketing</span>
                                <a  @click="RegistrarCliente('form')" href="javascript:void(0)" class="email-compose" data-toggle="modal" data-target="#exampleModalEmail">
                                   
                                </a>
                            </header>
                           
                            <form role="search" class="email" v-show="ListaMarketing">
                            <div class="row">
                                <div class="col-lg-12">
                                <!--div class="form-group" style="margin:10px;">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="inputGroupSelect01">Filtros</label>
                                                </div>
                                                <select class="form-control custom-select" id="inputGroupSelect01"  @change="MostrarLeads(false)"  v-model="filtrar_estado">
                                                <option value="todos"  >Todos</option>
                                                  <option v-for="i in filtros" :value="i.nombre">{{i.nombre}}</option>
                                                </select>
                                            </div>
                                        </div-->
                                    <div class="input-grounp">
                                          
                                                <div class="form-group" style="margin:10px;">
                                            <div class="input-group">
                                              
                                            <div class="input-group-append">
                                                    <button class="btn btn-primary dropdown-toggle"   type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                    <div class="dropdown-menu">
                                                    <option style="cursor:pointer" @click="  filtrarEstado('todos')" >  Todos</option>
                                                    <option style="cursor:pointer" @click="  filtrarEstado('sms')" >  Sms</option>
                                                  <option style="cursor:pointer" @click="  filtrarEstado('email')" >email</option>
                                                  <option style="cursor:pointer" @click="  filtrarEstado('whatsapp')" >whatsapp</option>
                                                 
                                                   
                                                    
                                                    </div>
                                                </div>
                                                <input type="date" @change="filtrar()" v-model="fecha_inicio" class="form-control">
                                                <input type="date" @change="filtrar()" v-model="fecha_final" class="form-control">
                                              
                                            </div>
                                        </div>
                                               
                                    </div>
                                    </div>
                                <br> <br>
                                   
                                        
                                      
                                               <!--div class="input-group-prepend">
                                        <span class="feather-icon"><i data-feather="search"></i></span>
                                    </div>
                                    <input v-model="buscar" @keyup="Buscar()" type="text"  class="form-control" placeholder="Buscar..."-->
                                </div>
                               
                                
                            </form>

                            
                            <!-- LISTA DE CONVENIOS-->
                            <div  class="emailapp-emails-list" v-if="ListaClientes" >
                                <div class="nicescroll-bar" >

                                <hr>
                                    <div class="container" v-show="ListaClientes">

             
<!-- Row -->

    <div class="col-xl-12" >
        <section class="">
           <div class="row" >
                <div class="col-sm">
                    <div class="table-wrap">
                        <table id="datable_1"  class="table table-hover  responsive">
                            <thead>
                                <tr>
                                    <th>Datos de los Clientes</th>
                                  
                                
                                </tr>
                            </thead>
                            <tbody >
                            
                            <!--tr v-for="(item,index) in  leads">
                              <td>
                              <a href="javascript:void(0);"  @click="MostrarLeads(item.id_leads)" class="media">
                       
                       <div class="media-body">
                                       

                               <div>
                                   <div class="email-head">Fecha:  {{item.fecha}} </div>
                                   <div class="email-subject"> Estado {{item.estado}}</div>
                                    <div >
                                       <p style="color:black">Telefono Cliente asociado: {{item.cliente.telefono}}</p>
                                   </div>
                               </div>
                               <div>
                                   <div class="last-email-details">
                                    
                                   </div>
                                   </div>
                           </div>
                       </a>
                       <div class="email-hr-wrap">
                           <hr>
                       </div>
                              </td>
                            </tr-->
                             
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Datos de los Clientes</th>
                                    
                                
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>

<!-- /Row -->

</div>
        
                                    <!--div v-for="(item,index) in  ClientesQueMuestran">
                                    <a href="javascript:void(0);"  @click="MostrarMarketing(item.id)" class="media">
                                        <div class="media-body">
                                            <div>
                                                <div class="email-head">{{item.tipo}} </div>
                                                <div class="email-subject">{{item.mensaje}}</div>
                                                <div class="email-text">
                                                    <p> Enviado ha {{item.alcance}} cliente(s)</p>
                                                </div> 
                                            </div>
                                            <div>
                                                <div class="last-email-details">
                                                    <span class="badge badge-success badge-indicator"></span>
                                                    <p>Fecha envio</p>
                                                 <br></p> {{item.fecha_envio}}</p> 
                                                </div>
                                                   </div>
                                        </div>
                                    </a>
                                    <div class="email-hr-wrap">
                                        <hr>
                                    </div>
                                    </div-->
                                </div>
                            </div>

                            <div v-if="mensajeNoLista" style="height:400px;"  >
                                <div  class="col-sm">
                                <p style="color:red">No puedes ver La lista de usuarios</p>
                                </div>
                                
                            </div>

                     
                           
                           
                        </div>






                        <div class="emailapp-right">
                            <header>
                                <a id="back_email_list" href="javascript:void(0)" class="back-email-list">
                                    <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                </a>
                                
                                <div class="email-options-wrap">
                       

                                 <a href="javascript:void(0)" @click="EnviarMensajeCorreoMasivo('correo')" class=""><span class="feather-icon"><img src="Assets/dist/img/email.png" width="30" ></span>
                            
                                 <a href="javascript:void(0)" @click=" EnviarMensajeAlWhastappMasivo('whatsapp')" class=""><span class="feather-icon"><span class="feather-icon"><img src="Assets/dist/img/whatsapp.png" width="30" ></span></span>
                                  </a>
                                 <a href="javascript:void(0)"  @click="EnviarMensajeAlSmsMasivo('sms')" class=""><span class="feather-icon"><img src="Assets/dist/img/sms.png" width="30" ></span>
                                
                                </a>
                                <a href="javascript:void(0)"  v-if="infoMarketing" @click="AlertEliminarMarketing =true" class=""><span class="feather-icon"><img src="Assets/dist/img/borrar_.png" width="30" ></span></a>
                                </div>
                            </header>
                            
                            
                            <div class="email-body">
                                
                                       <!--ENVIAR MENSAJE AL CORREO DEL CLIENTE-->
                               
                                <div class="nicescroll-bar" >
                                     <!--ENVIAR MENSAJE AL CORREO DEL Clientes-->
                                     <div class="col-sm-12" v-show="AlertEliminarMarketing" >
                                            <div class="alert alert-warning alert-wth-icon alert-dismissible fade show" role="alert">
                                                <span class="alert-icon-wrap"><i class="zmdi zmdi-alert-circle-o"></i></span>
                                                <h5 class="alert-heading">¿Desea mover este Cliente a la papelera?</h5>
                                                <button @click="AlertEliminarMarketing=false" class="btn btn-secondary mt-20 mr-5">Cancelar</button>
                                                <button @click="MoverAlaPapelera"  class="btn btn-primary mt-20">Eliminar</button>
                                                <button @click="AlertEliminarMarketing=false" type="button" class="close" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div> 
                                        </div>

                    <section v-show="correo"  class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Enviar Mensaje Email</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Asunto</label>
                                                    <input v-model="asunto_mensaje" class="form-control"  type="text">
                                                </div>                           
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Mensaje</label>
                                                    <textarea class="form-control mt-15" v-model="mensaje_mensaje" rows="3" placeholder="Ingresa el mensaje"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                                <label>Seleccionar Filtro</label>
                                                <select class="form-control custom-select  mt-15" v-model="filtro_s">
                                                  <option  value ="todos">Todos</option>
                                                  <option v-for="i in filtros" :value="i.nombre">{{i.nombre}}</option>
                                                
                                                </select>
                                               </div> 
                                            </div>
                                            <hr>
                                            <button @click="EnviarMensajeCorreoMasivo('enviar')" class="btn btn-primary" type="button">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                  <!--ENVIAR MENSAJE AL CORREO DEL CLIENTE-->


                  <!--INFORMACIÓN CLIENTE-->
                                      <!-- INFORMACIÓN DEL CLIENTE  -->
                                      <div v-if="infoMarketing" >
                                        
        
                                        <div class="email-head">
                                            <div class="media">
                                                <div class="media-body">
                                                    


                                                <section class="hk-sec-wrapper">
                                                    <div class="row">
                                                        <div class="col-sm">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <ul class="list-group mb-15">
                                                                       
                                                                        
                                                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                            <label> ID: {{marketing_seleccionado[0].id}}</label>
                                                                        </li>
                                                                        <li class="list-group-item d-flex ">
                                                                        <label> MENSAJE:</label> <br> <br> <br>
                                                                          <textarea name=""style="width:500px; height:200px; " value="" id="" cols="50" rows="12">{{marketing_seleccionado[0].mensaje}}
                                                                        </textarea>   
                                                                        
                                                                        </li>
                                                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                            <label> CATEGORIA:{{marketing_seleccionado[0].categoria}} </label>
                                                                       </li>

                                                                       
                                                                      
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <ul class="list-group mb-15">
                                                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                            <label> ALCANCE:{{marketing_seleccionado[0].alcance}} </label>
                                                                       </li>
                                                                       <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                            <label>FECHA DE ENVIO: {{marketing_seleccionado[0].fechaenvio}} </label>
                                                                        </li>
                                                                    
                                                                        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="hr-light">
                    </div>
                  <!--INFORMACIÓN CLIENTE-->
                   <!--ENVIAR MENSAJE WHASTAPP-->
                   <section v-show="whatsapp"  class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Enviar Mensaje WhastApp</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Mensaje</label>
                                                    <textarea class="form-control mt-15" v-model="w_mensaje" rows="3" placeholder="Ingresa Tu mensaje"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                                <label>Seleccionar Filtro</label>
                                                <select class="form-control custom-select  mt-15" v-model="filtro_s">
                                                  <option value="todos">Todos</option>
                                                  <option v-for="i in filtros" :value="i.nombre">{{i.nombre}}</option>
                                                
                                                </select>
                                               </div> 
                                            </div>


                                            <hr>
                                            <button @click="EnviarMensajeAlWhastappMasivo('enviar')" class="btn btn-primary" type="button">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                    <!--ENVIAR MENSAJE WHASTAPP-->
                    <section v-show="MensajeSms"  class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Enviar Mensaje Sms</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form>
                                         

                                          
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Mensaje</label>
                                                    <textarea class="form-control mt-15" v-model="sms_mensaje" rows="3" placeholder="Ingresa Tu mensaje"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                                <label>Seleccionar Filtro</label>
                                                <select class="form-control custom-select  mt-15">
                                                  <option value="todos">Todos</option>
                                                  <option v-for="i in filtros" :value="i.nombre">{{i.nombre}}</option>
                                                </select>
                                               </div> 
                                            </div>


                                            <hr>
                                            <button @click="EnviarMensajeAlSmsMasivo('enviar')" class="btn btn-primary" type="button">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                    <!--ENVIAR MENSAJE SMS-->
                    <!--INFORMACIÓN DEL MARKETING-->
                       
                    <!--INFORMACION DEL MARKETING-->

                    <!-- INFORMACIÓN DEL CLIENTE  -->
                    <div v-show="infoCliente">
                        <div class="row" v-for="cliente in Cliente">
                            <div class="col-lg-12" >
                                <div class="card card-refresh">
                                    <div class="refresh-container" :style="{ display:loader }">
                                        <div class="loader-pendulums"></div>
                                    </div>
                                    <div class="card-header card-header-action">
                                        <h6>Información del cliente</h6>
                                        <div class="d-flex align-items-center card-action-wrap">
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-radio-button-off"></i>
                                            </a>
                                            <a href="#" class="inline-block full-screen">
                                                <i class="ion ion-md-expand"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            ID_CLIENTE
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control " v-model="cliente.id">
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Nombre
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.nombre">
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            DNI
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.dni">
                                                </div>
                                                 <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Email
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.email">
                                                </div>
                                                 <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Teléfono
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.telefono">
                                                </div>
                                                 <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Dirección
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.direccion">
                                                </div>
                                                 <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Registro
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.fecha_registro">
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    
                                    

                                    <!--  Servicios contratados -->
                            <div class="col-lg-12 ">
                                <div class="card card-refresh">
                                    <div class="refresh-container" :style="{ display:loader }">
                                        <div class="loader-pendulums"></div>
                                    </div>
                                    <div class="card-header card-header-action">
                                        <h6>Servicios contratados</h6>
                                        <div class="d-flex align-items-center card-action-wrap">
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-add"></i>
                                            </a>
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-radio-button-off"></i>
                                            </a>
                                            <a href="#" class="inline-block full-screen">
                                                <i class="ion ion-md-expand"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    
                                    </div>
                                </div>
                            </div>
                            <!-- Servicios contratados -->





                            <!--  Citas programadas-->
                            <div class="col-lg-4 ">
                                <div class="card card-refresh">
                                    <div class="refresh-container" :style="{ display:loader }">
                                        <div class="loader-pendulums"></div>
                                    </div>
                                    <div class="card-header card-header-action">
                                        <h6>Citas programadas</h6>
                                        <div class="d-flex align-items-center card-action-wrap">
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-add"></i>
                                            </a>
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-radio-button-off"></i>
                                            </a>
                                            <a href="#" class="inline-block full-screen">
                                                <i class="ion ion-md-expand"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    
                                    </div>
                                </div>
                            </div>
                            <!-- Citas programadas-->







                                    <!--  Historial del cliente -->
                            <div class="col-lg-8 ">
                                <div class="card card-refresh">
                                    <div class="refresh-container" :style="{ display:loader }">
                                        <div class="loader-pendulums"></div>
                                    </div>
                                    <div class="card-header card-header-action">
                                        <h6>Historial del cliente</h6>
                                        <div class="d-flex align-items-center card-action-wrap">
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-add"></i>
                                            </a>
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-radio-button-off"></i>
                                            </a>
                                            <a href="#" class="inline-block full-screen">
                                                <i class="ion ion-md-expand"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    
                                    </div>
                                </div>
                            </div>
                            <!-- Historial del cliente -->


                        </div>


                        </div>
                    <!-- INFORMACIÓN DEl ESPECIALISTA -->
                                    
                                </div>
                            </div>
                        </div>
                                
                                
                                
            








                            </div>
                           
                        </div>
                    </div>
                </div>
                <!-- /Row -->

            </div>

<script>
    /*
<div class="container mt-20" id ="clientes">


<!-- Row -->
<div class="row">
    <div class="col-xl-12">
        <section>
          <button class="btn btn-primary btn-wth-icon btn-sm" @click="HabilitarComponente('lista')"> 
            <span class="icon-label">
              <span class="feather-icon"><i data-feather="list"></i></span> 
            </span>
            <span class="btn-text">Registros de Marketing</span>
          </button>
           <button class="btn btn-primary btn-wth-icon btn-sm" @click="EnviarMensajeCorreoMasivo('correo')"> 
             <span class="icon-label">
               <span class="feather-icon"><i data-feather="zap"></i></span> 
             </span>
             <span class="btn-text">Enviar Email</span>
            </button>
            <button class="btn btn-primary btn-wth-icon btn-sm" @click=" EnviarMensajeAlWhastappMasivo('whatsapp')"> 
              <span class="icon-label">
                <span class="feather-icon"><i data-feather="x"></i></span> 
              </span>
              <span class="btn-text">Enviar WhastApp</span>
            </button>
            <button class="btn btn-primary btn-wth-icon btn-sm" @click="EnviarMensajeAlSmsMasivo('sms')"> 
               <span class="icon-label">
                  <span class="feather-icon"><i data-feather="x"></i></span> 
               </span>
               <span class="btn-text">Enviar Sms</span>
            </button>
            <div class="row mt-10">
                <div class="col-sm">
                    <!-- LISTA DE HISTORIAL MARKETING -->
                    <div v-show="ListaClientes" class="table-wrap hk-sec-wrapper" >          
                        <div class="card card-refresh">
                            <div class="refresh-container">
                           <div class="la-anim-1"></div>
                        </div>
							<h6 class="card-header">
                                
                                    <input type="text" placeholder="Buscar"  class="form-control" v-model="buscar" @keyup="Buscar()">
							</h6>
                            <div class="card-body pa-0">
               
                                <div class="table-wrap">
                                  <div class="table-responsive" style="height:400px; over-flow:scroll">
                                        <table class="table mb-0">
                                              <thead>
                                                 <tr>
                                                     <th>#</th>
                                                     <td>Id</td>
                                                     <th>Tipo</th>
                                                    <th>Mensaje</th>
                                                    <th>Alcance</th>
                                                    <th>Fecha</th>
                                                    <td>Eliminar</td>
                                                 </tr>
                                                </thead>
                                                <tbody>
                                                  <tr  v-for="(item,index) in  ClientesQueMuestran" >
                                                     <th scope="row">{{index+1}}</th>
                                                     <td> {{ item.id }}</td>
                                                     <td></td>
                                                     <td></td>
                                                     <!--td> {{ item.mensaje }}</td-->
                                                     <td> {{ item.alcance }}</td>
                                                     <td> {{ item.fecha_envio }}</td>
                                                     <td> 
                                                        <span class="badge badge-danger" @click="EliminarCliente(item.id)"  style="cursor: pointer;" >Eliminar</span> </td>                                          
                                                     </tr>
                                                                
                                                </tbody>
                                         </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                
             </div>
                    <!-- LISTA DE CLIENTES -->
<!--Modal eliminar-->











                 <!--ENVIAR MENSAJE AL CORREO DEL Clientes-->

                    <section v-show="correo"  class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Enviar Mensaje Email</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Asunto</label>
                                                    <input v-model="asunto_mensaje" class="form-control"  type="text">
                                                </div>                           
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Mensaje</label>
                                                    <textarea class="form-control mt-15" v-model="mensaje_mensaje" rows="3" placeholder="Ingresa el mensaje"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                                <label>Seleccionar Filtro</label>
                                                <select class="form-control custom-select  mt-15" v-model="filtro_s">
                                                  <option selected>Escoge</option>
                                                  <option v-for="i in filtros" :value="i.nombre">{{i.nombre}}</option>
                                                
                                                </select>
                                               </div> 
                                            </div>
                                            <hr>
                                            <button @click="EnviarMensajeCorreoMasivo('enviar')" class="btn btn-primary" type="button">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                  <!--ENVIAR MENSAJE AL CORREO DEL CLIENTE-->
                   <!--ENVIAR MENSAJE WHASTAPP-->
                   <section v-show="whatsapp"  class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Enviar Mensaje WhastApp</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Mensaje</label>
                                                    <textarea class="form-control mt-15" v-model="w_mensaje" rows="3" placeholder="Ingresa Tu mensaje"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                                <label>Seleccionar Filtro</label>
                                                <select class="form-control custom-select  mt-15" v-model="filtro_s">
                                                  <option selected>Escoge</option>
                                                  <option v-for="i in filtros" :value="i.nombre">{{i.nombre}}</option>
                                                
                                                </select>
                                               </div> 
                                            </div>


                                            <hr>
                                            <button @click="EnviarMensajeAlWhastappMasivo('enviar')" class="btn btn-primary" type="button">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                    <!--ENVIAR MENSAJE WHASTAPP-->
                    <section v-show="MensajeSms"  class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Enviar Mensaje Sms</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form>
                                         

                                          
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Mensaje</label>
                                                    <textarea class="form-control mt-15" v-model="sms_mensaje" rows="3" placeholder="Ingresa Tu mensaje"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                                <label>Seleccionar Filtro</label>
                                                <select class="form-control custom-select  mt-15">
                                                  <option selected>Escoge</option>
                                                  <option value="1">One</option>
                                                  <option value="2">Two</option>
                                                  <option value="3">Three</option>
                                                </select>
                                               </div> 
                                            </div>


                                            <hr>
                                            <button @click="EnviarMensajeAlSmsMasivo('enviar')" class="btn btn-primary" type="button">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                    <!--ENVIAR MENSAJE SMS-->



                    <!-- INFORMACIÓN DEL CLIENTE  -->
                        <div class="row" v-show="infoCliente" v-for="cliente in Cliente">
                            <div class="col-lg-4" >
                                <div class="card card-refresh">
                                    <div class="refresh-container" :style="{ display:loader }">
                                        <div class="loader-pendulums"></div>
                                    </div>
                                    <div class="card-header card-header-action">
                                        <h6>Información del cliente</h6>
                                        <div class="d-flex align-items-center card-action-wrap">
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-radio-button-off"></i>
                                            </a>
                                            <a href="#" class="inline-block full-screen">
                                                <i class="ion ion-md-expand"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            ID_CLIENTE
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.id">
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Nombre
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.nombre">
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            DNI
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.dni">
                                                </div>
                                                 <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Email
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.email">
                                                </div>
                                                 <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Teléfono
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.telefono">
                                                </div>
                                                 <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Dirección
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.direccion">
                                                </div>
                                                 <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon3">
                                                            Registro
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" v-model="cliente.fecha_registro">
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    
                                    

                                    <!--  Servicios contratados -->
                            <div class="col-lg-8 ">
                                <div class="card card-refresh">
                                    <div class="refresh-container" :style="{ display:loader }">
                                        <div class="loader-pendulums"></div>
                                    </div>
                                    <div class="card-header card-header-action">
                                        <h6>Servicios contratados</h6>
                                        <div class="d-flex align-items-center card-action-wrap">
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-add"></i>
                                            </a>
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-radio-button-off"></i>
                                            </a>
                                            <a href="#" class="inline-block full-screen">
                                                <i class="ion ion-md-expand"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    
                                    </div>
                                </div>
                            </div>
                            <!-- Servicios contratados -->





                            <!--  Citas programadas-->
                            <div class="col-lg-4 ">
                                <div class="card card-refresh">
                                    <div class="refresh-container" :style="{ display:loader }">
                                        <div class="loader-pendulums"></div>
                                    </div>
                                    <div class="card-header card-header-action">
                                        <h6>Citas programadas</h6>
                                        <div class="d-flex align-items-center card-action-wrap">
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-add"></i>
                                            </a>
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-radio-button-off"></i>
                                            </a>
                                            <a href="#" class="inline-block full-screen">
                                                <i class="ion ion-md-expand"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    
                                    </div>
                                </div>
                            </div>
                            <!-- Citas programadas-->







                                    <!--  Historial del cliente -->
                            <div class="col-lg-8 ">
                                <div class="card card-refresh">
                                    <div class="refresh-container" :style="{ display:loader }">
                                        <div class="loader-pendulums"></div>
                                    </div>
                                    <div class="card-header card-header-action">
                                        <h6>Historial del cliente</h6>
                                        <div class="d-flex align-items-center card-action-wrap">
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-add"></i>
                                            </a>
                                            <a href="#" class="inline-block refresh mr-15">
                                                <i class="ion ion-md-radio-button-off"></i>
                                            </a>
                                            <a href="#" class="inline-block full-screen">
                                                <i class="ion ion-md-expand"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    
                                    </div>
                                </div>
                            </div>
                            <!-- Historial del cliente -->





                        </div>
                    <!-- INFORMACIÓN DEl ESPECIALISTA -->





                </div>
            </div>
        </section>
     
    </div>
</div>
<!-- /Row -->

</div>*/
</script>
<!-- /Container -->
<link href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
<script src="Controllers/js/ctrlMarketing.js"></script>