<?php ?>

<!-- Main Content -->
            <!-- Container -->
            <div class="container-fluid" id ="historial">
                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="emailapp-wrap">
                            <div class="email-box">
                            <div class="emailapp-left card  card-refresh ">
                            <div class="refresh-container" :style="{ display:loaderLeft }">
                                <div class="loader-pendulums"></div>
                            </div>
                            <header>
                                <a @click="HabilitarComponente('lista')" href="javascript:void(0)" class="emailapp-sidebar-move">
                                    <span class="feather-icon"><i data-feather="list"></i></span>
                                </a>
                                <span class="">Historial de los Agentes</span>
                                <a  href="javascript:void(0)" class="email-compose" >
                                
                                </a>

                            </header>
                           
                            <form role="search" class="email-search" v-show="ListaAgentes">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="feather-icon"><i data-feather="search"></i></span>
                                    </div>
                                    <input v-model="buscar" @keyup="Buscar()" type="text"  class="form-control" placeholder="Buscar...">
                                </div>
                            </form>

                            
                            <!-- LISTA DE CONVENIOS-->
                            <div v-show="ListaAgentes" class="emailapp-emails-list" >
                                <div class="nicescroll-bar" >
                                    <div v-for="(agente, index) in agentes">
                                    <a href="javascript:void(0);" @click="MostrarAgentes(agente.id)" class="media">
                                        <div class="media-body">
                                            <div>
                                                <div class="email-head">{{agente.nombre}} </div>
                                                <div class="email-subject">{{agente.usuario}}</div>
                                                <!-- <div class="email-text">
                                                    <p>So how did the classical Latin become so incoherent? According to McClintock.</p>
                                                </div> -->
                                            </div>
                                            <div>
                                                <div class="last-email-details">
                                                    <span class="badge badge-success badge-indicator"></span>
                                                    -----
                                                </div>
                                                <!-- <span class="email-star"><span class="feather-icon"><i data-feather="star"></i></span></span> -->
                                            </div>
                                        </div>
                                    </a>
                                    <div class="email-hr-wrap">
                                        <hr>
                                    </div>
                                    </div>
                                </div>
                            </div>



                           
                           
                        </div>






                        <div class="emailapp-right">
                            <header>
                                <a id="back_email_list" href="javascript:void(0)" class="back-email-list">
                                    <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                </a>
                                <div class="email-options-wrap">
    
                                </div>
                            </header>
                            
                            
                            <div class="email-body">
                            <div class="nicescroll-bar" >


                   <!--DETALLES HISORIAL DEL CLIENTE-->
               
                   <div class="row"v-if="detalle">

                              <section class="col-lg-12  hk-sec-wrapper">
                                                      <h4 class="hk-sec-title mb-25"><img src="Assets/dist/img/editar.png" width="30" > Detalles del Historial--  {{asunto}}</h4>
                                                      <div class="row">
                                                          <div class="col-12">
                                                              <div class="row">
                                                                  <div class="col-md-12 col-sm-12">
      
                                                                      <form>
                                                                          
                                                                          <hr>
                                                                          <div class="media-body"  >
                                                                            <div>
                                                                                <div class="email-head .pb-30" v-if="detalles.cliente!=''"> <p> <b class="font-weight-500 text-dark text-capitaliz ">Cliente:</b> <strong> {{detalles.cliente}}</strong></p>  </div>
                                                                               
                                                                                <div class="email-subject .pb-30" v-if="detalles.mensaje!=''"><p><b class="font-weight-500 text-dark text-capitaliz">Mensaje enviado:</b><strong> {{detalles.mensaje}}</strong></p>  </div>
                                                                               
                                                                                <div class="email-subject .pb-30" v-if="detalles.telefono!=''"><p><b class="font-weight-500 text-dark text-capitaliz"> Telefono Del Cliente:</b><strong> {{detalles.telefono}}</strong>   </p> </div>
                                                                             
                                                                                <div class="email-subject .pb-30" v-if="detalles.titulo!=''"> <p><b class="font-weight-500 text-dark text-capitaliz">Titulo o asunto:<b><strong> {{detalles.titulo}}</strong></p></div>
                                                                                
                                                                                <div class="email-subject .pb-30" v-if="detalles.fecha!=''"><p><b class="font-weight-500 text-dark text-capitaliz">Fecha:</b><strong> {{detalles.fecha}}</strong></p></div>
                                                                             
                                                                                <div class="email-subject .pb-30" v-if="detalles.estado!=''"><p><b class="font-weight-500 text-dark text-capitaliz">Estado Del Lead:</b><strong> {{detalles.estado}}   </strong></p> </div>
                                                                               
                                                                                <div class="email-subject .pb-30" v-if="detalles.email!=''"><p><b class="font-weight-500 text-dark text-capitaliz">El email del cliente:</b><strong> {{detalles.email}}</strong> </p> </div>
                                                                            </div>
                                                                            <div>
                                                                             
                                                                                <!-- <span class="email-star"><span class="feather-icon"><i data-feather="star"></i></span></span> -->
                                                                            </div>
                                                                            <button class="btn btn-xs btn-primary ml-15 w-sm-100p" @click.prevent="HabilitarComponente('infoAgente')">Cerrar Detalles</button>
												
                                                                        </div>
                                                                                                    
                                                                          <hr>
                                                                      
                                                                      </form>
                                                  
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                              </section>
                                
      
      
                                                                   
                              </div>
                 <!--DETALLES HISORIAL DEL agente-->          

          <div  v-show="infoAgente" v-for="ag in agente">          
              <div class="email-subject-head">
                 <h4>Agente: {{ag.nombre}} | {{ag.usuario}}</h4> 
              </div>
               <hr class="mt-10 mb-20">
              <div v-show="infoAgente">
                  <div class="email-head">
                      <div class="media">
                          <div class="media-body">                                                              
                           <div class="row">
                                <div class="col-xl-12">
                                    <div class="card card-lg">
                                        <h6 class="card-header">
                                            Historial
                                        </h6>							
                                        <div class="card-body">
                                            <div class="user-activity">
                                                <div class="media" v-for="(i,index) in historial">
                                                    <div class="media-img-wrap">
                                                        <div class="avatar avatar-sm">
                                                        <img src="Assets/dist/img/historial.svg" width="30" alt="Historial" class="avatar-img rounded-circle" >
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="media-body" >
                                                        <div>
                                                            <span class="d-block mb-5"><span class="font-weight-500 text-dark text-capitalize" id="cursor" @click="buscarInfo(i)" >{{i.movimiento}}</span>
                                                            <span class="d-block font-13 mb-30">fecha : {{i.fecha_registro}}</span>
                                                        </div>
                                                    
                        
                                                    
                                                    </div>
                                                </div>
                    
                                            </div>
                                        </div>
                                    </div>
                        
                                </div>
                            </div>  
                        </div>                       
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-light">
                                        </div>
                                </div>
                            </div>
                            </div>
                        </div>
                                
                                
                                
            








                            </div>
                           
                        </div>
                    </div>
                </div>
                <!-- /Row -->

            </div>
            <!-- /Container -->
        
<!-- /Main Content -->







<script src="Controllers/js/ctrlHistorial.js"></script>


