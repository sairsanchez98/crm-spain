  <!-- Main Content -->
  <link href="Assets/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" type="text/css" />
       
            <!-- /Container -->




            
<!-- Container -->
<div class="container-fluid" id ="citas">
                <!-- Row -->
                <div class="modal fade"  id="exampleModalEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalEmail" aria-hidden="true">
                                <div class="modal-dialog modal-lg col-lg-8" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-smoke-dark-2">
                                        <button  @click="programar=false, edit=true" class="btn btn-info btn-rounded">Editar</button> &nbsp
                                        <button   @click="programar=true, edit=false" class="btn btn-success btn-rounded">Enviar Evento</button>
                                
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                        <form v-if="edit">      
                                        <div class="row">
                                        <div class="col-lg-6">     
                                              <label for="id">Telefono Cliente</label>
                                              <input type="number" disabled v-model="telefono_" class="form-control" id="id" >
                                        
                                            </div>
                                            <div class="col-lg-6">     
                                              <label for="id">Fecha De la Cita</label>
                                              <input type="date" v-model="fecha_" class="form-control" id="id" >
                                           </div> 
                                           <div class="col-lg-6">     
                                              <label for="id">Hora de la cita</label>
                                              <input type="time" v-model="hora_cita_" class="form-control" id="id" >
                                           </div> 
                                           <div class="col-lg-6">     
                                              <label for="id">Duración de la Cita</label>
                                              <input type="time" v-model="hora_duracion_"  class="form-control" id="id" >
                                           </div> 
                                           <div class="col-lg-6">     
                                              <label for="id">Estado</label>
                                              <input type="text" v-model="estado_cita"  class="form-control" id="id" >
                                           </div> 
                                        </div>          
                                 
                                          
                                          
                                           <div class="form-group">     
                                              <label for="id">Asunto De la Cita</label>
                                              <textarea type="text" v-model="asunto_"  class="form-control" id="id" placeholder="Tema de la cita"></textarea>
                                           </div> 
                                          <button  @click.prevent="EditarCita()" class="btn btn-primary">Guardar Cambios</button>
                                         </form>
                                         <form v-if="programar" >   
                                                      
                                        <div class="form-group">     
                                              <label for="id">Telefono Cliente</label>
                                              <input type="number" disabled v-model="telefono_" class="form-control" id="id" >
                                           </div> 
                                           <div class="row">
                                                    <div class="col-lg-6">     
                                                        <label for="id">Fecha de de envio del evento</label>
                                                        <input type="date" v-model="fecha_cita_programada" class="form-control" id="id" >
                                                    </div>
                                                    <div class="col-lg-6">     
                                                        <label for="id">Hora de envio del evento</label>
                                                        <input type="time" v-model="hora_cita_programada" class="form-control" id="id" >
                                                    </div> 
                                                    
                                                    
                                           </div>  
                                           <br>
                                           <div class="row">
                                           <div class="col-lg-12">     
                                                        <label for="id">Url de envio</label>

                                                        <input type="text" v-model="url" class="form-control" id="id" >

                                                    </div>
                                           </div>
                                                <br>                                 
                                  
                                      
                                          <button  @click.prevent="CitaProgramada()" class="btn btn-primary">Programar evento</button>
                                         </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="emailapp-wrap">
            
                            <div class="email-box">
                            <div class="emailapp-left card  card-refresh ">
                            <div class="refresh-container" :style="{ display:loaderLeft }">
                                <div class="loader-pendulums"></div>
                            </div>
                            <header>
                                <a @click="HabilitarComponente('lista')" href="javascript:void(0)" class="emailapp-sidebar-move">
                             
                                </a>
                                <span class="">Crear Citas</span>
                                <a  @click="RegistrarCliente('form')" href="javascript:void(0)" class="email-compose" data-toggle="modal" data-target="#exampleModalEmail">
                             
                                </a>
                            </header>
                           
                         
                     
                            
                            <!-- LISTA DE CONVENIOS-->
                       
                            <!-- FORMULARIO DE REGISTRO  -->
                            <section  class="hk-sec-wrapper" >
                               
                                <div class="row">
                                    <div class="col-sm">
                                
                                    <form >               
                    
                                     <div class="row">
                        
                                                <div class="col-md-5 form-group">
                                                    <label>Nombre</label>
                                                    <input v-model="nombre" class="form-control"  type="text">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label>DNI</label>
                                                    <input v-model="dni" class="form-control"  type="text">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5 form-group">
                                                    <label>E-mail</label>
                                                    <input v-model="email" class="form-control"  type="email">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label>Teléfono</label>
                                                    <input v-model="telefono"  class="form-control"   type="number">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Dirección</label>
                                                    <input v-model="direccion" class="form-control"  type="text">
                                                </div>
                                            </div>
                                             
                                            
                                          
                                     
                                           <div class="form-group">     
                                              <label for="id">Fecha De la Cita</label>
                                              <input type="date" v-model="fecha" class="form-control" id="id" >
                                           </div> 
                                           <div class="form-group">     
                                              <label for="id">Hora de la cita</label>
                                              <input type="time" v-model="hora_cita" class="form-control" id="id" >
                                           </div> 
                                           <div class="form-group">     
                                              <label for="id">Duración de la Cita</label>
                                              <input type="time" v-model="hora_duracion"  class="form-control" id="id" >
                                             
                                           </div> 
                                           <div class="form-group">     
                                              <label for="id">Asunto De la Cita</label>
                                              <input type="text" v-model="asunto"  class="form-control" id="id" placeholder="Tema de la cita">
                                           </div> 
                                          <input  @click.prevent="EnviarCita()" type="button" class="btn btn-primary" value="Guardar Cita">
                                         </form>
                                    </div>
                                </div>
                            </section>
                                <!--FORMULARIO REGISTRO-->
                           
                           
                        </div>






                        <div class="emailapp-right">
                            <header>
                                <a id="back_email_list" href="javascript:void(0)" class="back-email-list">
                                    <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                </a>
                                
             
                            </header>
                            
                            
                            <div class="email-body">
                           
                               
                                <div class="nicescroll-bar" >
                                <div class="calendar-wrap" v-if="Calendario">
                                <div id="calendar"></div>
                            </div>
                                 
                               <hr>
                        
                                    
                                </div>
                            </div>
                        </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
                <!-- /Row -->

            </div>


      
<script src="Assets/vendors/moment/min/moment.min.js"></script>
<script src="Assets/vendors/jquery-ui.min.js"></script>
<script src="Assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="Assets/vendors/fullcalendar/dist/locale/es.js"></script>
<!-- <script src="dist/js/fullcalendar-data.js"></script> -->




<!-- Pickr JavaScript -->
<script src="Assets/vendors/pickr-widget/dist/pickr.min.js"></script>
<script src="Assets/dist/js/pickr-data.js"></script>

<!-- Daterangepicker JavaScript -->
<script src="Assets/vendors/moment/min/moment.min.js"></script>
<script src="Assets/vendors/daterangepicker/daterangepicker.js"></script>

<script src="Controllers/js/ctrlCitas.js"></script>

 
    