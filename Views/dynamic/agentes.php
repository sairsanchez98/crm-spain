<?php ?>

<!-- Main Content -->
            <!-- Container -->
            <div class="container-fluid" id ="agentes">
                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="emailapp-wrap">
                            <!-- <div class="emailapp-sidebar">
                                <div class="nicescroll-bar">
                                    <div class="emailapp-nav-wrap">
                                        <a id="close_emailapp_sidebar" href="javascript:void(0)" class="close-emailapp-sidebar">
                                            <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                        </a>
                                        <ul class="nav flex-column mail-category">
                                            <li class="nav-item active">
                                                <a class="nav-link" href="javascript:void(0);">Inbox<span class="badge badge-primary">50</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:void(0);">Sent mail</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:void(0);">Important</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:void(0);">Draft<span class="badge badge-secondary">5</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:void(0);">Trash</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:void(0);">Chat</a>
                                            </li>
                                        </ul>
                                        <button type="button" class="btn btn-primary btn-block mt-20 mb-20" data-toggle="modal" data-target="#exampleModalEmail">
                                            Compose email
                                        </button>
                                        <ul class="nav flex-column mail-labels">
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:void(0);"><span class="badge badge-primary badge-indicator"></span>clients</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:void(0);"><span class="badge badge-danger badge-indicator"></span>personal</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:void(0);"><span class="badge badge-warning badge-indicator"></span>office</a>
                                            </li>
                                        </ul>
                                        <hr>
                                        <ul class="nav flex-column mail-settings">
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:void(0);"><i class="zmdi zmdi-settings"></i>settings</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->
                            <div class="email-box">
                            <div class="emailapp-left card  card-refresh ">
                            <div class="refresh-container" :style="{ display:loaderLeft }">
                                <div class="loader-pendulums"></div>
                            </div>
                            <header>
                                <a @click="HabilitarComponente('lista')" href="javascript:void(0)" class="emailapp-sidebar-move">
                                    <span class="feather-icon"><i data-feather="list"></i></span>
                                </a>
                                <span class="">Agentes</span>
                                <a  @click="RegistrarAgente('form')" href="javascript:void(0)" class="email-compose" data-toggle="modal" data-target="#exampleModalEmail">
                                    <span class="feather-icon"><i data-feather="plus"></i></span>
                                </a>
                            </header>
                           
                            <form role="search" class="email-search" v-show="ListaAgentes">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="feather-icon"><i data-feather="search"></i></span>
                                    </div>
                                    <input v-model="buscar" @keyup="Buscar()" type="text"  class="form-control" placeholder="Buscar...">
                                </div>
                            </form>

                            
                            <!-- LISTA DE CONVENIOS-->
                            <div v-show="ListaAgentes" class="emailapp-emails-list" >
                                <div class="nicescroll-bar" >
                                    <div v-for="(agente, index) in agentes">
                                    <a href="javascript:void(0);" @click="MostrarAgentes(agente.id)" class="media">
                                        <div class="media-body">
                                            <div>
                                                <div class="email-head">{{agente.nombre}} </div>
                                                <div class="email-subject">{{agente.usuario}}</div>
                                                <!-- <div class="email-text">
                                                    <p>So how did the classical Latin become so incoherent? According to McClintock.</p>
                                                </div> -->
                                            </div>
                                            <div>
                                                <div class="last-email-details">
                                                    <span class="badge badge-success badge-indicator"></span>
                                                    -----
                                                </div>
                                                <!-- <span class="email-star"><span class="feather-icon"><i data-feather="star"></i></span></span> -->
                                            </div>
                                        </div>
                                    </a>
                                    <div class="email-hr-wrap">
                                        <hr>
                                    </div>
                                    </div>
                                </div>
                            </div>



                            <!-- FORMULARIO DE REGISTRO  -->
                            <div v-show="formRegistrarAgentes" class="row mr-15 ml-15">
                                <div class="col-sm mb-20">
                                    <h5 class="hk-sec-title mt-10 mb-10">Registrar agente</h5>
                                    <form>
                                        
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input v-model="nombre" class="form-control"  type="text">
                                            </div>
                                            <div class="form-group">
                                                <label>Usuario</label>
                                                <input v-model="usuario" class="form-control"  type="text">
                                            </div>
                                            <div class="form-group">
                                                <label>Contraseña</label>
                                                <input v-model="contrasena" class="form-control"  type="password">
                                            </div>
                                        
                                        <hr>
                                        <button @click="RegistrarAgente('Procesar')" class="btn btn-primary" type="button">Registrar</button>
                                    </form>
                                </div>
                            </div>
                           
                           
                        </div>






                        <div class="emailapp-right">
                            <header>
                                <a id="back_email_list" href="javascript:void(0)" class="back-email-list">
                                    <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                </a>
                                <div class="email-options-wrap">
                                    <a href="javascript:void(0)" @click="HabilitarComponente('FormEditarPaciente')" class=""><span class="feather-icon"><i data-feather="edit"></i></span></a>
                                    <a href="javascript:void(0)" @click="MoverAlaPapelera('Alert')" class=""><span class="feather-icon"><i data-feather="trash"></i></span></a>
                                </div>
                            </header>
                            
                            
                            <div class="email-body">
                            <div class="nicescroll-bar" >
                                <div  v-show="infoAgente" v-for="ag in agente">
                                        <div class="email-subject-head">
                                            <h4>Agente: {{ag.nombre}} | {{ag.usuario}}
                                            </h4> 
                                        </div>
                                        <hr class="mt-10 mb-20">

                                        <!-- Alerta de eliminar -->
                                        <div class="col-sm-12"  >
                                            <!-- <div class="alert alert-warning alert-wth-icon alert-dismissible fade show" role="alert">
                                                <span class="alert-icon-wrap"><i class="zmdi zmdi-alert-circle-o"></i></span>
                                                <h5 class="alert-heading">¿Desea mover este tratamiento a la papelera?</h5>
                                                <button @click="AlertEliminarConv=false" class="btn btn-secondary mt-20 mr-5">Cancelar</button>
                                                <button @click="MoverAlaPapelera('procesar')"  class="btn btn-primary mt-20">Eliminar</button>
                                                <button @click="AlertEliminarConv=false" type="button" class="close" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div> -->
                                        </div>
                                        



                                       <!-- Editar tratamiento -->
                                        <!-- <div class="col-md-12" v-show="formEditarRemitente">
                                            
                                        <section class="hk-sec-wrapper">
                                                <h4 class="hk-sec-title mb-25"><i class="fa fa-edit"></i> Modificar paciente</h4>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12">

                                                             
                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </section>
                                        </div> -->
                                        
                                        

                                        <div v-show="infoAgente">
                                            <div class="email-head">
                                                <div class="media">
                                                    <div class="media-body">
                                                        


                                                    <section class="hk-sec-wrapper ">
                                                            <div class="col-12">
                                                                <div class="accordion accordion-type-2 " id="accordion_2">
                                                                    <div class="card">
                                                                        <div class="card-header d-flex justify-content-between activestate">
                                                                            <a role="button" data-toggle="collapse" href="#collapse_1i" aria-expanded="true">General</a>
                                                                        </div>
                                                                        <div id="collapse_1i" class="collapse show" data-parent="#accordion_2" role="tabpanel">
                                                                            <div class="card-body pa-15"> 
                                                                                <div class="container">
                                                                                    <div class="row">
                                                                                        
                                                                                        <div v-for="permisosAgente in permisosAgentes">
                                                                                            <div class="col-md-12 mt-15" >
                                                                                                <div v-if="permisosAgente.categoria == 'general' " class="custom-control custom-checkbox checkbox-teal">
                                                                                                    <input type="checkbox" class="custom-control-input" :id="permisosAgente.id" v-model="permisosAgente.activado">
                                                                                                    <label class="custom-control-label" :for="permisosAgente.id">{{permisosAgente.permiso}}  </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="card " >
                                                                        <div class="card-header d-flex justify-content-between">
                                                                            <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_2i" aria-expanded="false">Permisos Clientes</a>
                                                                        </div>
                                                                        <div id="collapse_2i" class="collapse" data-parent="#accordion_2">
                                                                            <div class="card-body pa-15 nicescroll-bar"> 
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    
                                                                                    <!--div v-for="permisosAgente in permisosAgentes">
                                                                                        <div class="col-md-12 mt-15" >
                                                                                            <div v-if="permisosAgente.categoria == 'clientes' " class="custom-control custom-checkbox checkbox-teal">
                                                                                                <input type="checkbox" class="custom-control-input" id="permisosAgente.id" v-model="permisosAgente.activado">
                                                                                                <label class="custom-control-label" for="permisosAgente.id">{{permisosAgente.permiso}}  </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div-->
                                                                                    <div class="col-md-12"  v-if="permisosAgente.categoria == 'clientes' " v-for="permisosAgente in permisosAgentes" >
                                                                            <ul class="list-group " v-if="permisosAgente.categoria == 'clientes' " >
                                                                                <li  class="list-group-item d-flex justify-content-between align-items-center">
                                                                                    <div class="custom-control custom-checkbox">
                                                                                        <input v-model="permisosAgente.activado" type="checkbox" class="custom-control-input" :id="permisosAgente.id">
                                                                                        <label class="custom-control-label" :for="permisosAgente.id">{{permisosAgente.permiso}}</label>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card " >
                                                                        <div class="card-header d-flex justify-content-between">
                                                                            <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_3" aria-expanded="false">Permisos Leads</a>
                                                                        </div>
                                                                        <div id="collapse_3" class="collapse" data-parent="#accordion_2">
                                                                            <div class="card-body pa-15 nicescroll-bar"> 
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    
                                                                                    <!--div v-for="permisosAgente in permisosAgentes">
                                                                                        <div class="col-md-12 mt-15" >
                                                                                            <div v-if="permisosAgente.categoria == 'clientes' " class="custom-control custom-checkbox checkbox-teal">
                                                                                                <input type="checkbox" class="custom-control-input" id="permisosAgente.id" v-model="permisosAgente.activado">
                                                                                                <label class="custom-control-label" for="permisosAgente.id">{{permisosAgente.permiso}}  </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div-->
                                                                                    <div class="col-md-12"  v-if="permisosAgente.categoria == 'leads' " v-for="permisosAgente in permisosAgentes" >
                                                                            <ul class="list-group " v-if="permisosAgente.categoria == 'leads' " >
                                                                                <li  class="list-group-item d-flex justify-content-between align-ite
                                                                                ms-center">
                                                                                    <div class="custom-control custom-checkbox">
                                                                                        <input v-model="permisosAgente.activado" type="checkbox" class="custom-control-input" :id="permisosAgente.id">
                                                                                        <label class="custom-control-label" :for="permisosAgente.id">{{permisosAgente.permiso}}</label>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card " >
                                                                        <div class="card-header d-flex justify-content-between">
                                                                            <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_4" aria-expanded="false">Permisos Marketing</a>
                                                                        </div>
                                                                        <div id="collapse_4" class="collapse" data-parent="#accordion_2">
                                                                            <div class="card-body pa-15 nicescroll-bar"> 
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    
                                                                                    <!--div v-for="permisosAgente in permisosAgentes">
                                                                                        <div class="col-md-12 mt-15" >
                                                                                            <div v-if="permisosAgente.categoria == 'clientes' " class="custom-control custom-checkbox checkbox-teal">
                                                                                                <input type="checkbox" class="custom-control-input" id="permisosAgente.id" v-model="permisosAgente.activado">
                                                                                                <label class="custom-control-label" for="permisosAgente.id">{{permisosAgente.permiso}}  </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div-->
                                                                                    <div class="col-md-12"  v-if="permisosAgente.categoria == 'marketing' " v-for="permisosAgente in permisosAgentes" >
                                                                            <ul class="list-group " v-if="permisosAgente.categoria == 'marketing' " >
                                                                                <li  class="list-group-item d-flex justify-content-between align-items-center">
                                                                                    <div class="custom-control custom-checkbox">
                                                                                        <input v-model="permisosAgente.activado" type="checkbox" class="custom-control-input" :id="permisosAgente.id">
                                                                                        <label class="custom-control-label" :for="permisosAgente.id">{{permisosAgente.permiso}}</label>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card " >
                                                                        <div class="card-header d-flex justify-content-between">
                                                                            <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_5" aria-expanded="false">Permisos Citas</a>
                                                                        </div>
                                                                        <div id="collapse_5" class="collapse" data-parent="#accordion_2">
                                                                            <div class="card-body pa-15 nicescroll-bar"> 
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    
                                                                                    <!--div v-for="permisosAgente in permisosAgentes">
                                                                                        <div class="col-md-12 mt-15" >
                                                                                            <div v-if="permisosAgente.categoria == 'clientes' " class="custom-control custom-checkbox checkbox-teal">
                                                                                                <input type="checkbox" class="custom-control-input" id="permisosAgente.id" v-model="permisosAgente.activado">
                                                                                                <label class="custom-control-label" for="permisosAgente.id">{{permisosAgente.permiso}}  </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div-->
                                                                                    <div class="col-md-12"  v-if="permisosAgente.categoria == 'citas' " v-for="permisosAgente in permisosAgentes" >
                                                                            <ul class="list-group " v-if="permisosAgente.categoria == 'citas' " >
                                                                                <li  class="list-group-item d-flex justify-content-between align-items-center">
                                                                                    <div class="custom-control custom-checkbox">
                                                                                        <input v-model="permisosAgente.activado" type="checkbox" class="custom-control-input" :id="permisosAgente.id">
                                                                                        <label class="custom-control-label" :for="permisosAgente.id">{{permisosAgente.permiso}}</label>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                              

                                    <div v-for="ag in agente">
                                        <button @click="GuardarCambios(ag.id)"  class="btn btn-primary mt-20" type="button">Guardar cambios</button>
                                    </div>
                                    
                                </div>
                                                    </section>




                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-light">
                                        </div>
                                </div>
                            </div>
                            </div>
                        </div>
                                
                                
                                
            








                            </div>
                           
                        </div>
                    </div>
                </div>
                <!-- /Row -->

            </div>
            <!-- /Container -->
        
<!-- /Main Content -->







<script src="Controllers/js/ctrlAgentes.js"></script>


