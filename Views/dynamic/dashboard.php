


			<!-- Container -->
            <div class="container-fluid" id="dashboard">
                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="profile-cover-wrap overlay-wrap">
                            <div class="profile-cover-img" style="background-image:url('dist/img/trans-bg.jpg')"></div>
							<div class="bg-overlay bg-trans-dark-60"></div>
							<div class="container profile-cover-content py-50">
								<div class="hk-row"> 
									<div class="col-lg-6">
										<div class="media align-items-center">
											<div class="media-img-wrap  d-flex">
												<div class="avatar">
													<img src="dist/img/avatar12.jpg" alt="user" class="avatar-img rounded-circle">
												</div>
											</div>
											<div class="media-body">
												<div class="text-white text-capitalize display-6 mb-5 font-weight-400">{{nombre_agente}}</div>
												<strong>{{tipo_agente}}</strong>
												<div class="font-14 text-white"><span class="mr-5"><span class="font-weight-500 pr-5">124</span><span class="mr-5">Followers</span></span><span><span class="font-weight-500 pr-5">45</span><span>Following</span></span></div>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="button-list">
											<a href="#" data-toggle="modal" data-target="#exampleModalForms" class="btn btn-dark btn-wth-icon icon-wthot-bg btn-rounded"><span class="btn-text">Crear Noticia</span><span class="icon-label"><i class="icon ion-md-mail"></i> </span></a>
											<!--a href="#" class="btn btn-icon btn-icon-circle btn-indigo btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
											<a href="#" class="btn btn-icon btn-icon-circle btn-sky btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
											<a href="#" class="btn btn-icon btn-icon-circle btn-purple btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-instagram"></i></span></a-->
										
										</div>
									</div>
								</div>
							</div>
						</div>
                        <!-- Modal forms-->
						<div class="modal fade" id="exampleModalForms" tabindex="-1" role="dialog" aria-labelledby="exampleModalForms" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                 <center><h5 class="modal-title"><a href="#">Crear Una Nueva Noticia</a></h5></center>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        <div class="form-group">
                                                            <label for="exampleDropdownFormEmail1">Titulo</label>
                                                            <input type="text" v-model="titulo_noticia" class="form-control" id="exampleDropdownFormEmail1" placeholder="Titulo">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleDropdownFormPassword1">Mensaje</label>
                                                            <textarea  class="form-control" v-model="mensaje_noticia" id="exampleDropdownFormPassword1" placeholder="Mensaje"></textarea>
                                                        </div>
                                                  
                                                        <button type="submit" @click.prevent ="CrearNoticia()" class="btn btn-primary">Crear Noticia</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

						<div class="tab-content mt-sm-60 mt-30">
							<div class="tab-pane fade show active" role="tabpanel">
								<div class="container">
									<div class="hk-row">
										<div class="col-lg-8">
                                        <section class="hk-sec-wrapper">
                                                    <h6 class="hk-sec-title">Movimientos Diarios</h6>
                                                    <div class="row">
                                                        <div class="col-sm">
                                                            <div id="e_chart_11" class="echart" style="height:400px;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer justify-content-between">
                                                    <div>
														<a href="#"><i class="ion ion-md-heart text-primary"></i><p v-text="total"></p></a>
													</div>
													<div>
														<a href="#">Movimientos hechos a cada uno de los item mostrados con anterioridad
														</a>
														<a href="#"></a>
													</div>
                                                </div>
                                                  </section>
												  <div class="card card-profile-feed">
                                                <div class="card-header card-header-action">
													<div class="media align-items-center">
														<div class="media-img-wrap d-flex mr-10">
															<div class="avatar avatar-sm">
																<img src="dist/img/avatar7.jpg" alt="user" class="avatar-img rounded-circle">
															</div>
														</div>
														<div class="media-body">
															<div class="text-capitalize font-weight-500 text-dark"><h4><a hreft="#">Citas</a></h4></div>
															<div class="font-13"></div>
														</div>
													</div>
											
												</div>
												<div class="row text-center">
													<div class="col-6 border-right pr-0">
														<div class="pa-15">
															<span class="d-block display-6 text-dark mb-5">{{citas_para_hoy}}</span>
															<span class="d-block text-capitalize font-14">Citas Para Hoy</span>
														</div>
													</div>
													<div class="col-6 border-right pr-0">
														<div class="pa-15">
															<span class="d-block display-6 text-dark mb-5">{{citas_tomadas_hoy}}</span>
															<span class="d-block text-capitalize font-14">Citas Tomadas Hoy</span>
														</div>
													</div>
												</div>
												<hr>
      											<center>	<h5><a href="#">Citas creadas por cada Agente</a></h5></center>
												<hr>
												<div class="row text-center">
													<div  v-for="i in agentes_citas"class="col-4 border-right pr-0">
														<div class="pa-15">
															<span class="d-block display-6 text-dark mb-5">{{i.cantidad_citas}}</span>
															<span class="d-block text-capitalize font-14">Citas procesadas por el agente: {{i.id_agente}} --{{i.nombre_agente}} </span>
														</div>
													</div>
													
												</div>
												
											 </div>
										</div>
										<div class="col-lg-4">
										<h4> <a href="#">Noticias.....</a></h4>
											<div class="card card-profile-feed">
                                                <div class="card-header card-header-action">
													<div class="media align-items-center">
														<div class="media-img-wrap d-flex mr-10">
															<div class="avatar avatar-sm">
																<img src="dist/img/avatar7.jpg" alt="user" class="avatar-img rounded-circle">
															</div>
														</div>
														<div class="media-body">
															<div class="text-capitalize font-weight-500 text-dark">Angelic Lauver</div>
															<div class="font-13">Business Manager</div>
														</div>
													</div>
													<div class="d-flex align-items-center card-action-wrap">
														<div class="inline-block dropdown">
															<a class="dropdown-toggle no-caret" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="ion ion-ios-settings"></i></a>
															<div class="dropdown-menu dropdown-menu-right">
																<a class="dropdown-item" href="#">Action</a>
																<a class="dropdown-item" href="#">Another action</a>
																<a class="dropdown-item" href="#">Something else here</a>
																<div class="dropdown-divider"></div>
																<a class="dropdown-item" href="#">Separated link</a>
															</div>
														</div>
													</div>
												</div>
												<div class="row text-center">
													<div class="col-4 border-right pr-0">
														<div class="pa-15">
															<span class="d-block display-6 text-dark mb-5">154</span>
															<span class="d-block text-capitalize font-14">photos</span>
														</div>
													</div>
													<div class="col-4 border-right px-0">
														<div class="pa-15">
															<span class="d-block display-6 text-dark mb-5">65</span>
															<span class="d-block text-capitalize font-14">followers</span>
														</div>
													</div>
													<div class="col-4 pl-0">
														<div class="pa-15">
															<span class="d-block display-6 text-dark mb-5">433</span>
															<span class="d-block text-capitalize font-14">views</span>
														</div>
													</div>
												</div>
												<ul class="list-group list-group-flush">
                                                    <li class="list-group-item"><span><i class="ion ion-md-calendar font-18 text-light-20 mr-10"></i><span>Went to:</span></span><span class="ml-5 text-dark">Oh, Canada</span></li>
                                                    <li class="list-group-item"><span><i class="ion ion-md-briefcase font-18 text-light-20 mr-10"></i><span>Worked at:</span></span><span class="ml-5 text-dark">Companey</span></li>
                                                    <li class="list-group-item"><span><i class="ion ion-md-home font-18 text-light-20 mr-10"></i><span>Lives in:</span></span><span class="ml-5 text-dark">San Francisco, CA</span></li>
                                                    <li class="list-group-item"><span><i class="ion ion-md-pin font-18 text-light-20 mr-10"></i><span>From:</span></span><span class="ml-5 text-dark">Settle, WA</span></li>
                                                </ul>
											 </div>
		
										
						
										
											<div class="card bg-primary text-center border-0">
                                                <div class="twitter-slider-wrap card-body">
                                                    <div class="twitter-icon text-center mb-15">
                                                        <i class="fa fa-twitter"></i>
                                                    </div>
                                                    <div id="tweets_fetch" class="owl-carousel owl-theme"></div>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
                </div>
                <!-- /Row -->
            </div>
            <!-- /Container -->


           
            <script src="Controllers/js/ctrldashboard.js"></script>
        