<?php ?>
<!-- Container -->
<div class="container-fluid" id ="clientes">
                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="emailapp-wrap">
            
                            <div class="email-box">
                            <div class="emailapp-left card  card-refresh ">
                            <div class="refresh-container" :style="{ display:loaderLeft }">
                                <div class="loader-pendulums"></div>
                            </div>
                            <header>
                                <a @click="HabilitarComponente('lista')" href="javascript:void(0)" class="emailapp-sidebar-move">
                                    <span class="feather-icon"><i data-feather="list"></i></span>
                                </a>
                                <span class="">Clientes</span>
                                <a  @click="RegistrarCliente('form')" href="javascript:void(0)" class="email-compose" data-toggle="modal" data-target="#exampleModalEmail">
                                    <span class="feather-icon"><i data-feather="plus"></i></span>
                                </a>
                            </header>
                           
                            <form role="search"  v-show="ListaClientes">
                                <!--div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="feather-icon"><i data-feather="search"></i></span>
                                    </div>
                                    <input v-model="buscar" @keyup="Buscar()" type="text"  class="form-control" placeholder="Buscar...">
                                </div-->
                                <div class="input-grounp">
                                          
                                          <div class="form-group" style="margin:10px;">
                                      <div class="input-group">
                                        
                                
                                          <input type="date" @change="filtrar()" placeholder="Inicio"  v-model="fecha_inicio" class="form-control">
                                          <input type="date" @change="filtrar()" laceholder="Fin"  id="input_tiempo"  v-model="fecha_final" class="form-control">
                                        
                                      </div>
                                  </div>
                                         
                              </div>
                            </form>
                            <div v-show="mensajeNoLista" class="mt-20 col-lg-12 col-md-6 col-sm-6 col-xs-12 mb-30">
                                    <div class="card border-danger">
                                        <div class="card-header">Clientes</div>
                                        <div class="card-body text-danger">
                                            <h5 class="card-title text-danger">PERMISO NO AUTORIZADO</h5>
                                            <p class="card-text">Usted no se encuentra autorizado para ingresar a este módulo</p>
                                        </div>
                                    </div>
                                </div>
                            <!--div v-if="mensajeNoLista" style="height:400px;"  >
                                <div  class="col-sm">
                                <p style="color:red">No puedes ver La lista de usuarios</p>
                                </div>
                                
                            </div-->
                            
                            <!-- LISTA DE CONVENIOS-->
                            <div  class="emailapp-emails-list" v-show="ListaClientes" >
                                <div class="nicescroll-bar" >
                                    <hr>
                                    <div class="container" v-show="ListaClientes">

             
<!-- Row -->

    <div class="col-xl-12" >
        <section class="">
           <div class="row" >
                <div class="col-sm">
                    <div class="table-wrap">
                        <table id="datable_1"  class="table table-hover  responsive">
                            <thead>
                                <tr>
                                    <th>Datos de los Clientes</th>
                                  
                                
                                </tr>
                            </thead>
                            <tbody >
                            
                            <!--tr v-for="(item,index) in  leads">
                              <td>
                              <a href="javascript:void(0);"  @click="MostrarLeads(item.id_leads)" class="media">
                       
                       <div class="media-body">
                                       

                               <div>
                                   <div class="email-head">Fecha:  {{item.fecha}} </div>
                                   <div class="email-subject"> Estado {{item.estado}}</div>
                                    <div >
                                       <p style="color:black">Telefono Cliente asociado: {{item.cliente.telefono}}</p>
                                   </div>
                               </div>
                               <div>
                                   <div class="last-email-details">
                                    
                                   </div>
                                   </div>
                           </div>
                       </a>
                       <div class="email-hr-wrap">
                           <hr>
                       </div>
                              </td>
                            </tr-->
                             
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Datos de los Clientes</th>
                                    
                                
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>

<!-- /Row -->

</div>
                                    <hr>
                                    <!--div v-for="(item,index) in  ClientesQueMuestran">
                                    <a href="javascript:void(0);"  @click="MostrarClientes(item.id)" class="media">
                                        <div class="media-body">
                                            <div>
                                                <div class="email-head">{{item.nombre}} </div>
                                                <div class="email-subject">Telefono: {{item.telefono}}</div>
                                                <div class="email-text">
                                                    <p>{{item.email}}</p>
                                                </div> 
                                            </div>
                                            <div>
                                                <div class="last-email-details">
                                                     <p>Dni: {{item.dni}}</p>
                                                
                                                </div>
                                                </div>
                                        </div>
                                    </a>
                                    <div class="email-hr-wrap">
                                        <hr>
                                    </div>
                                    </div-->
                                </div>
                            </div>
                          
                           



                            <!-- FORMULARIO DE REGISTRO  -->
                            <section v-show="formRegistrarClientes" class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Registrar cliente</h5>
                                <div class="row">
                                    <div class="col-sm">
                                
                                        <form>
                                            <div class="refresh-container" :style="{ display:loaderLeft }">
                                                <div class="loader-pendulums"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5 form-group">
                                                    <label>Nombre</label>
                                                    <input v-model="nombre" class="form-control"  type="text">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label>DNI</label>
                                                    <input v-model="dni" class="form-control"  type="text">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5 form-group">
                                                    <label>E-mail</label>
                                                    <input v-model="email" class="form-control"  type="email">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label>Teléfono</label>
                                                    <input v-model="telefono"  @click="Alert_telefono = 'border-default' " :class="['form-control', Alert_telefono ]"   type="number">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Dirección</label>
                                                    <input v-model="direccion" class="form-control"  type="text">
                                                </div>
                                            </div>


                                            <hr>
                                            <button @click="RegistrarCliente('Procesar')" class="btn btn-primary" type="button">Registrar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                                <!--FORMULARIO REGISTRO-->
                           
                           
                        </div>






                        <div class="emailapp-right">
                            <header>
                                <a id="back_email_list" href="javascript:void(0)" class="back-email-list">
                                    <span class="feather-icon"><i data-feather="chevron-left"></i></span>
                                </a>
                                
                                <div class="email-options-wrap" v-show="habilitar">
                              <a href="javascript:void(0)" @click="EnviarMensajeCorreo('correo')" class=""><span class="feather-icon"><img src="Assets/dist/img/email.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="EnviarMensajeAlWhastapp('whatsapp')" class=""><span class="feather-icon"><img src="Assets/dist/img/whatsapp.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="EnviarMensajeAlSms('sms')" class=""><span class="feather-icon"><img src="Assets/dist/img/sms.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="HabilitarComponente('editar')" class=""><span class="feather-icon"><img src="Assets/dist/img/editar.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="HabilitarComponente('infoCliente')" class=""><span class="feather-icon"><img src="Assets/dist/img/llamadas.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="HabilitarComponente('historial')" class=""><span class="feather-icon"><img src="Assets/dist/img/historial_.png" width="30" ></span></a>
                                 <a href="javascript:void(0)" @click="AlertEliminarCliente=true" class=""><span class="feather-icon"><img src="Assets/dist/img/borrar.png" width="30" ></span></a>
                                
                                    
                                </div>
                            </header>
                            
                            
                            <div class="email-body">
                                
                                       <!--ENVIAR MENSAJE AL CORREO DEL CLIENTE-->
                               
                                <div class="nicescroll-bar" >
                                <!--div class="email-subject-head">
                                            <h4>Cliente<br> {{cliente_s}}
                                            </h4>
                                            
                               </div-->
                               <hr>
                               <div class="col-sm-12" v-show="AlertEliminarCliente" >
                                            <div class="alert alert-warning alert-wth-icon alert-dismissible fade show" role="alert">
                                                <span class="alert-icon-wrap"><i class="zmdi zmdi-alert-circle-o"></i></span>
                                                <h5 class="alert-heading">¿Desea mover este Cliente a la papelera?</h5>
                                                <button @click="AlertEliminarCliente=false" class="btn btn-secondary mt-20 mr-5">Cancelar</button>
                                                <button @click="MoverAlaPapelera('procesar')"  class="btn btn-primary mt-20">Eliminar</button>
                                                <button @click="AlertEliminarCliente=false" type="button" class="close" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div> 
                                        </div>

                                        

                    <section v-show="correo"  class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Enviar Mensaje</h5>
                                <div class="row">
                                    <div class="col-sm">
                                    <div class="refresh-container" :style="{ display:loader }">
                                              <div class="loader-pendulums"></div>
                                          </div>
                                        <form>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Correo</label>
                                                    <input v-model="correo_mensaje" class="form-control"  type="email">
                                                </div>
                                            
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Asunto</label>
                                                    <input v-model="asunto_mensaje" class="form-control"  type="text">
                                                </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Mensaje</label>
                                                    <textarea class="form-control mt-15" v-model="mensaje_mensaje" rows="3" placeholder="Ingresa el mensaje"></textarea>
                                                </div>
                                            </div>


                                            <hr>
                                            <button @click="EnviarMensajeCorreo('enviar')" class="btn btn-primary" type="button">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>

                  <!--ENVIAR MENSAJE AL CORREO DEL CLIENTE-->
                   <!--ENVIAR MENSAJE WHASTAPP-->
                   <section v-show="whatsapp"  class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Enviar Mensaje WhastApp</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Numero Cliente</label>
                                                    <input v-model="w_numero" class="form-control"  type="number">
                                                </div>
                                            
                                            </div>

                                          
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Mensaje</label>
                                                    <textarea class="form-control mt-15" v-model="w_mensaje" rows="3" placeholder="Ingresa Tu mensaje"></textarea>
                                                </div>
                                            </div>


                                            <hr>
                                            <button @click="EnviarMensajeAlWhastapp('enviar')" class="btn btn-primary" type="button">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                    <!--ENVIAR MENSAJE WHASTAPP-->
                      <!--ENVIAR MENSAJE SMS-->
                   <section v-show="MensajeSms"  class="hk-sec-wrapper" >
                                <h5 class="hk-sec-title">Enviar Mensaje Sms</h5>
                                <div class="row">
                                    <div class="col-sm">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Numero Cliente</label>
                                                    <input v-model="sms_numero" class="form-control"  type="number">
                                                </div>
                                            
                                            </div>

                                          
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label>Mensaje</label>
                                                    <textarea class="form-control mt-15" v-model="sms_mensaje" rows="3" placeholder="Ingresa Tu mensaje"></textarea>
                                                </div>
                                            </div>


                                            <hr>
                                            <button @click="EnviarMensajeAlSms('enviar')" class="btn btn-primary" type="button">Enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                    <!--ENVIAR MENSAJE SMS-->
    
                   <!--EDITAR CLIENTE-->
               
                   <div class="row" v-for="cliente in Cliente" v-if="editar_">
                              
                              <section class="col-lg-12  hk-sec-wrapper">
                                                      <h4 class="hk-sec-title mb-25"><img src="Assets/dist/img/editar.png" width="30" > Modificar Cliente</h4>
                                                      <div class="row">
                                                          <div class="col-12">
                                                              <div class="row" v-for="cliente in Cliente">
                                                                  <div class="col-md-12 col-sm-12">
      
                                                                      <form>
                                                                          
                                                                        
      
                                                                          
                                                                          <div class = "row">
                                                                              <div class="form-group col-md-6">
                                                                                  <label >DNI</label>
                                                                                  <input v-model="cliente.dni"  class="form-control" type="number">
                                                                              </div>
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Nombre</label>
                                                                                  <input v-model="cliente.nombre" class="form-control"  type="text">
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group">
                                                                                  <label >Email</label>
                                                                                  <input v-model="cliente.email" class="form-control" type="text">
                                                                          </div>
      
                                                                          <div class = "row">
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Teléfono</label>
                                                                                  <input v-model="cliente.telefono" class="form-control" type="text">
                                                                              </div>
                                                                              <div class="form-group col-md-6">
                                                                                  <label >Dirección</label>
                                                                                  <input v-model="cliente.direccion" class="form-control"  type="text">
                                                                              </div> 
                                                                             
                                                                          </div>
      
                                                                     
                                                                          <hr>
                                                                          <center><button @click="HabilitarComponente('lista')" class="btn btn-dark" type="button">Cancelar</button>
                                                                        <button @click.prevent="EditarCliente('editar')" class="btn btn-primary" type="button">Guardar Cambios</button></center>
      
                                                                      </form>
                                                  
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                              </section>
                                
      
      
                                                                   
                              </div>
                   <!--EDITAR CLIENTE-->
                   <!--HISTORIAL CLIENTE-->
                   <div class="col-lg-12 " v-if="ClienteHistorial">
                                      
                   
                   
                                    <div class="card card-refresh">
                                        
                                          <div class="card-header card-header-action">
                                              <h6>Historial Del Cliente</h6>
                                             
                                          </div>
                                          <div class="card-body">
                                         
                                          <div class="card-body pa-15"> 
                                     
                                          <ol>
                             <li  v-for="item in HistorialCliente">
                             <blockquote class="blockquote mb-0">
                             <a href="javascript:void(0);"  @click="MostrarLeads(item.id_leads)" class="media">
                       
                       <div class="media-body">
                                       

                               <div>
                                   
                                   <div class="email-subject"><strong>Tema:</strong> {{item.historial.movimiento}}</div>
                                   <div class="email-head"><strong>Asunto: {{item.historial.titulo}} </strong>  {{item.antiguedad}} </div>
                                   <div class="email-head"><strong>Fecha: {{item.historial.fecha_registro}} </strong>  </div>
                                   <div class="email-head"><strong>Agente: {{item.nombre_agente}} </strong>  </div>
                                    <div >

                                   </div>
                               </div>
                               <div>
                                   <div class="last-email-details">
                                    
                                   </div>
                                   </div>
                           </div>
                       </a>
                       </blockquote>    
                             </li>
                         </ol>    
                                                                       
                                                        </div>
                             
                                          </div>
                                      </div>
                                  </div>
                   <!--HISTORIAL CLIENTE-->
                   <!-- INFORMACIÓN DEL CLIENTE  -->
                    <div v-if="infoCliente" >
                                        
        
                                            <div class="email-head">
                                                <div class="media">
                                                    <div class="media-body">
                                                        


                                                    <section class="hk-sec-wrapper">
                                                        <div class="row">
                                                            <div class="col-sm">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul class="list-group mb-15">
                                                                           
                                                                            
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Nombre: {{Cliente[0].nombre}}</label>
                                                                            </li>
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Dni: {{Cliente[0].dni}}</label>
                                                                            </li>

                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Teléfono: {{Cliente[0].telefono}}</label>
                                                                            </li>
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Fecha Registro: {{Cliente[0].fecha_registro}} </label>
                                                                            </li>
                                                                          
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul class="list-group mb-15">
                                                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Email:{{Cliente[0].email}} </label>
                                                                           </li>
                                                                           <li class="list-group-item d-flex justify-content-between align-items-center">
                                                                                <label> Dirección: {{Cliente[0].direccion}} </label>
                                                                            </li>
                                                                        
                                                                            
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>




                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-light">
                                    
    
    


                        </div>
                    <!-- INFORMACIÓN DEl ESPECIALISTA -->
                                    
                                </div>
                            </div>
                        </div>
                                
                                
                                
            








                            </div>
                           
                        </div>
                    </div>
                </div>
                <!-- /Row -->

            </div>



<!-- /Container -->
<link href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>
<script src="Controllers/js/ctrlClientes.js"></script>