<?php 
    session_start(); 
    date_default_timezone_set('America/Bogota');
    require_once "Models/mdlHistorial.php";
    session_regenerate_id(true);
    if (isset($_POST["SessionUsrOk"])) {
        $_SESSION["user_logged"] = $_POST["SessionUsrOk"];
    }
    if (isset($_POST["destroy"])) {
        $movimiento  = mdlHistorial::RegistrarMovimiento($_SESSION["user_logged"],"usuarios", $_SESSION["user_logged"], "Cerro sesion", date("Y:m:d h:i:s") );
        
        session_destroy();   
    }
?>
<!DOCTYPE html>
<!-- 
Template Name: Brunette - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: https://hencework.ticksy.com/

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="es">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>ARRAY TIC | CRM</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />
<style>
.lista_mover{
   margin-left: 30px;    
}
#espacio{
    margin-right: 10px;;
}
#cursor{
    cursor:pointer
}

</style>
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <script src="Assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Tablesaw table CSS -->
    <link href="Assets/vendors/tablesaw/dist/tablesaw.css" rel="stylesheet" type="text/css" />
	
     <!-- Data Table CSS -->
    <link href="Assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="Assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />

	<!-- vector map CSS -->
    <link href="Assets/vendors/vectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" type="text/css" />

    <!-- Toggles CSS -->
    <link href="Assets/vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
    <link href="Assets/vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">
	
	<!-- Toastr CSS -->
    <link href="Assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <script src="Assets/vendors/pickr-widget/dist/pickr.min.js"></script>
    <script src="Assets/vendors/dist/js/pickr-data.js"></script>

    <!-- Custom CSS -->
    <link href="Assets/dist/css/style.css" rel="stylesheet" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
       <!-- select2 CSS -->
       <link href="Assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
	
    <!--VUE! -->
        <!-- Calendar CSS -->
        <link href="Assets/vendors/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" type="text/css" />

<!-- Daterangepicker CSS -->
<link href="Assets/vendors/daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />

<!-- Toggles CSS -->
<link href="Assets/vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
<link href="Assets/vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">




    <script src="Assets/Vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@1.5.14/dist/vuetify.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
  <script src="https://unpkg.com/vuejs-datepicker"></script>
    <script src="Assets/Axios.js"></script>
 
</head>

<body>
  

    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->
	<!-- HK Wrapper -->
   
      	<?php 
           if (isset($_SESSION["user_logged"])) 
           {
               echo " <div class='hk-wrapper hk-vertical-nav'>";
                //NAVBAR
                require_once"Views/static/nav.php" ;
                // END NAVBAR
                // SIDEBAR
                require_once"Views/static/sidebar.php";
                // END SIDEBAR
                echo "<div class='hk-pg-wrapper pb-0'>" ;
                //controlador de vistas:
                require_once"Controllers/php/ctrl_vistas.php";
                //End controlador de vistas
                echo "</div> </div>";
           }else{
               require_once"Views/dynamic/log-in.php";
           }
        ?>

    

    <!-- /HK Wrapper -->
	

    <!-- jQuery -->
  

    <!-- Bootstrap Core JavaScript -->
    <script src="Assets/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="Assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="Assets/dist/js/jquery.slimscroll.js"></script>


     <!-- Data Table JavaScript -->
    <script src="Assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="Assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="Assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="Assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="Assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="Assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="Assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="Assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="Assets/dist/js/dataTables-data.js"></script>


    <!-- Fancy Dropdown JS -->
    <script src="Assets/dist/js/dropdown-bootstrap-extended.js"></script>

     <!-- Tablesaw JavaScript -->
     <script src="Assets/vendors/tablesaw/dist/tablesaw.jquery.js"></script>
    <script src="Assets/dist/js/tablesaw-data.js"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="Assets/dist/js/feather.min.js"></script>

    <!-- Toggles JavaScript -->
    <script src="Assets/vendors/jquery-toggles/toggles.min.js"></script>
    <script src="Assets/dist/js/toggle-data.js"></script>
	
	<!-- Counter Animation JavaScript -->
	<script src="Assets/vendors/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="Assets/vendors/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- EChartJS JavaScript -->
    <script src="Assets/vendors/echarts/dist/echarts-en.min.js"></script>
    
	<!-- Sparkline JavaScript -->
    <script src="Assets/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Vector Maps JavaScript -->
    <script src="Assets/vendors/vectormap/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="Assets/vendors/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="Assets/dist/js/vectormap-data.js"></script>

	<!-- Owl JavaScript -->
    <script src="Assets/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Toastr JS -->
    <script src="Assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.js"></script>



  <!-- Select2 JavaScript -->
    <script src="Assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="Assets/dist/js/select2-data.js"></script>
     

   <!--barchart-->

   <script src="Assets/vendors/echarts/dist/echarts-en.min.js"></script>
  



    
    <!-- Init JavaScript -->
    
    <script src="Assets/dist/js/init.js"></script>
    
	<script src="Assets/dist/js/dashboard-data.js"></script>
	
</body>

</html>