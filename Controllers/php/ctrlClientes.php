<?php
session_start();
require_once"../../Models/mdlClientes.php";
require_once"../../Models/mdlAgentes.php";
require_once"../../Models/mdlHistorial.php";
require_once"../../Models/mdlPapelera.php";
require_once"../../Models/mdlCitas.php";
require_once"../../Models/mdlLeads.php";
////
//date_default_timezone_set('America/Bogota');
//date_default_timezone_set('Europe/Madrid');
require_once "../../ext/carbon/vendor/autoload.php";

use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();
if(isset($_POST["leads_"])){
    $datos= MdlLeads::Leads();
     //funcion lead y clientes para relacionarlos
    echo json_encode($datos);
}
if (isset($_GET["permiso"])) {
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343454");  
    if($VPRUT){
        $rest["permiso"] = "ok";
    }else{
        $rest["permiso"] = "No";
    }


    header("Content-Type: application/json");
    echo json_encode($rest);
}
if (isset($_GET["FechaActual"])) {
    
    $rest["FechaActual"] = date("Y-m-d");

    header("Content-Type: application/json");
    echo json_encode($rest);
}
//////// REGISTRAR CLIENTES //////
if (isset($_POST["registrar"])) 
{
    if ( $_POST["telefono"] !== "" ) 
     
    {
      $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343454");    
      if ($VPRUT) { 
          
        //GUARDAMOS LOS DATOS EN UN ARREGLO
        $datos = array(
            "nombre" => $_POST["nombre"],
            "dni" => $_POST["dni"],
            "telefono" => $_POST["telefono"],
            "email" => $_POST["email"],
            "direccion" => $_POST["direccion"],
            "fecha_registro" => $fechaActual 
        );
        //ENVIAMOS LOS DATOS PARA QUE SEAN REGISTRADOS EN LA BSAE DE DATOS
        $registro = MdlClientes::RegistrarClicnte($datos);

        if ($registro) { // PREGUNTAMOS SI TODO ESTA CORRECTO

           $listar1 = mdlClientes:: MostrarClientes(false);
           //REGISTRAMOS EL MOVIMIENTO EN LA BASE DE DATOS
           $movimiento  = mdlHistorial::RegistrarMovimiento($_SESSION["user_logged"],"crm_clientes", $listar1[0]["id"], "Registro Nuevo Cliente", date("Y:m:d h:i:s") );
           if($listar1[0]["id"]>=0 && $_POST["cita_cliente"]=="ok"){
              
              $registro= MdlCitas::CrearCita($_POST["fecha"],$_POST["fecha2"], $_POST["title"], $listar1[0]["id"]);    

             
    if ($registro) { // preguntamos si el registro se realizó correctamente
        $listar = MdlCitas::MostrarCitas();
        
        if($listar){
            $fecha1 =$_POST["fecha_3_antes"];
            $fecha2 =$_POST["fecha_1_antes"]; 
            $fecha3 =$_POST["fecha_6_horas_antes"];
            $fecha4 =$_POST["fecha_3_despues"];
            $fecha5 =$_POST["fecha_1_despues"];
            $fecha6 =$_POST["fecha_6_horas_despues"];
            $registro= MdlCitas::ProgramarCita( $listar1[0]["id"], $_SESSION["user_logged"], $listar[0]["id"],$fecha1, "");
            if($registro){
                $registro2 =MdlCitas::ProgramarCita( $listar1[0]["id"], $_SESSION["user_logged"], $listar[0]["id"],$fecha2,"");
                if($registro2){
                    $registro3 =MdlCitas::ProgramarCita( $listar1[0]["id"], $_SESSION["user_logged"], $listar[0]["id"],$fecha3, "");
                    if($registro3){
                        $registro4 =MdlCitas::ProgramarCita( $listar1[0]["id"], $_SESSION["user_logged"], $listar[0]["id"],$fecha4, "");
                        if($registro4){
                            $registro5 =MdlCitas::ProgramarCita( $listar1[0]["id"], $_SESSION["user_logged"], $listar[0]["id"],$fecha5, "");
                             if($registro5){
                                $registro6 =MdlCitas::ProgramarCita( $listar1[0]["id"], $_SESSION["user_logged"], $listar[0]["id"],$fecha6, "");
                             }   
                         }
                    }
                }
            }
                  
           
            $movimiento  = mdlHistorial::RegistrarMovimiento(
                $_SESSION["user_logged"],
                "citas", 
                $listar[0]["id"], 
                "Registro una nueva cita", 
                $fechaActual
            );
            if($movimiento){
                $movimiento  = mdlHistorial::RegistrarMovimiento2(
                    $_SESSION["user_logged"],
                    "crm_clientes", 
                    $listar1[0]["id"], 
                    "Se le creo una cita",
                    $_POST["title"], 
                    $fechaActual
                );
            }
   
            $rest["respuesta"]  = "ok";
        }else{
            $rest["respuesta"]  = "error";
        }

        
        //$rest["respuesta"] =$listar;
     }else{
         $rest["respuesta"]  = "500"; // error interno 
     }
           
            }else{
              $rest["respuesta"] ="ok";
            }
       
        }else{
          $rest["respuesta"]  = "500"; // error interno 
        }
    
      }else{
        $rest["respuesta"] ="No"; ///no tiene permiso
      }
        
    }else{
        $rest["respuesta"]  = "00"; /// 00 significa campos vacios
    }
    header("Content-Type: application/json");
    echo json_encode($rest);
}
//condicional de eliminar datos
if(isset($_POST["eliminar"])){
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343454");    
    if ($VPRUT) {
        //MOVEMOS ALA PAPAPELERA AL CLIENTE SE
        $papelera = mdlPapelera::MoverAlapapelera($_POST["eliminar"],"crm_clientes",  date("Y:m:d h:i:s"));

           if($papelera){
               $movimiento  = mdlHistorial::RegistrarMovimiento(
                    $_SESSION["user_logged"],
                    "crm_clientes", 
                    $_POST["eliminar"], 
                    "Se Movio este cliente a la Papelera", 
                    $fechaActual
                );
                if($movimiento){
                    $movimiento  = mdlHistorial::RegistrarMovimiento2(
                        $_SESSION["user_logged"],
                        "crm_clientes", 
                        $_POST["eliminar"], 
                        "Fue movido a la papelera",
                        "", 
                        $fechaActual
                    );
                }
            }
  $rest["respuesta"]=$papelera;
  header("Content-Type: application/json");
  echo json_encode($rest);
}
}
//condicional de la parte de enviar correos electronicos
if (isset($_POST["Enviarcorreo"])) 
{
    if ($_POST["correo_m"] != "" && $_POST["asunto"] !== "" && $_POST["mensaje"] != "") 
        /// si los campos estan llenos que no esté ninguno vacío
    {
        if (filter_var($_POST["correo_m"], FILTER_VALIDATE_EMAIL))  /// si el correo si es valido 
        {
            $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343455");    
            if ($VPRUT) {
           
           $datos = array("id_cliente" => $_POST["id_cliente"],
            "email" => $_POST["correo_m"],
            "asunto" => $_POST["asunto"],
            "mensaje" => $_POST["mensaje"],
            "fecha_envio" => $fechaActual );
            $registro = MdlClientes::RegistrarMensajeCorreo($datos);
            if($registro){
                $Mc =MdlClientes::MostrarCorreos();
                if($Mc){
                   $movimiento  = mdlHistorial::RegistrarMovimiento(
                      $_SESSION["user_logged"],
                      "email_registro", 
                      $Mc[0]["id"], 
                      "se envio un correo electronico", 
                      $fechaActual
                   );
                   if($movimiento){
                    $movimiento  = mdlHistorial::RegistrarMovimiento2(
                        $_SESSION["user_logged"],
                        "crm_clientes", 
                        $_POST["id_cliente"], 
                        "recibio un email",
                        $_POST["asunto"],
                        $fechaActual
                     );
                     $rest["respuesta"]  = "ok";
                   }
                }  
                // $listar = mdlClientes:: MostrarClientes(false);
            }else{
                $rest["respuesta"]  = "500";
            }
        }else{
            $rest["respuesta"]  = "No";
        }
        }else{// de lo contrario .. email invalido
            $rest["respuesta"]  = "email_err"; /// email_err significa : email no válido 
        }
    }else{
        $rest["respuesta"]  = "00"; /// 00 significa campos vacios
    }
    header("Content-Type: application/json");
    echo json_encode($rest);
}
//condicional para procesar lo de mensaje de whatsapp


if (isset($_POST["Enviarwhatsapp"])) 
{ 
    if ($_POST["w_telefono"]!= ""  &&  $_POST["w_mensaje"]!== "" )        
    {
      $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343456");    
      if ($VPRUT) {
        $datos = array("id_cliente" => $_POST["id_cliente"],
          "telefono" => $_POST["w_telefono"],
          "mensaje" => $_POST["w_mensaje"],
          "fecha_envio" => $fechaActual );
          
        $registro = MdlClientes::RegistrarMensajeWhatsapp($datos);
        if($registro){
            $wh =MdlClientes::MostrarWhatsapp();
            if($wh){
                $movimiento  = mdlHistorial::RegistrarMovimiento(
                   $_SESSION["user_logged"],
                   "whatsapp_registros", 
                   $wh[0]["id"], 
                   "se envio un mensaje whatsapp", 
                   $fechaActual
                );
                if($movimiento){
                    $movimiento  = mdlHistorial::RegistrarMovimiento2(
                        $_SESSION["user_logged"],
                        "crm_clientes", 
                        $_POST["id_cliente"], 
                        "Recibio un mensaje whatsapp", 
                        $_POST["w_mensaje"],
                        $fechaActual
                     );
                }
                $rest["respuesta"]  = "ok";
             } 
            
        }else{
            $rest["respuesta"] = "500";
        }
        }else{
            $rest["respuesta"] = "No";
        }
    }else{
        $rest["respuesta"]  = "00"; /// 00 significa campos vacios
    }

   
    

    header("Content-Type: application/json");
    echo json_encode($rest);
}


if (isset($_POST["sms"])) 
{
    
  
    if ($_POST["s_numero"]!= ""  &&  $_POST["s_mensaje"]!== "" ) { 
        /// si los campos estan llenos que no esté ninguno vacío
        $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343456");    
    if ($VPRUT) {
       $datos = array("id_cliente" => $_POST["id_cliente"],
       "telefono" => $_POST["s_numero"],
       "mensaje" => $_POST["s_mensaje"],
     
       "fecha_envio" => $fechaActual);
       $registro = MdlClientes::RegistrarMensajeSms($datos);
       if($registro){
        $sms =MdlClientes::MostrarSms();
        if($sms){
            $movimiento  = mdlHistorial::RegistrarMovimiento(
               $_SESSION["user_logged"],
               "sms_registros", 
               $sms[0]["id"], 
               "se envio un mensaje sms", 
               $fechaActual
            );
            if($movimiento){
                $movimiento  = mdlHistorial::RegistrarMovimiento2(
                    $_SESSION["user_logged"],
                    "crm_clientes", 
                    $_POST["id_cliente"], 
                    "Recibio un mensaje sms",
                    $_POST["s_mensaje"], 
                    $fechaActual
                 );
              $rest["respuesta"]  = "ok";
            }
         } 
        
       }else{
        $rest["respuesta"] = "500";
       }    
    }else{
        $rest["respuesta"] = "No";
    }

   
    
    }else{
        $rest["respuesta"]  = "00"; /// 00 significa campos vacios
    }

   
    header("Content-Type: application/json");
    echo json_encode($rest);
    

  
}



if (isset($_POST["edita"])) 
{
if(isset($_POST["id"])){
    $datos = array( "id" => $_POST["id"],  "nombre" => $_POST["nombre"],
    "dni" => $_POST["dni"],
    "telefono" => $_POST["telefono"],
    "email" => $_POST["email"],
    "direccion" => $_POST["direccion"],
    "fecha_registro" => $fechaActual );
    $registro = MdlClientes::Actualizar($datos);
    if($registro){
        $movimiento  = mdlHistorial::RegistrarMovimiento(
            $_SESSION["user_logged"],
            "crm_clientes", 
            $datos["id"], 
            "Se actualizaron datos", 
            $fechaActual
         );
         if($movimiento){
            $movimiento  = mdlHistorial::RegistrarMovimiento2(
                $_SESSION["user_logged"],
                "crm_clientes", 
                $datos["id"], 
                "Se actualizaron datos",
                "", 
                $fechaActual
             );
         }
       
    }
   
   if ($registro) { // preguntamos si el registro se realizó correctamente
    $rest["respuesta"]  = "ok";
}else{
    $rest["respuesta"]  = "500"; // error interno 
}

}
  

    header("Content-Type: application/json");
    echo json_encode($rest);
}




////////////////LISTAR O MOSTRAR CLIENTES ////////////////
if (isset($_POST["listarTodo"])) {

    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343458");    
    if ($VPRUT) {
        $clientes_info =array();
        $listar = mdlClientes:: MostrarClientes(false);
        
        foreach ($listar as $cliente) 
        {
            $ConsultarPapelera = mdlPapelera::ConsultarPapelera($cliente["id"], "crm_clientes");
           if (!$ConsultarPapelera) {
              
                $client = array(
                    "id" => $cliente["id"],
                    "nombre" => $cliente["nombre"],
                    "dni" => $cliente["dni"],
                    "email" => $cliente["email"],
                    "telefono" => $cliente["telefono"],
                    "direccion" => $cliente["direccion"],
                    "fecha_registro" => $cliente["fecha_registro"]
                    
                );
                array_push($clientes_info, $client);
           }
        }
        /*movimiento  = mdlHistorial::RegistrarMovimiento(
            $_SESSION["user_logged"],
            "clientes", 
            00, 
            "Listo a todos los clientes", 
            date("Y:m:d h:i:s") 
        );*/

        $rest["clientes"]  = $clientes_info ;


    
 


    }else{
        $rest["clientes"] = "No";
    }




    header("Content-Type: application/json");
    echo json_encode($rest);
}


if(isset($_POST["filtro_clientes"])){

    if($_POST["fecha_inicial"]!="" & $_POST["fecha_final"]!=""){
      $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343458");    
      if ($VPRUT) {
     
          if($_POST["fecha_inicial"]!="" &  $_POST["fecha_final"]!=""){
            
          $filtro_fechas=MdlClientes::FiltroFechasClientes($_POST["fecha_inicial"], $_POST["fecha_final"]);
         
          $clientes =array();

           if($filtro_fechas){
            
            foreach ($filtro_fechas as $cliente) 
            {
                $ConsultarPapelera = mdlPapelera::ConsultarPapelera($cliente["id"], "crm_clientes");
               if (!$ConsultarPapelera) {
                  
                    $client = array(
                        "id" => $cliente["id"],
                        "nombre" => $cliente["nombre"],
                        "dni" => $cliente["dni"],
                        "email" => $cliente["email"],
                        "telefono" => $cliente["telefono"],
                        "direccion" => $cliente["direccion"],
                        "fecha_registro" => $cliente["fecha_registro"]
                        
                    );
                    array_push($clientes, $client);
               }

            }
            $rest["clientes"]  = $clientes;
           }else{
            $rest["clientes"] = "";
        
         
           }
         
           
         }
         
       
        }else{
         $rest["clientes"] = "No";
        }
    }else{
      $rest["clientes"] = "";
    }
    echo json_encode($rest);
  }
  