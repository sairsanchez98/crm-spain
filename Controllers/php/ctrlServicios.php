<?php
session_start();
require_once"../../Models/mdlClientes.php";
require_once"../../Models/mdlAgentes.php";
require_once"../../Models/mdlHistorial.php";
require_once"../../Models/mdlPapelera.php";
require_once"../../Models/mdlServicios.php";
require_once"../../Models/mdlLogin.php";
////////////////LISTAR O MOSTRAR CLIENTES ////////////////
date_default_timezone_set('Europe/Madrid');
if (isset($_POST["listarTodo"])) {

    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343468");    
    if ($VPRUT) {
        $servicios_info =array();
        $listar = MdlServicios:: MostrarServicios(false);
        
        foreach ($listar as $servicios) 
        {
            $ConsultarPapelera = mdlPapelera::ConsultarPapelera($servicios["id"], "servicios");
           if (!$ConsultarPapelera) {
              
                $client = array(
                    "id" => $servicios["id"],
                    "id_cliente" => $servicios["id_cliente"],
                    "id_agente"=>$servicios["id_agente"],
                    "asunto" => $servicios["asunto"],
                    "precio" => $servicios["precio"],
                    "fecha_creacion" => $servicios["fecha_creacion"],
                    
                );
                array_push($servicios_info, $client);
           }
        }
     

        $rest["servicios"]  = $servicios_info ;
    }else{
        $rest["clientes"] = "No";
    }
    header("Content-Type: application/json");
    echo json_encode($rest);
}
if(isset($_POST["registrar"])){
 
    if($_POST["telefono"]!="" & $_POST["precio"]!=""){
  
        $listar = mdlClientes:: MostrarClientes(false);
        if($listar){
            $contador =0;
            foreach($listar as $i){
               if($_POST["telefono"]==$i["telefono"]){
                 $contador=$contador+1;
                 $client = array(
                    "id_cliente"=>$i["id"] ,
                    "id_agente"=> $_SESSION["user_logged"],
                    "precio" => $_POST["precio"],
                    "fecha" => $_POST["fecha"],
                    "servicio"=>$_POST["servicio_ofrecido"]       
                );
               }
            }
           
            if($contador==0){
               $rest["respuesta"]="No existe Ningun Cliente registrado en el sistema con ese numero de telefono";
            }else{
                $registro =MdlServicios::RegistrarServicio($client);
                if($registro){
                    $rest["respuesta"]="ok";
                  }else{
                      $rest["respuesta"] ="error";
                  }
            }
           
        }else{
            $rest["respuesta"]="No clientes Registrado en el sistema";
        }
       
        header("Content-Type: application/json");
        echo json_encode($rest);
    }
}
if(isset($_POST["InfoServicio"])){
    if($_POST["id_cliente"]!="" & $_POST["id_agente"]!="")
     {
      
         $cliente = MdlClientes::UnicoCliente($_POST["id_cliente"]);
         $usuario = MdlLogin::UnicoUser($_POST["id_agente"]);
         if($cliente==true & $usuario==true){

                
                $client=array("nombre"=>$cliente[0]["nombre"], "telefono"=>$cliente[0]["telefono"], "email"=>$cliente[0]["email"]);
               $usuar =array("usuario"=>$usuario[0]["nombre"], "usuario"=>$usuario[0]["id"]);               
               $datos =array("cliente"=>$client, "agente"=>$usuar);
                $rest["resultado"] =$datos;
         }else{
               $rest["resultado"]="error";
         }
     }
     header("Content-Type: application/json");
     echo json_encode($rest);
}
?>