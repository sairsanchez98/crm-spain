<?php
  require_once"../../Models/mdlHistorial.php";
  require_once"../../Models/mdlAgentes.php";
  //date_default_timezone_set('America/Bogota');date_default_timezone_set('Europe/Madrid');
  require_once "../../ext/carbon/vendor/autoload.php";
use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();

  

  if(isset($_POST["historial_cliente"])){
       if($_POST["id_cliente"]){
        $id= $_POST["id_cliente"];
         $Historial=mdlHistorial::HistorialCliente();
         if($Historial){
          $ht = array();
          $todo =array();
           foreach($Historial as $valor){
            
          
             if($id== $valor["id_objeto_afectado"]){
                 $agente = mdlAgentes::MostrarunAgente($valor["id_agente"]);
                 if($agente){
                  $fecha_registro  = new Carbon($valor["fecha_registro"]);
                  $fecha_registro_time =  $fecha_registro ->diffForHumans();
                  $datos =array("nombre_agente"=> $agente[0]["nombre"], "antiguedad"=>$fecha_registro_time, "id_agente"=>$agente[0]["id"],"historial"=> $valor );
                  array_push($ht, $datos);
                 }


             }
           }
           
           $rest["datos"] =$ht;
           $rest["resultado"] ="ok";
       }else{
         $rest["resultado"] ="error";
       }
         header("Content-Type: application/json");
         echo json_encode($rest);
       }
  }

  if(isset($_POST["BuscarHistorialAgente"])){
          $id = $_POST["id_agente"];
          $Historial = mdlHistorial::HistorialAgente();
          if($Historial){
             $ht = array();
              foreach($Historial as $valor){
                if($id== $valor["id_agente"]){
                  array_push($ht, $valor);
                }
              }
              $rest["datos"] =$ht;
              $rest["resultado"] ="ok";
          }else{
            $rest["resultado"] ="error";
          }
          
          header("Content-Type: application/json");
          echo json_encode($rest);
         
     
  }
  if(isset($_POST["bucarInfo"])){
    
    if($_POST["tabla_afectada"]!="" & $_POST["fecha"]!=""){
      
      $buscar=mdlHistorial::BuscarInfo($_POST["tabla_afectada"], $_POST["id_agente"],$_POST["id_afectado"], $_POST["fecha"]);
      if($buscar){
        $rest["datos"] =$buscar;
        $rest["resultado"] ="ok";
      }else{
        
        $rest["resultado"] ="error";

      }
      header("Content-Type: application/json");
      echo json_encode($rest);
    }
   
  }
 
?>