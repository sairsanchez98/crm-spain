<?php

session_start();
require_once"../../Models/mdlCitas.php";
require_once"../../Models/mdlClientes.php";
require_once"../../Models/mdlAgentes.php";
require_once"../../Models/mdlHistorial.php";
require_once"../../Models/mdlProgramador_eventos.php";



require_once "../../ext/carbon/vendor/autoload.php";
use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
Carbon::now()->toDateTimeString();
$fechaActual =date("Y-m-d");

//date_default_timezone_set('Europe/Madrid');

//BUSCAR TODAS LAS CITAS
if (isset($_POST["listarTodo"])) {
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343463");    
      if ($VPRUT) {
        $listar = mdlCitas::MostrarCitas();
        $citas = array();
        foreach ($listar as $key => $cita) {
            $cita = array (
                "start" => $cita["start_"],
                "end" => $cita["end_"],
                "estado" => $cita["estado"],
                 "id_cita"=>$cita["id"],  
                "title" => $cita["title"],
                "id_agente" => $cita["id_agente"],
                "id_cliente" => $cita["id_cliente"]
            );

            array_push($citas , $cita);
        }

        $rest["citas"] = $citas;
      }else{
          $rest["citas"] = "No";
      }
    header("Content-Type: application/json");
    echo json_encode($rest);
}
//BUSCAR TODAS LAS CITAS


//EDITAR CITA
if(isset($_POST["EditarCita"])){
    if($_POST["id_cita"]!=""){
        $editar = mdlCitas::EditarCita($_POST["title"],$_POST["fecha1"],$_POST["fecha2"], $_POST["id_cita"], $_POST["estado"]);
        if($editar){
            $movimiento  = mdlHistorial::RegistrarMovimiento(
                $_SESSION["user_logged"],
                "citas", 
                $_POST["id_cita"], 
                "Actualizo una cita con el id" .  " "  .$_POST["id_cita"], 
                date("Y:m:d h:i:s") 
            );
    
           $rest["datos"] ="ok";
        }else{
            $rest["datos"] ="error";
        }
    }
    header("Content-Type: application/json");
    echo json_encode($rest);
}
//CITAS PARA Y TOMASDAS HOY


if(isset($_POST["citasHoy_Tomadas"])){
     $citasHoy= mdlCitas::CitasParaHoy($fechaActual);
     //ESTE BUSCA LAS CITAS QUE ESTAN PROGRAMADAS PARA HOY
     if($citasHoy!=false){
         $rest["citas_para_hoy"] = count($citasHoy);
         $rest["citas_resultado_hoy"] = "ok";
         
     }else{
        $rest["citas_para_hoy"]="500";
     }

     $citastomadasHoy= mdlCitas::CitastomadasHoy($fechaActual);
     if($citastomadasHoy!=false){
         $rest["citas_tomadas_hoy"] = count($citastomadasHoy);
         $rest["citas_tomadas_resultado_hoy"] = "ok";
         
     }else{
        $rest["citas_tomadas_hoy"]="500";
     }
     header("Content-Type: application/json");
     echo json_encode($rest);




}

//EDITAR CITA


//CREAR UN EVENTO
if(isset($_POST["programar_cita"])){
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343464");    
    if ($VPRUT) {
        //$data = json_decode($_POST['tipo']);
   //     var_dump($data);
   $registro= mdlCitas::ProgramarCita($_POST["id_cliente_"], $_SESSION["user_logged"],$_POST["id_cita"],$_POST["fecha"], $_POST["url"]);
      if($registro){
        $consultar= MdlProgramador::ConsultarEventoCitaUltimo();
        if($consultar){
            $movimiento  = mdlHistorial::RegistrarMovimiento(
                $_SESSION["user_logged"],
                "mensajes_programados", 
                $consultar[0]["id"], 
                "Se creo un mensaje Programado por el Agente", 
                date("Y:m:d h:i:s") 
            );
            $movimiento  = mdlHistorial::RegistrarMovimiento(
                $_SESSION["user_logged"],
                "crm_clientes", 
                $_POST["id_cliente"], 
                "se le creo un  evento", 
                date("Y:m:d h:i:s") 
            );
            $rest["citas"]=="ok";
        }
      } 
    }else{
        $rest["citas"] = "No"; 
    }
    header("Content-Type: application/json");
    echo json_encode($rest);
}
//CREAR UN EVENTO


//CREAR CITA Y EVENTOS PROGRAMADOS DE ESA CITA
if (isset($_POST["CrearCita"])) {
  
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343464");    
    if ($VPRUT) {
      $registro= mdlCitas::CrearCita($_POST["fecha"],$_POST["fecha2"], $_POST["title"], $_POST["id_clie"],"");
     if ($registro) { // preguntamos si el registro se realizó correctamente
        $listar = mdlCitas::MostrarCitas();
        if($listar){
            $fecha1 =$_POST["fecha_3_antes"];
            $fecha2 =$_POST["fecha_1_antes"]; 
            $fecha3 =$_POST["fecha_6_horas_antes"];
            $fecha4 =$_POST["fecha_3_despues"];
            $fecha5 =$_POST["fecha_1_despues"];
            $fecha6 =$_POST["fecha_6_horas_despues"];
            $registro= mdlCitas::ProgramarCita($_POST["id_clie"], $_SESSION["user_logged"], $listar[0]["id"],$fecha1, "");
            if($registro){
                $registro2 =mdlCitas::ProgramarCita($_POST["id_clie"], $_SESSION["user_logged"], $listar[0]["id"],$fecha2,"");
                if($registro2){
                    $registro3 =mdlCitas::ProgramarCita($_POST["id_clie"], $_SESSION["user_logged"], $listar[0]["id"],$fecha3, "");
                    if($registro3){
                        $registro4 =mdlCitas::ProgramarCita($_POST["id_clie"], $_SESSION["user_logged"], $listar[0]["id"],$fecha4, "");
                        if($registro4){
                            $registro5 =mdlCitas::ProgramarCita($_POST["id_clie"], $_SESSION["user_logged"], $listar[0]["id"],$fecha5, "");
                             if($registro5){
                                $registro6 =mdlCitas::ProgramarCita($_POST["id_clie"], $_SESSION["user_logged"], $listar[0]["id"],$fecha6,"");
                             }   
                         }
                    }
                }
            }     
            $movimiento  = mdlHistorial::RegistrarMovimiento(
                $_SESSION["user_logged"],
                "citas", 
                $listar[0]["id"], 
                "Registro una nueva cita", 
                date("Y:m:d h:i:s") 
            );
            if($movimiento){
                $movimiento  = mdlHistorial::RegistrarMovimiento2(
                    $_SESSION["user_logged"],
                    "crm_clientes", 
                    $_POST["id_clie"], 
                    "Se le creo una cita", 
                    date("Y:m:d h:i:s") 
                );
            }
            $rest["respuesta"]  = "ok";
        }else{
            $rest["respuesta"]  = "error";
        }
        //$rest["respuesta"] =$listar;
     }else{
         $rest["respuesta"]  = "500"; // error interno 
     }
    }else{
        $rest["citas"] = "No";
    }
    header("Content-Type: application/json");
    echo json_encode($rest);
}
//CREAR CITA Y EVENTOS PROGRAMADOS DE ESA CITA
?>
