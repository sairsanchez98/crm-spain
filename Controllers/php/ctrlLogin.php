<?php
require_once "../../Models/mdlLogin.php";
require_once "../../Models/mdlHistorial.php";
//date_default_timezone_set('America/Bogota');
date_default_timezone_set('Europe/Madrid');
if (isset($_POST["login"])) {
  
   
    if ($_POST["user"] !="" && $_POST["pass"] !="") {
      
        $pass = crypt($_POST["pass"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        $log = MdlLogin::Login($_POST["user"] , $pass);
        
       
         if($log!=false){
          $cantidad = count($log);
          if($cantidad>0){
            $usr =$log[0]["id"];
          }else{
            $usr =null;
          }
           
         }else{
           $usr =null;
         }
        $iduser = $usr;
        
          if ($iduser==null) {
          $rest["respuesta"] = "denegado";
          }else{

            $_permiso_ = "343453";
            $permisoLogin = MdlLogin::LoginPermiso($_permiso_);
            $permiso = 0;
            foreach($permisoLogin as $pl){
              if ($pl["id_usuario"] == $iduser) 
              { $permiso++; }
            }
            

            if ($permiso > 0) {
              $rest["respuesta"] = "ok";
              $movimiento  = mdlHistorial::RegistrarMovimiento($iduser,"usuarios",$iduser , "Inicio sesion", date("Y:m:d h:i:s") );
            
              $rest["user_session"] = $iduser;
            }else{
              $rest["respuesta"] = "denegado";
            }
              
          }
        
        

       

    }else if ($_POST["user"] =="" && $_POST["pass"] =="" ) {
      $rest["respuesta"] = "datanull";
    }else if ($_POST["user"] == "") {
      $rest["respuesta"] = "user_null";
    }elseif ($_POST["pass"] == "") {
      $rest["respuesta"] = "pass_null";
    }



    header("Content-Type: application/json");
    echo json_encode($rest);
}


  
 