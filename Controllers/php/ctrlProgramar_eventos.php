<?php

require_once"../../Models/mdlProgramador_eventos.php";
require_once"../../Models/mdlClientes.php";
require_once"../../Models/mdlCitas.php";
require_once"../../ext/carbon/vendor/autoload.php";

use Carbon\Carbon;
//date_default_timezone_set('America/Bogota');
//date_default_timezone_set('Europe/Madrid');
date_default_timezone_set('Europe/Madrid');
$fechaActual=   date("Y:m:d h:i:s") ;
echo $fechaActual;
$consultar= MdlProgramador::ConsultarEventoCita();
if($consultar){
  
  $cantidad =count($consultar);
  $arreglo_cliente =array();
  $arreglo_cita =array();
  $arreglo_general =array();
  
  for($i =0;$i<$cantidad;$i++){
  if(strtotime($fechaActual)-strtotime($consultar[$i]["fecha_envio"]) <60  & strtotime($fechaActual)-strtotime($consultar[$i]["fecha_envio"])>=0){
            $cliente = MdlClientes::UnicoCliente($consultar[$i]["id_cliente"]);
           $client = array(
              "id" => $cliente[0]["id"],
              "nombre" => $cliente[0]["nombre"],
              "dni" => $cliente[0]["dni"],
              "email" => $cliente[0]["email"],
              "telefono" => $cliente[0]["telefono"],
              "direccion" => $cliente[0]["direccion"],
              "fecha_registro" => $cliente[0]["fecha_registro"]);
              array_push($arreglo_cliente, $client);
              $cita = MdlCitas::CitaUnica($consultar[$i]["id_cita"]);
              $citas = array(
                "id" => $cita[0]["id"],
                "titulo" => $cita[0]["title"],
                "fecha_de_la_cita" => $cita[0]["start"],
                "Fecha_de_finalizacion_de_cita" => $cita[0]["end"],
                "estado"=>$cita[0]["estado"]
               );
                array_push($arreglo_cita, $citas);
                $arreglo=array(
                    "datos_cliente"=>$arreglo_cliente,
                    "datos_cita"=>$arreglo_cita
                );
              array_push($arreglo_general,$arreglo);
              $estado=MdlProgramador::EditarEstado($consultar[$i]["id"]);
      }
     
  
  }
  
 echo json_encode($arreglo_general);
}else{
  echo "No Hay Citas";
}
?>