<?php
session_start();
require_once "../../Models/mdlMarketing.php";
require_once "../../Models/mdlClientes.php";
require_once "../../Models/mdlAgentes.php";
require_once"../../Models/mdlPapelera.php";
require_once"../../Models/mdlHistorial.php";
require_once"../../Models/mdlCitas.php";
//date_default_timezone_set('America/Bogota');
//date_default_timezone_set('Europe/Madrid');


require_once "../../ext/carbon/vendor/autoload.php";

use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();

if (isset($_GET["permiso"])) {
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343459");  
    if($VPRUT){
        $rest["permiso"] = "ok";
    }else{
        $rest["permiso"] = "No";
    }


    header("Content-Type: application/json");
    echo json_encode($rest);
}
if (isset($_GET["FechaActual"])) {
    
  $rest["FechaActual"] = date("Y-m-d");

  header("Content-Type: application/json");
  echo json_encode($rest);
}


if(isset($_POST["filtro_marketing"])){

    if($_POST["filtro_estado"]!="" || ($_POST["fecha_inicial"]!=""|| $_POST["fecha_final"]!="")){
      $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343459");    
      if ($VPRUT) {
      
         if($_POST["filtro_estado"]!=""  & ($_POST["fecha_inicial"]=="" & $_POST["fecha_final"]=="")){
           $filtro_estado_unico = MdlMarketing::MarktetingFiltroEstado($_POST["filtro_estado"]);

        
          

           if($filtro_estado_unico){
            $marketing_info =array();
            foreach ($filtro_estado_unico as $marketing) 
             {
                 $ConsultarPapelera = mdlPapelera::ConsultarPapelera($marketing["id"], "marketing");
                 if (!$ConsultarPapelera) {
                    $marketing_f  = new Carbon($marketing["fecha_envio"]);
                     $mark= array(
                         "id" => $marketing["id"],
                         "mensaje" => $marketing["mensaje"],
                         "alcance" => $marketing["alcance"],
                         "fecha_envio" => $marketing["fecha_envio"],
                         "antiguedad"=>$marketing_f->diffForHumans(),
                         "catgoria_mensaje" => $marketing["catgoria_mensaje"],
                         "tipo" => $marketing["tipo"],
                         
                         
                     );
                     array_push($marketing_info, $mark);
                }
             } 
             $rest["registros"] = $marketing_info;
            
           }else{
            $rest["registros"] = "";
           }

         }else if(($_POST["filtro_estado"]== "" ||$_POST["filtro_estado"]== "todos") & ($_POST["fecha_inicial"]!="" & $_POST["fecha_final"]!="")){
          $filtro_fechas=MdlMarketing::FiltroFechasMarketing($_POST["fecha_inicial"], $_POST["fecha_final"]);

          if($filtro_fechas){
            $marketing_info =array();
            foreach ($filtro_fechas as $marketing) 
             {
                 $ConsultarPapelera = mdlPapelera::ConsultarPapelera($marketing["id"], "marketing");
                 if (!$ConsultarPapelera) {
                    $marketing_f  = new Carbon($marketing["fecha_envio"]);
                     $mark= array(
                         "id" => $marketing["id"],
                         "mensaje" => $marketing["mensaje"],
                         "alcance" => $marketing["alcance"],
                         "fecha_envio" => $marketing["fecha_envio"],
                         "antiguedad"=>$marketing_f->diffForHumans(),
                         "catgoria_mensaje" => $marketing["catgoria_mensaje"],
                         "tipo" => $marketing["tipo"],
                         
                         
                     );
                     array_push($marketing_info, $mark);
                }
             } 
             $rest["registros"] = $marketing_info;
            
           }else{
            $rest["registros"] = $filtro_fechas;
           }
         }else if(($_POST["filtro_estado"]!= "" & $_POST["filtro_estado"]!= "todos") & ($_POST["fecha_inicial"]!="" & $_POST["fecha_final"]!="")) {
          
      $filtro_todos=MdlMarketing::FiltroFechasEstadoMarketing($_POST["fecha_inicial"], $_POST["fecha_final"], $_POST["filtro_estado"]);
         
      if($filtro_todos){
        $marketing_info =array();
        foreach ($filtro_todos as $marketing) 
         {
             $ConsultarPapelera = mdlPapelera::ConsultarPapelera($marketing["id"], "marketing");
             if (!$ConsultarPapelera) {
                $marketing_f  = new Carbon($marketing["fecha_envio"]);
                 $mark= array(
                     "id" => $marketing["id"],
                     "mensaje" => $marketing["mensaje"],
                     "alcance" => $marketing["alcance"],
                     "fecha_envio" => $marketing["fecha_envio"],
                     "antiguedad"=>$marketing_f->diffForHumans(),
                     "catgoria_mensaje" => $marketing["catgoria_mensaje"],
                     "tipo" => $marketing["tipo"],
                     
                     
                 );
                 array_push($marketing_info, $mark);
            }        
        } 
         $rest["registros"] = $marketing_info;
        
       }else{
        $rest["registros"] = $filtro_todos;
       }
        }else if(($_POST["filtro_estado"]== "" || $_POST["filtro_estado"]== "todos") & ($_POST["fecha_inicial"]!="" || $_POST["fecha_final"]!="")){
          $filtro_fechas=MdlMarketing::FiltroFechasMarketing($_POST["fecha_inicial"], $_POST["fecha_final"]);
         

          if ($filtro_fechas){
            $marketing_info =array();
            foreach ($listar as $marketing) 
             {
                if (!$ConsultarPapelera) {
                    $marketing_f  = new Carbon($marketing["fecha_envio"]);
                     $mark= array(
                         "id" => $marketing["id"],
                         "mensaje" => $marketing["mensaje"],
                         "alcance" => $marketing["alcance"],
                         "fecha_envio" => $marketing["fecha_envio"],
                         "antiguedad"=>$marketing_f->diffForHumans(),
                         "catgoria_mensaje" => $marketing["catgoria_mensaje"],
                         "tipo" => $marketing["tipo"],
                         
                         
                     );
                     array_push($marketing_info, $mark);
                }
             } 
             $rest["registros"] = $marketing_info;
            
           }else{
            $rest["registros"] = $filtro_fechas;
           }
         }

       
        }else{
         $rest["registros"] = "No";
        }
    }else{
      $rest["registros"] = "";
    }
    echo json_encode($rest);
  }




if (isset($_POST["buscar"])) {
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343459");    
    if ($VPRUT) {
 
        
    $listar = MdlMarketing:: RegistrosMarketing(false);
    $marketing_info =array();
   foreach ($listar as $marketing) 
    {
        $ConsultarPapelera = mdlPapelera::ConsultarPapelera($marketing["id"], "marketing");
       if (!$ConsultarPapelera) {
          
            $mark = array(
                "id" => $marketing["id"],
                "mensaje" => $marketing["mensaje"],
                "alcance" => $marketing["alcance"],
                "fecha_envio" => $marketing["fecha_envio"],
                "catgoria_mensaje" => $marketing["catgoria_mensaje"],
                "tipo" => $marketing["tipo"],
                
                
            );
            array_push($marketing_info, $mark);
       }
    } 








    $rest["registros"] = $marketing_info;
    $filtros = MdlMarketing::FiltrosMarketing(false);
    $rest["filtros"] =$filtros;
   

    }else{
        $rest["registros"] = "No";
    }
    header("Content-Type: application/json");
    echo json_encode($rest);

}
if(isset($_POST["eliminar"])){
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343467");    
    if ($VPRUT) {
        $papelera = mdlPapelera::MoverAlapapelera($_POST["eliminar"],"marketing",  date("Y:m:d h:i:s"));

           if($papelera){
               $movimiento  = mdlHistorial::RegistrarMovimiento(
                    $_SESSION["user_logged"],
                    "marketing", 
                    $_POST["eliminar"], 
                    "Mover Cliente A la Papelera", 
                    date("Y:m:d h:i:s")
                );
            }
  $rest["respuesta"]=$papelera;
  
  header("Content-Type: application/json");
  echo json_encode($rest);

}
}
/// ESTOS METODOS LLAMAN AL MODELO DE CLIENTES
if(isset($_POST["email_masivo"])){
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343460");    
    if ($VPRUT) {
    if($_POST["filtro"]=="" || $_POST["filtro"]=="todos" ){
    
        $datos = array( 
       "asunto" => $_POST["asunto"],
       "mensaje" => $_POST["mensaje"],
       "fecha_envio" => date("Y:m:d h:i:s"),
       "tipo"=> "email",
       "categoria"=>  "masivo" );
        $listar = mdlClientes:: MostrarClientes($datos);
                // $listar = mdlClientes:: MostrarClientes(false);
           
               /* $movimiento  = mdlHistorial::RegistrarMovimiento(
                    $_SESSION["user_logged"],
                    "op_notas", 
                    $listar["id"], 
                    "Nueva nota", 
                    $listar["fecha_registro"]
                );
*/
          $rest = $listar;

        
          header("Content-Type: application/json");
          echo json_encode($rest);
    }else if($_POST["filtro"]!="" || $_POST["filtro"]!="todos"){
  
        $arreglo_email =array();
            $citas =MdlCitas::CitasFiltro($_POST["filtro"]);
            $contador =count($citas);
            for($i=0; $i<$contador;$i++){
              $cliente =MdlClientes::UnicoCliente($citas[$i]["id_cliente"]);
              $client=array(
                  
                  "id_cliente"=>$cliente[0]["id"],
                  "email"=>$cliente[0]["email"],
                  "mensaje"=>$_POST["mensaje"],
                  "asunto"=>$_POST["asunto"]
              );
              array_push($arreglo_email, $client);
              if($client[0]["id"]!=""){
                       $movimiento  = mdlHistorial::RegistrarMovimiento2(
                            $_SESSION["user_logged"],
                            "crm_clientes", 
                             $marketing[0]["id"], 
                            "recicbio un mensaje email",
                            $_POST["mensaje"], 
                            date("Y:m:d h:i:s")
                       );
              }
            }  
         $cantidad= count($arreglo_email);
              if($cantidad>0){
                $registrar =MdlMarketing::MensajeFiltroEmail($arreglo_email,$_POST["mensaje"],date("Y:m:d h:i:s"),$_POST["tipo"],$_POST["filtro"]);
                if($registrar){
                    $marketing= MdlMarketing::RegistrosMarketing(false);
                     if($marketing){
                        $movimiento  = mdlHistorial::RegistrarMovimiento(
                            $_SESSION["user_logged"],
                            "marketing", 
                             $marketing[0]["id"], 
                            "Envio un mensaje masivo a " . $marketing[0]["alcance"], 
                            date("Y:m:d h:i:s")
                        );
                     }
                    $enviar_mensaje =MdlMarketing::MensajesEmail($arreglo_email,$marketing[0]["id"],$_POST["asunto"],$_POST["mensaje"],date("Y:m:d h:i:s"));
                }
              }else{
                $enviar_mensaje ="No existe Ningun Cliente con citas ". $_POST["filtro"] . "s";
              }
             
            header("Content-Type: application/json");
            echo json_encode($enviar_mensaje);
    }
}else{
    $rest ="No";
}
}
if(isset($_POST["whatsapp_masivo"])){
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343461");    
        if ($VPRUT) {
    if($_POST["filtro"]=="" || $_POST["filtro"]=="todos" ){
        $datos = array( 
       "mensaje" => $_POST["mensaje"],
       "fecha_envio" => date("Y:m:d h:i:s"),
       "tipo"=> "whatsapp",
       "categoria"=>  "masivo" );
        $listar = mdlClientes:: MostrarClientes($datos);
                // $listar = mdlClientes:: MostrarClientes(false);
           
               /* $movimiento  = mdlHistorial::RegistrarMovimiento(
                    $_SESSION["user_logged"],
                    "op_notas", 
                    $listar["id"], 
                    "Nueva nota", 
                    $listar["fecha_registro"]
                );
*/
          $rest = $listar;
          header("Content-Type: application/json");
          echo json_encode($rest);
    }else if($_POST["filtro"]!="" || $_POST["filtro"]!="todos"){
        $arreglo_wha =array();
            $citas =MdlCitas::CitasFiltro($_POST["filtro"]);
            $contador =count($citas);
            for($i=0; $i<$contador;$i++){
              $cliente =MdlClientes::UnicoCliente($citas[$i]["id_cliente"]);
              $client=array(
                  "telefono"=>$cliente[0]["telefono"],
                  "mensaje"=>$_POST["mensaje"],
                  "id_cliente"=>$cliente[0]["id"]
              );
              if($cliente[0]["id"]!=""){
                $movimiento  = mdlHistorial::RegistrarMovimiento2(
                     $_SESSION["user_logged"],
                     "crm_clientes", 
                      $marketing[0]["id"], 
                     "recicbio un mensaje de whatsapp", 
                     $_POST["mensaje"],
                     date("Y:m:d h:i:s")
                );
       }
              array_push($arreglo_wha, $client);
            }
            $cantidad= count($arreglo_wha);
            if($cantidad>0){
                $registrar =MdlMarketing::MensajeFiltroEmail($arreglo_wha,$_POST["mensaje"],date("Y:m:d h:i:s"),$_POST["tipo"],$_POST["filtro"]);
                if($registrar){
                    $marketing= MdlMarketing::RegistrosMarketing(false);
                    $marketing= MdlMarketing::RegistrosMarketing(false);
                     if($marketing){
                        $movimiento  = mdlHistorial::RegistrarMovimiento(
                            $_SESSION["user_logged"],
                            "marketing", 
                             $marketing[0]["id"], 
                            "Envio un mensaje con el filtro a todos los clientes con citas".$_POST["filtro"] ." " ."lo recibieron " .  $marketing[0]["alcance"] . "persona(s)", 
                            date("Y:m:d h:i:s")
                        );
                     }
                    $enviar_mensaje =MdlMarketing::MensajesWhatsapp($arreglo_wha,$marketing[0]["id"],$_POST["mensaje"],date("Y:m:d h:i:s"));
                   if($enviar_mensaje){
                    $enviar_mensaje ="ok";
                   }
                 
                }
            }else{
                $enviar_mensaje ="No existe Ningun Cliente con citas ". $_POST["filtro"] . "s";
            }
           
            header("Content-Type: application/json");
            echo json_encode($enviar_mensaje);
    }
 }else{
    $rest = "No";
 }
}
if(isset($_POST["sms_masivo"])){
    
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343462");    
    if ($VPRUT) {
    if($_POST["filtro"]=="" || $_POST["filtro"]=="todos" ){
        echo $_POST["filtro"];
        $datos = array(   
       "mensaje" => $_POST["mensaje"],
       "fecha_envio" => date("Y:m:d h:i:s"),
       "tipo"=> $_POST["tipo"],
       "categoria"=>  "masivo" );
        $listar = mdlClientes:: MostrarClientes($datos);
          $rest = $listar;       
          header("Content-Type: application/json");
          echo json_encode($rest);
    }else if($_POST["filtro"]!="" || $_POST["filtro"]!="todos"){
        $arreglo_sms =array();
     
        $citas =MdlCitas::CitasFiltro($_POST["filtro"]);
        $contador =count($citas);
        for($i=0; $i<$contador;$i++){
          $cliente =MdlClientes::UnicoCliente($citas[$i]["id_cliente"]);
          $client=array(
              "telefono"=>$cliente[0]["telefono"],
              "mensaje"=>$_POST["mensaje"],
              "id_cliente"=>$cliente[0]["id"]
          );
          if($cliente[0]["id"]!=""){
            $movimiento  = mdlHistorial::RegistrarMovimiento2(
                 $_SESSION["user_logged"],
                 "crm_clientes", 
                  $marketing[0]["id"], 
                 "recicbio un mensaje sms",
                 $_POST["mensaje"], 
                 date("Y:m:d h:i:s")
            );
   }
          array_push($arreglo_sms, $client);
        }
        $cantidad= count($arreglo_sms);
              if($cantidad>0){
                $registrar =MdlMarketing::MensajeFiltroEmail($arreglo_sms,$_POST["mensaje"],date("Y:m:d h:i:s"),$_POST["tipo"],$_POST["filtro"]);
                if($registrar){
                    
                    $marketing= MdlMarketing::RegistrosMarketing(false);
                     if($marketing){
                        $movimiento  = mdlHistorial::RegistrarMovimiento(
                            $_SESSION["user_logged"],
                            "marketing", 
                             $marketing[0]["id"], 
                            "Envio un mensaje masivo a " . $marketing[0]["alcance"] . "persona(s)", 
                            date("Y:m:d h:i:s")
                        );
                     }
                    $enviar_mensaje =MdlMarketing::MensajesSms($arreglo_sms,$marketing[0]["id"],$_POST["mensaje"],date("Y:m:d h:i:s"));
                }
              }else{
                $enviar_mensaje="No existe Ningun Cliente con citas ". $_POST["filtro"] . "s";
            }
      
        header("Content-Type: application/json");
        echo json_encode($enviar_mensaje);
    }
}else{
    $rest ="No";
}
}
/*
if(isset($_POST["email_masivo"])){
    if($_POST["filtro"]!="" && $_POST["filtro"]!="Escoge" ){
        $datos = array( 
       
       "mensaje" => $_POST["mensaje"],
       "fecha_envio" => date("Y:m:d h:i:s"),
       "tipo"=> $_POST["tipo"],
       "categoria"=>  "masivo" );
        $listar = mdlClientes:: MostrarClientes($datos);
   
          $rest = $listar;
          header("Content-Type: application/json");
          echo json_encode($rest);
    }
}
if(isset($_POST["whatsapp_masivo"])){
    if($_POST["filtro"]!="" && $_POST["filtro"]!="Escoge" ){
        $datos = array( 
       
       "mensaje" => $_POST["mensaje"],
       "fecha_envio" => date("Y:m:d h:i:s"),
       "tipo"=> $_POST["tipo"],
       "categoria"=>  "masivo" );
        $listar = mdlClientes:: MostrarClientes($datos);
        
          $rest = $listar;
          header("Content-Type: application/json");
          echo json_encode($rest);
    }
}
if(isset($_POST["sms_masivo"])){
    if($_POST["filtro"]!="" && $_POST["filtro"]!="Escoge" ){
        $datos = array( 
       
       "mensaje" => $_POST["mensaje"],
       "fecha_envio" => date("Y:m:d h:i:s"),
       "tipo"=> $_POST["tipo"],
       "categoria"=>  "masivo" );
        $listar = mdlClientes:: MostrarClientes($datos);
        
          $rest = $listar;
          header("Content-Type: application/json");
          echo json_encode($rest);
    }
}*/

  
 