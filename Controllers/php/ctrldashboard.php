<?php
  require_once"../../Models/mdlHistorial.php";
  require_once "../../ext/carbon/vendor/autoload.php";
session_start();
  use Carbon\Carbon;
  date_default_timezone_set('America/Bogota');
  Carbon::setLocale('es');
  $fechaActual = Carbon::now()->toDateString();

    if(isset($_POST["BuscarCantidad"])){
        $datos_movimiento_diario = mdlHistorial::MivimientosSemana($fechaActual);
        if($datos_movimiento_diario){
            //buscar la cantidad de datos
            $leadsCantidad =0; $citasCantidad =0; $sms=0; $smsCantidad=0;$emailCantidad =0;$whatsapp=0;$servicio=0; $clientesCantidad=0;
            foreach($datos_movimiento_diario as $datos){
              //buscar de l historial yu contar cada item de los espesificados
               if($datos["tabla_afectada"] =="lead"){
                   $leadsCantidad=$leadsCantidad+ 1;
               } if($datos["tabla_afectada"] =="crm_clientes"){
                   $clientesCantidad=$clientesCantidad+ 1;
               } if($datos["tabla_afectada"]=="whatsapp_resgistros"){
                   $whatsapp=$whatsapp+ 1;
               } if($datos["tabla_afectada"]=="sms_registros"){
                   $sms=$sms+ 1;
               } if($datos["tabla_afectada"]=="email_registro"){
                   $emailCantidad=$emailCantidad+ 1;
              } if($datos["tabla_afectada"] =="servicios"){
                  $servicio=$servicio+ 1;
              } if($datos["tabla_afectada"]=="citas" || $datos["tabla_afectada"]=="eventos_programados"){
                  $citasCantidad=$citasCantidad+ 1;
              }
            }
            $rest["consultacorrecta"] ="ok";
            $rest["total"] =$leadsCantidad+ $citasCantidad+ $whatsapp+ $sms+$emailCantidad+$servicio + $clientesCantidad;//cantidad de datos de toda la consulta
            $rest["datos_cantidad"] = array($leadsCantidad, $citasCantidad, $whatsapp, $sms, $emailCantidad,$servicio, $clientesCantidad);
        }else{
            $rest["consultacorrecta"] ="Ocurrio un problema";
        }
        echo json_encode($rest);
    }
    if(isset($_POST["crear_noticia"])){

        if($_POST["titulo_noticia"]!="" && $_POST["mensaje_noticia"]){
             $datos=array($_SESSION["user_logged"],"titulo"=>$_POST["titulo_noticia"], "mensaje"=>$_POST["mensaje_noticia"], $_POST[["imagen"]]);
             $noticias_registro = mdldashboard::RegistrarNoticia($datos);   
        }
    }
   
?>