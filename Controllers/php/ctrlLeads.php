<?php
require_once"../../Models/mdlLeads.php";
require_once"../../Models/mdlAgentes.php";
require_once"../../Models/mdlClientes.php";
require_once"../../Models/mdlPapelera.php";
require_once"../../Models/mdlHistorial.php";
//date_default_timezone_set('America/Bogota');
date_default_timezone_set('Europe/Madrid');
session_start();
require_once "../../ext/carbon/vendor/autoload.php";

use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();

if (isset($_GET["permiso"])) {
    $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343454");  
    if($VPRUT){
        $rest["permiso"] = "ok";
    }else{
        $rest["permiso"] = "No";
    }


    header("Content-Type: application/json");
    echo json_encode($rest);
}
if (isset($_GET["FechaActual"])) {
    
  $rest["FechaActual"] = date("Y-m-d");

  header("Content-Type: application/json");
  echo json_encode($rest);
}



if(isset($_POST["leads"])){
    $leads__ =  json_decode($_POST["leads"], true); 

    if (count($leads__) > 0) { // si el total de leads es mayor a cero entonces
      ## quiere decir que debemos procesar los leads 

        ## 1. CARGAMOS LOS CLIENTES PARA LUEGO ASOCIARLOS CON SU RESPECTIVO LEAD
        $clientes_info =array();
        $listar = mdlClientes:: MostrarClientes(false);
        foreach ($listar as $cliente) 
        {
            $ConsultarPapelera = mdlPapelera::ConsultarPapelera($cliente["id"], "clientes");
            if (!$ConsultarPapelera) {       
                  $client = array(
                      "id" => $cliente["id"],
                      "email" => $cliente["email"],
                      "telefono" => $cliente["telefono"]
                  );
                  array_push($clientes_info, $client);
            }
        }
        

        ## 2. VALIDAMOS LA INFORMACIÓN DE LOS LEADS Y REGISTRAMOS
        
        $contadorLeadsRegistrados = 0;
        $contadorLeadsNoRegistrados = 0;
        $contadorLeadsNoAsociados = 0; // no se encuentra ningún cliente cone sos contactos
        $LeadsNoRegistrados = array(); // por error
        $leadsNoAsociados = array(); // porque no se encontraron relacionadoscon un cliente

        foreach($leads__ as $lead){ ## recorremos cada lead
          $leadAsociado = false;
          foreach($clientes_info as $cliente){ ## para compararlo con cada cliente
              if($lead["email"] == $cliente["email"] || $lead["telefono"] == $cliente["telefono"]){ 
                  ## si el email o el telefono son equivalentes (cliente-lead) entonces...
                  $registrar = MdlLeads::RegistrarLeads(
                      $cliente["id"],
                      $lead["origen"],
                      $lead["nombre"],
                      $lead["telefono"],
                      $lead["email"],
                      $fechaActual
                  );

                  if($registrar == true){
                    $contadorLeadsRegistrados ++; 
                  } ## si se registra el lead
                  ## es porque sus campos fueron escritos correctamente (origen, nombre, telefono, email)
                  else{ ## sino ...
                    $contadorLeadsNoRegistrados ++;
                    array_push($LeadsNoRegistrados  ,  $lead); ## indicamos cuales fueron rechazados
                  }
                  $leadAsociado = true;
                break;
              }
          }   
          //si despues de finalizar el analisis del lead con cada cliente la variable leadAsociado
          ## está en false entonces se agregará el lead al conjunto de leads no asociados
          if (!$leadAsociado) {
            array_push($leadsNoAsociados , $lead);
            $contadorLeadsNoAsociados ++;
          }
        }

        $rest["respuesta"] = "LEADS_PROCESADOS";
        $rest["numero_leads_registrados"] = $contadorLeadsRegistrados ;
        $rest["numero_leads_no_asociados"] = $contadorLeadsNoAsociados;
        $rest["numero_leads_no_registrados"] = $contadorLeadsNoRegistrados ;
        $rest["leads_no_registrados"] = $LeadsNoRegistrados;
        $rest["leadsNoAsociados"] = $leadsNoAsociados;
        


    }else{
      $rest["respuesta"] = "NO_SE_ENCONTRARON_LEADS";
    }
    
 
   
    header("Content-Type: application/json");
    echo json_encode($rest);
 }











  if(isset($_POST["edita"])){
   
   $actualizar=MdlLeads::Actualizar($_POST["estado"],$_POST["id"]);
  
   if($actualizar){
    $movimiento  = mdlHistorial::RegistrarMovimiento(
      $_SESSION["user_logged"],
      "lead", 
      $_POST["id"], 
      "actualizo el estado del leads a ". $_POST['estado'], 
      date("Y:m:d h:i:s")
   );
   $rest["respuesta"] ="ok";
   }else{
    $rest["respuesta"] = "error";
   }
   header("Content-Type: application/json");
   echo json_encode($rest);
  }



  if(isset($_POST["filtro_leads"])){

    if($_POST["filtro_estado"]!="" || ($_POST["fecha_inicial"]!=""|| $_POST["fecha_final"]!="")){
      $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343458");    
      if ($VPRUT) {
        $VPRUT = mdlAgentes::verPermisosDelAgente($_SESSION["user_logged"], "343465");    
        if ($VPRUT) {
         if($_POST["filtro_estado"]!=""  & ($_POST["fecha_inicial"]=="" & $_POST["fecha_final"]=="")){
           $filtro_estado_unico = MdlLeads::LeadsFiltroEstado($_POST["filtro_estado"]);
           $leads =array();

           if($filtro_estado_unico){
            
              foreach ($filtro_estado_unico as $lead) {
                $clientes =MdlClientes::UnicoCliente($lead["id_cliente"]);
                
               if(count($clientes)>0){
                $da =array("id_leads"=> $lead["id"],"cliente"=>$clientes[0], "estado"=>$lead["estado"], "fecha"=>$lead["fecha"]);
                array_push($leads, $da);
              }
              }
           }else{
            $rest["leads"] = "";
           }

         }else if(($_POST["filtro_estado"]== "" ||$_POST["filtro_estado"]== "todos") & ($_POST["fecha_inicial"]!="" & $_POST["fecha_final"]!="")){
          $filtro_fechas=MdlLeads::FiltroFechasLeads($_POST["fecha_inicial"], $_POST["fecha_final"]);
          $leads =array();

           if($filtro_fechas){
            
              foreach ($filtro_fechas as $lead) {
                $clientes =MdlClientes::UnicoCliente($lead["id_cliente"]);
                
               if(count($clientes)>0){
                $da =array("id_leads"=> $lead["id"],"cliente"=>$clientes[0], "estado"=>$lead["estado"], "fecha"=>$lead["fecha"]);
                array_push($leads, $da);
              }
              }
           }else{
            $rest["leads"] = "";
           }
         }else if(($_POST["filtro_estado"]!= "" & $_POST["filtro_estado"]!= "todos") & ($_POST["fecha_inicial"]!="" & $_POST["fecha_final"]!="")) {
          
      $filtro_todos=MdlLeads::FiltroFechasEstadoLeads($_POST["fecha_inicial"], $_POST["fecha_final"], $_POST["filtro_estado"]);
         
          $leads =array();

           if($filtro_todos){
            
              foreach ($filtro_todos as $lead) {
                $clientes =MdlClientes::UnicoCliente($lead["id_cliente"]);
                
               if(count($clientes)>0){
                $da =array("id_leads"=> $lead["id"],"cliente"=>$clientes[0], "estado"=>$lead["estado"], "fecha"=>$lead["fecha"]);
                array_push($leads, $da);
              }
              }
         }
        }else if(($_POST["filtro_estado"]== "" || $_POST["filtro_estado"]== "todos") & ($_POST["fecha_inicial"]!="" || $_POST["fecha_final"]!="")){
          $filtro_fechas=MdlLeads::FiltroFechasLeads($_POST["fecha_inicial"], $_POST["fecha_final"]);
          $leads =array();

           if($filtro_fechas){
            
              foreach ($filtro_fechas as $lead) {
                $clientes =MdlClientes::UnicoCliente($lead["id_cliente"]);
                
               if(count($clientes)>0){
                $da =array("id_leads"=> $lead["id"],"cliente"=>$clientes[0],"origen"=>$lead["origen"] ,"estado"=>$lead["estado"], "fecha"=>$lead["fecha"]);
                array_push($leads, $da);
              }
              }
           }else{
            $rest["leads"] = "";
           }
         }
         $rest["leads"] =$leads;
        }else{
         $rest["leads"] = "No";
        }
        }else{
         $rest["leads"] = "No";
        }
    }else{
      $rest["leads"] = "";
    }
    echo json_encode($rest);
  }
  

    
  
    
    

?>



