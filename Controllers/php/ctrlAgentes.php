<?php   

require_once"../../Models/mdlAgentes.php";
require_once"../../Models/mdlHistorial.php";
require_once "../../ext/carbon/vendor/autoload.php";

use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();


////////////////LISTAR O MOSTRAR AGENTES ////////////////
if (isset($_POST["listarTodo"])) {
    $listar = mdlAgentes::MostrarAgentes(false);
    $rest["agentes"] = $listar;
    header("Content-Type: application/json");
    echo json_encode($rest);
}

//la funcion esta en mdlhistorial
//buscara los agentes y las citas que registraron hoy
if(isset($_POST["datos_agentes_citas"])){
    $agentes = mdlAgentes::MostrarAgentes(false);
    $arreglo_agente_datos_citas = array();
    foreach($agentes as $agente){
      $historial = mdlHistorial::HistorialDeUnagentepordia($agente["id"],$fechaActual, "citas", "Registro una nueva cita");
      if(count($historial)){
        $cantidad_citas = count($historial);
        $dato_agente_cita  = array("id_agente"=>$agente["id"], "cantidad_citas"=> $cantidad_citas, "nombre_agente"=>$agente["nombre"]);
        array_push($arreglo_agente_datos_citas, $dato_agente_cita); 
      }
   
    }
    if(count($arreglo_agente_datos_citas>0)){
        $rest["agentes_citas"] =$arreglo_agente_datos_citas ;
    }else{
        $rest["agentes_citas"] = [];
    }    
    header("Content-Type: application/json");
    echo json_encode($rest);
}


if(isset($_POST["DatosAgente"])){
    session_start();
  if($_SESSION["user_logged"]!=""){
      $datos_agente = mdlAgentes::MostrarunAgente($_SESSION["user_logged"]);
      if(count($datos_agente)>0){
          $datos =  array("nombre"=> $datos_agente[0]["nombre"], "tipo_agente"=>$datos_agente[0]["usuario"], "id"=>$datos_agente[0]["id"]); 

        $rest["agente"] = $datos;
        $rest["resultado"] = "ok";

      }else{
        $rest["resultado"] ="error";
      }

  }else{
      $rest["resultado"] ="este agente no existe";
  }   

  header("Content-Type: application/json");
  echo json_encode($rest);
}


//// REGISTRAR AGENTES ////////////
if (isset($_POST["registrar"])) {
    
    $datos = array(
        "nombre" => $_POST["nombre"],
        "usuario" => $_POST["usuario"],
        "contrasena" => crypt($_POST["contrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$')
    );

    // verificación de datos completos: 
        if ($datos["nombre"] != "" && $datos["usuario"] != "" && $datos["contrasena"] != "") 
        {
                $usuario = MdlAgentes::RegistrarAgente($datos);
                if ($usuario) {
                    $rest["respuesta"] = "ok";
                }
        }else{
            $rest["respuesta"] = "data_nulled";
        }



    
    header("Content-Type: application/json");
    echo json_encode($rest);
}



////////// TRAER PERMISOS DEL USUARIO AGENTE ////////////
if (isset($_POST["traerServicios"])) {
    $PermisosAgente = array();
    
    $permisos = mdlAgentes::VerPermisos();
    session_start();
    foreach ($permisos as $key => $permiso) 
    {

        $consulta = mdlAgentes::VerPermisosDelAgente($_SESSION["user_logged"], $permiso["id"]);    
        if ($consulta ) {
            $arraydb = array(
                "id"=>$permiso['id'],
                "permiso"=> $permiso['permiso'],
                "categoria" => $permiso['categoria'] ,
                "activado" => true
            );
            array_push($PermisosAgente, $arraydb);
            //array_push($PermisosActivados, $permiso);
        }else{
            $arraydb = array(
                "id"=>$permiso['id'],
                "permiso"=> $permiso['permiso'],
                "categoria" => $permiso['categoria'] ,
                "activado" => false
            );
            array_push($PermisosAgente, $arraydb);
        }
    }

    // $rest["permisosActivados"] = $PermisosActivados;
    // $rest["permisosDesactivados"] = $PermisosNoActivados;
    $rest["PermisosAgente"] = $PermisosAgente;

   


    header("Content-Type: application/json");
    echo json_encode($rest);
}



/// ACTUALIZAR LOS DATOS DE LOS AGENTES ////

if (isset($_POST["GuardarCambios"])) {
    if($_POST["GuardarCambios"] == "permisos"){
        

        
        // reviso si el usuario agente ya tiene relación con el servico
        $relacionPermisoAgente = mdlAgentes::VerPermisosDelAgente($_POST["id_agente"], $_POST["permisoAgente"]);    
        if ($relacionPermisoAgente) { // si existe una relaión, entonces: 
            // si la relación entre el permiso y el usuario agente existe y el estado que viene por parte
            // del usuario administrados al querer guardar cambio es TRUE entonces no es necesario 
            // marcar nuevamente la relación (si ya EXISTE)
            if ($_POST["estado"] == false) { // en la base de datos hay una relación pero el admin ha
                // envíado un valos falso para este permiso ... hay que actualizar entonces.
                $Actualizacion = mdlAgentes::ActualizarPermisos($_POST["id_agente"],
                $_POST["permisoAgente"],$_POST["estado"]);
            }
            $rest["respuesta"]="ok";
        }else{
            if ($_POST["estado"] == true) { // en la base de datos NO hay una relación pero el admin ha
                // envíado un valos verdadero para este permiso ... hay que actualizar entonces. hay qeu agregar este permiso a este usuario
                $Actualizacion = mdlAgentes::ActualizarPermisos($_POST["id_agente"],
                $_POST["permisoAgente"],$_POST["estado"]);
            }
            
        }
         
    }
    
    
    
    header("Content-Type: application/json");
    echo json_encode($rest);
}