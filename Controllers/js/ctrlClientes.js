var CT = new Vue({
    el: "#clientes",
    data:{
        /// DATOS PARA UN NUEVO CLIENTE////
        dni: "",
        mensajeNoLista:true,
        nombre: "",
        telefono: "",
        email: "",
        direccion: "",
        lists: [],
        name: '',
         ///FIN  DATOS PARA UN NUEVO CLIENTE////

        //filtros base de datos
        fecha_final:"",
        fecha_inicio:"",

         //VARIABLES DE CARGAS Y MODAL//
        modal:false,
        loaderLeft: "none",
        loader:"none",
        
        //FIN VARIABLES DE CARGAS Y MODAL//


        ///VARIABLES PARA SER CONTENEDORES//
        cliente_s:"",
        ClientesEncontrados: [],
        AlertEliminarCliente:false,
        ClienteHistorial:false,
        ClientesQueMuestran:[],
        ///FIN VARIABLES PARA SER CONTENEDORES//


        ///// CLIENTES ALMACENADOS EN LA DB, HISTORIAL Y CLIENTE ESCOGIDO//////
        clientes:[],
        Cliente:[],
        HistorialCliente:[],
        ///// CLIENTES ALMACENADOS EN LA DB, HISTORIAL Y CLIENTE ESCOGIDO//////
        
        Alert_telefono :'border-default' ,
      
        ///DATOS ENVIAR CORREO///
        correo_mensaje:"",
        asunto_mensaje:"",
        mensaje_mensaje:"",
         ///FIN DATOS ENVIAR CORREO///


       ///DATOS WHATSAPP//
        w_numero:"",
        w_mensaje:"",
        w_numero_servidor:"",
         ///FIN DATOS WHATSAPP//

        //DATOS DE ENVIAR SMS//
        sms_numero:0,
        sms_mensaje:"",
       //FIN DATOS DE ENVIAR SMS//

        //// ESTADOS ////
      
        correo:null,
        mensaje:null,
        asunto:null,
        buscar:"",
       ////FIN  ESTADOS ////

        ///  COMPONENTES /////
        MensajeSms:false,
        ListaClientes: true,
        whatsapp:false,
        formRegistrarClientes : false,
        infoCliente: false,
        correo:false,
        habilitar:false,
        editar_:false,
        //FIN COMPONENTES//


        //control de botones//
        botonesclientes:true,
        botonessms:false
          //FIN control de botones//
       
    },
    computed: {
        searchUser: function () {
            return this.lists.filter((item) => item.dni.includes(this.dni));
        }
    },

    methods: 
    
    {
        
         filtrar(){
                var formdata = new FormData();  
                console.log(formdata)
               formdata.append("filtro_clientes", "ok");
                this.loaderLeft ="block"
                formdata.append("fecha_inicial", this.fecha_inicio);
                formdata.append("fecha_final", this.fecha_final);
               
                var vm = this;
              
                axios.post("Controllers/php/ctrlClientes.php", formdata).then((respuesta)=>{
                    
                    if ( $.fn.dataTable.isDataTable( '#datable_1' ) ) {
                        CT.ClientesQueMuestran = $('#datable_1').DataTable();
                    }
                    else {
                        CT.ClientesQueMuestran = $('#datable_1').DataTable( {
                            paging: false
                        } );
                    }
                    CT.ClientesQueMuestran.clear().draw()
               
                     console.log(respuesta)
                    if(respuesta.data.clientes =="No"){
                        if ( $.fn.dataTable.isDataTable( '#datable_1' ) ) {
                            CT.ClientesQueMuestran = $('#datable_1').DataTable();
                        }
                        else {
                            CT.ClientesQueMuestran = $('#datable_1').DataTable( {
                                paging: false
                            } );
                        }
                        CT.ClientesQueMuestran.clear().draw()
                   
                        CT.loaderLeft="none"
                  
                        CT.ListaClientes=false;
                        
                    
                        CT.mensajeNoLista =true;
                         CT.ShowAlerts("leads", "no");
                      
                    }else{
                        CT.loaderLeft="none"
                        var datos  = respuesta.data;
                        CT.clientes = datos.clientes;
                        CT.lists =datos.clientes;
                        CT.ListaClientes =true;
                        CT.mensajeNoLista =false;
                      
                       if ( $.fn.dataTable.isDataTable( '#datable_1' ) ) {
                        CT.ClientesQueMuestran = $('#datable_1').DataTable();
                    }
                    else {
                        CT.ClientesQueMuestran = $('#datable_1').DataTable( {
                            paging: false
                        } );
                    }
                    CT.ClientesQueMuestran.clear().draw()
               
                        datos.clientes.forEach(user=>{
                            CT.ClientesQueMuestran.row.add([
                             `
                             <a href="javascript:void(0);"  onclick="cargar(${user.id})" class="media">
                             <div class="media-body">
                              <div>
                                  <div class="email-head">Nombre: ${user.nombre} </div>
                                  
                                  <div class="email-subject">  ${user.fecha_registro} </div>
                              </div>
                              <div>
                                  <div class="last-email-details">
                                  </div>
                                  </div>
                              </div>
                           </a>`
                            ]).draw(false)
                            })
                    }
                })
             
             
          
            
         },
    
       
        EnviarMensajeAlWhastapp:function(action){
            switch(action)
            {
                case "whatsapp": /// mostrar formulario de registro:
                    CT.HabilitarComponente("whatsapp");
                    
                    
                break;
                case "enviar" : 
                  if(CT.w_mensaje !== "" && CT.w_numero!== "" )
                  {
                    var contador =0;
                    for(var i=0;i<CT.clientes.length;i++){
                        if(CT.w_numero ==CT.clientes[i].telefono){
                            contador++
                    var formdata = new FormData();
                    formdata.append("Enviarwhatsapp", "ok");
                    formdata.append("w_telefono", CT.w_numero);
                    formdata.append("w_mensaje", CT.w_mensaje);
                    formdata.append("id_cliente", this.clientes[i].id)
                    axios.post("Controllers/php/ctrlClientes.php", formdata)
                    .then(function(response){
                        var  datos =  response.data;
                        console.log(datos)
                       if(datos.respuesta == "ok"){
                           CT.ShowAlerts("cliente", "mensaje_enviado");
                           CT.HistoRial();
                           CT.HabilitarComponente("infoCliente") 
                            /// formateamos: 
                            CT.w_mensaje = "";
                        }else if(datos.respuesta == "00"){
                            CT.ShowAlerts("cliente", "DatosIncompletos_m");
                        }else if(datos.respuesta == "500"){
                            CT.ShowAlerts("cliente", "500");
                        }else if(datos.respuesta == "No"){
                            CT.ShowAlerts("cliente", "no");
                        }
                    });
                }
                }
                if(contador==0){
                    CT.ShowAlerts("cliente", "w_no_existe");
                
                }


                  }else{
                    CT.ShowAlerts("cliente", "DatosIncompletos_m");
                  }
                break;
            }
        },
        EnviarMensajeAlSms(action){
            switch(action)
            {
                case "sms": /// mostrar formulario de registro:
                    CT.HabilitarComponente("sms");
                break;
                case "enviar" : 
                  if(CT.sms_mensaje != "" && CT.sms_numero!= "" )
                  {
                      var contador =0;
                    for(var i=0;i<CT.clientes.length;i++){
                      
                        if(CT.sms_numero ==CT.clientes[i].telefono){
                            contador++
                            
                            var formdata = new FormData();
                            formdata.append("sms", "ok");
                            formdata.append("s_numero", CT.sms_numero);
                            formdata.append("s_mensaje", CT.sms_mensaje);
                            formdata.append("id_cliente", this.clientes[i].id) 
                            axios.post("Controllers/php/ctrlClientes.php", formdata)
                            .then(function(response){
                                var  datos =  response.data;
                                console.log(datos.respuesta);
                               if(datos.respuesta == "ok"){
                                    CT.ShowAlerts("cliente", "mensaje_enviado_sms");
                                    CT.HistoRial();
                                    CT.HabilitarComponente("infoCliente") 
                                    CT.infoCliente=true
                                    CT.MensajeSms=false
                                    /// formateamos: 
                                    CT.sms_mensaje = "";
                                }else if(datos.respuesta == "00"){
                                    CT.ShowAlerts("cliente", "DatosIncompletos_m");
                                }else if(datos.respuesta == "500"){
                                    CT.ShowAlerts("cliente", "500");
                                }else if(datos.respuesta == "No"){
                                    CT.ShowAlerts("cliente", "no");
                                }
                            });
                        }
                    }
                    if(contador==0){
                        CT.ShowAlerts("cliente", "telefono_no_existe");
                    
                    }
                  }else{
                    CT.ShowAlerts("cliente", "DatosIncompletos_m");
                  }
                break;
            }
        },
        EnviarMensajeCorreo: function(action)
        {
            switch(action)
            {
                case "correo": /// mostrar formulario de registro:
                    CT.HabilitarComponente("correo");           
                break;
                case "enviar" : 
                  if(CT.mensaje_mensaje != "" && CT.correo_mensaje!= "" && CT.asunto_mensaje != "")
                  {
                    var contador =0;
                    for(var i=0;i<CT.clientes.length;i++){
                      
                        if(CT.correo_mensaje ==CT.clientes[i].email){
                            contador++
                    var formdata = new FormData();
                    formdata.append("Enviarcorreo", "ok");
                    formdata.append("correo_m", CT.correo_mensaje);
                    formdata.append("asunto", CT.asunto_mensaje);
                    formdata.append("mensaje", CT.mensaje_mensaje);
                    formdata.append("id_cliente",CT.clientes[i].id)
                    axios.post("Controllers/php/ctrlClientes.php", formdata)
                    .then(function(response){
                        var  datos =  response.data;
                        console.log(datos);
                       if(datos.respuesta == "ok"){
                          CT.ShowAlerts("cliente", "mensaje_enviado");
                          CT.HistoRial();
                          CT.correo=false
                          CT.HabilitarComponente("infoCliente") 
                            /// formateamos: 
                          CT.asunto_mensaje = "";
                          CT.mensaje_mensaje = "";   
                        }else if(datos.respuesta == "00"){
                          CT.ShowAlerts("cliente", "DatosIncompletos_m");
                        }else if(datos.respuesta == "email_err"){
                          CT.ShowAlerts("cliente", "EmailInvalido");
                        }else if(datos.respuesta == "500"){
                          CT.ShowAlerts("cliente", "500");
                        }else if(datos.respuesta == "No"){
                          CT.ShowAlerts("cliente", "no");
                        }
                    });
                }
            }
            if(contador==0){
                CT.ShowAlerts("cliente", "email_no_existe");
            
            }
                  }else{
                    CT.ShowAlerts("cliente", "DatosIncompletos_m");
                  }
                break;
            }
        },
      

        //////////// REGISTRAR CLIENTE ///////
        RegistrarCliente: function(action)
        {  
            switch(action)
            {
                case "form": /// mostrar formulario de registro:
                    CT.HabilitarComponente("form");
                break;
                case "Procesar" : ///// Control de datos y registro en la base de datos
                  if( CT.telefono != "" )
                  {
                    var formdata = new FormData();
                    formdata.append("registrar", "ok");
                    formdata.append("cita_cliente", "no")
                    formdata.append("nombre", CT.nombre);
                    formdata.append("dni", CT.dni);
                    formdata.append("telefono", CT.telefono);
                    formdata.append("email", CT.email);
                    formdata.append("direccion", CT.direccion);
                    axios.post("Controllers/php/ctrlClientes.php", formdata)
                    .then(function(response){
                        var  datos =  response.data;
                        if(datos.respuesta == "ok"){
                            CT.ShowAlerts("cliente", "ok");
                            CT.ShowAlerts("cliente", "ok_");
                            CT.HabilitarComponente("lista");
                            CT.MostrarClientes(false);
                            /// formateamos: 
                            CT.dni = "";
                            CT.nombre = "";
                            CT.telefono = ""; 
                            CT.email = ""; 
                            CT.direccion = "";
                        }else if(datos.respuesta == "00"){
                            CT.ShowAlerts("cliente", "DatosIncompletos");
                        }else if(datos.respuesta == "email_err"){
                            CT.ShowAlerts("cliente", "EmailInvalido");
                        }else if(datos.respuesta == "500"){
                            CT.ShowAlerts("cliente", "500");
                        }else if(datos.respuesta == "No"){
                            CT.ShowAlerts("cliente", "no");}
                    });
                  }else{
                    this.Alert_telefono= "border-danger"
                    CT.ShowAlerts("cliente", "DatosIncompletos");
                  }
                break;
            }
        },
        //////////MOSTRAR CLIENTES //////////////
        MostrarClientes: function(id)
        {
            if(!id){
                formdata = new FormData();
                formdata.append("listarTodo", "ok");
                this.loaderLeft ="block"
                axios.post("Controllers/php/ctrlClientes.php", formdata)
                .then(function(response){
                    CT.loaderLeft="none"
                   if(response.data.clientes=="No"){
                    CT.ShowAlerts("cliente", "no");
                    CT.ListaClientes =false;
                    CT.mensajeNoLista =true;
                   }else{

                    var datos  = response.data;
                    CT.clientes = datos.clientes;
                    CT.lists =datos.clientes;
                    CT.ListaClientes =true;
                    CT.mensajeNoLista =false;
                    console.log(datos)
                    CT.ClientesQueMuestran = datos.clientes;
                   }
                })
            }else {
                 this.Cliente.splice(0); 
                for(let index = 0; index < this.clientes.length; index++){
                    if(this.clientes[index]['id'] == id){
                        this.Cliente.push(
                            {
                            id: this.clientes[index]['id'],
                            nombre: this.clientes[index]['nombre'],
                            dni: this.clientes[index]['dni'],
                            email: this.clientes[index]['email'],
                            telefono: this.clientes[index]['telefono'],
                            direccion: this.clientes[index]['direccion'],
                            fecha_registro: this.clientes[index]['fecha_registro'],
                            }
                           
                        );
                        this.HistoRial()   
                        this.cliente_s =this.Cliente[0].nombre
                        this.correo_mensaje= this.clientes[index]['email'] ;
                        this.w_numero =this.clientes[index]['telefono'];   
                        this.sms_numero = this.clientes[index]['telefono'];          
                    }
                }
                CT.HabilitarComponente('infoCliente');
            }
        },
        HistoRial(){
         
            CT.habilitar =true  
            var formdata = new FormData();
            formdata.append("historial_cliente", "ok");
            formdata.append("id_cliente",this.Cliente[0]['id'])
            axios.post("Controllers/php/ctrlHistorial.php", formdata).then((respuesta)=>{
                var dato =respuesta.data
               console.log(dato.datos)
                
                CT.HistorialCliente=dato.datos
            })         
        },
        ///////////EDITAR CLIENTE /////////////
        EditarCliente: function(dato)
        {
              if( dato=="editar"){
                this.loader = "block";
                formdata = new FormData();
                formdata.append("edita", "editar")
                formdata.append("dni", this.Cliente[0].dni)
                formdata.append("nombre", this.Cliente[0].nombre)
                formdata.append("email", this.Cliente[0].email)
                formdata.append("id", this.Cliente[0].id)
                formdata.append("telefono", this.Cliente[0].telefono)
                formdata.append("direccion",this.Cliente[0].direccion )
                formdata.append("fecha_registro",this.Cliente[0].fecha_registro )
                var vm =this;      
                axios.post("Controllers/php/ctrlClientes.php", formdata).then((respuesta)=>{
                    var  datos =  respuesta.data;
                    vm.loader = "none";
                        if(datos.respuesta == "ok"){
                            CT.ShowAlerts("cliente", "actualizar");
                            CT.MostrarClientes(false)
                            CT.HabilitarComponente("infoCliente")
                            CT.cliente_s =this.Cliente[0].nombre
                            /// formateamos: 
                            CT.dni = "";
                            CT.nombre = "";
                            CT.telefono = ""; 
                            CT.email = ""; 
                            CT.direccion = "";
                            CT.filtrar()
                        }else if(datos.respuesta == "00"){
                            CT.ShowAlerts("cliente", "DatosIncompletos");
                        }else if(datos.respuesta == "email_err"){
                            CT.ShowAlerts("cliente", "EmailInvalido");
                        }else if(datos.respuesta == "500"){
                            CT.ShowAlerts("cliente", "500");
                        }else if(datos.respuesta == "No"){
                            CT.ShowAlerts("cliente", "no");}
                    });
             }
        },
        ///////////ELIMINAR CLIENTE /////////////////
        MoverAlaPapelera: function()
        {
            var esto = this;
          if(this.Cliente.length >0){       
               var formdata = new FormData();
               formdata.append("eliminar", CT.Cliente[0].id)
               axios.post("Controllers/php/ctrlClientes.php", formdata).then((respuesta)=>
                { 
                    if(respuesta.data.respuesta ==true){
                        CT.ShowAlerts("cliente", "eliminar");
                        CT.MostrarClientes(false);
                        CT.HabilitarComponente("cero")
                        CT.AlertEliminarCliente=false;
                        if(respuesta.data.respuesta==true){
                           CT.Cliente =[];
                           CT.sms_numero=null;
                           CT.w_numero=null;
                           CT.correo_mensaje=null;
                           CT.HistorialCliente=[];
                           CT.cliente_s=
                           this.habilitar="";
                        }
                    }else{
                        CT.ShowAlerts("cliente", "eliminar_error");                    
                    }  
                })
          }else{
            CT.ShowAlerts("cliente", "No existe");
          }
        },
        EnviarMensajeSms(){
        },
        /////// HABILITAR O DESHABILITAR COMPONENTES /////
        HabilitarComponente: function(component) 
        {
            switch(component)
            {
              case "form": // habilitar formulario de registro de clientes
                CT.formRegistrarClientes = true;
                CT.ListaClientes = false;
                CT.infoCliente = false;
                CT.correo =false;
                CT.whatsapp =false;
                CT.MensajeSms=false;
              break;
              case "lista": //habilitar la lista de clientes
                CT.formRegistrarClientes = false;
                CT.ListaClientes = true;
                CT.MostrarClientes(false);
                CT.infoCliente = false;
                CT.correo =false;
                CT.whatsapp =false;
                CT.botonessms =false;
                CT.botonesclientes =true
                CT.MensajeSms=false;
              break;  
              case "cero":
                CT.ClienteHistorial=false;
                CT.correo =false;
                CT.editar_=false;
                CT.MensajeSms=false;
                CT.infoCliente=false;
                CT.whatsapp =false;
              break;
              case "historial":
                CT.ClienteHistorial=true;
                CT.correo =false;
                CT.editar_=false;
                CT.MensajeSms=false;
                CT.infoCliente=false;
                CT.whatsapp =false;
              break
              case"editar":
                CT.correo =false;
                CT.editar_=true;
                CT.ClienteHistorial=false;
                CT.MensajeSms=false;
                CT.infoCliente=false;
                CT.whatsapp =false;
              break;
              case "correo":
                 CT.correo =true;
                 CT.editar_=false;
                 CT.ClienteHistorial=false; 
                 CT.MensajeSms=false;
                 CT.infoCliente=false;
                 CT.whatsapp =false;
               break;
               case "infoCliente": //habilitar la info de los clientes
                 CT.editar_=false;
                 CT.ClienteHistorial=false;
                 CT.infoCliente = true;
                 CT.correo =false;
                 CT.botonessms=true;
                 CT.whatsapp =false;
                 CT.MensajeSms=false;
               break;
               case "whatsapp": //habilitar la info de los clientes
                 CT.whatsapp =true;
                 CT.editar_=false;
                 CT.ClienteHistorial=false;
                 CT.MensajeSms=false;
                 CT.infoCliente = false;
                 CT.correo =false;
               break;
               case "sms": //habilitar la info de los clientes
                 CT.MensajeSms=true;
                 CT.ClienteHistorial=false;
                 CT.whatsapp =false;
                 CT.editar_=false;
                 CT.infoCliente = false;
                 CT.correo =false;
               break;
            }
        },
        //////////MOSTRAR NOTIFICACIONES O ALERTS DE EXITO Y ADVERTENCIA O ERROR //////////////
        ShowAlerts: function(type, status){
            switch(type)
            {
                case "cliente": 
                    switch(status)
                    {
                        case "ok":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                              heading: '¡Ok!',
                              text: '<p>Se guardó correctamente el cliente.</p>',
                              position: 'bottom-right',
                              loaderBg:'#ffffff',
                              class: 'jq-toast-success',
                              hideAfter: 4500, 
                              stack: 6,
                              showHideTransition: 'fade'
                            });                                        
                        break;
                        case "actualizar":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Ok!',
                                text: '<p>Se guardó correctamente la actualización de los datos del cliente.</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-success',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                    break;
                    case "No existe":
                        $.toast().reset('all');
                        $("body").removeAttr('class');
                        $.toast({
                            heading: '¡Error!',
                            text: '<p>ERROR escoge a un cliente para poder mover ala papeleria</p>',
                            position: 'bottom-right',
                            loaderBg:'#ffffff',
                            class: 'jq-toast-success',
                            hideAfter: 4500, 
                            stack: 6,
                            showHideTransition: 'fade'
                        });                                        
                    break;   
             
                    
                    case "mensaje_enviado":
                        $.toast().reset('all');
                        $("body").removeAttr('class');
                        $.toast({
                           heading: '¡Ok!',
                            text: '<p>Se Envio Correctamente el correo</p>',
                            position: 'bottom-right',
                            loaderBg:'#ffffff',
                            class: 'jq-toast-success',
                            hideAfter: 4500, 
                            stack: 6,
                            showHideTransition: 'fade'
                         });                                        
                    break;
                    case "mensaje_enviado_sms":
                        $.toast().reset('all');
                        $("body").removeAttr('class');
                        $.toast({
                            heading: '¡Ok!',
                            text: '<p>Se Envio Correctamente el mensaje sms</p>',
                            position: 'bottom-right',
                            loaderBg:'#ffffff',
                            class: 'jq-toast-success',
                            hideAfter: 4500, 
                            stack: 6,
                            showHideTransition: 'fade'
                        });                                        
                    break;
                    case "eliminar":
                        $.toast().reset('all');
                        $("body").removeAttr('class');
                        $.toast({
                            heading: '¡Ok!',
                            text: '<p>Se Ha Eliminado Correctamente el Cliente</p>',
                            position: 'bottom-right',
                            loaderBg:'#ffffff',
                            class: 'jq-toast-success',
                            hideAfter: 4500, 
                            stack: 6,
                            showHideTransition: 'fade'
                        });                                        
                    break;
                    case "eliminar":
                        $.toast().reset('all');
                        $("body").removeAttr('class');
                        $.toast({
                            heading: '¡Error!',
                            text: '<p>Ocurrio un error al eliminar el Cliente</p>',
                            position: 'bottom-right',
                            loaderBg:'#ffffff',
                            class: 'jq-toast-success',
                            hideAfter: 4500, 
                            stack: 6,
                            showHideTransition: 'fade'
                        });                                        
                break;
                        case "DatosIncompletos":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Llene todos los campos de este formulario.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                        case "no":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>No tienes permiso para realizar este proceso</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });       
                        break;
                        case "DatosIncompletos_m":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>Llene todos los campos</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });       
                        break;
                        case "EmailInvalido":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>El correo electrónico no es válido.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                        case "EmailInvalido":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Fue imposible terminar el proceso, compruebe su conexión y vuelva a intentarlo o verifique que no haya digitado caracteres especiales.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
             
                        case "telefono_no_existe":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>ERROR este teléfono no existe</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                        break;   
                        case "w_no_existe":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>ERROR este teléfono no existe</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                        break;   
                        case "email_no_existe":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>ERROR no existe este email</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                        break;   
                        case "cancelar_e":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡cancelaste!',
                                    text: '<p>Cancelastes la eliminaión del cliente</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                    }
                break;
            }
        },
        cargarFechaActual(){
            axios.get("Controllers/php/ctrlClientes.php?FechaActual")
            .then(function(response){
                let datos = response.data;
              
                CT.fecha_inicio = datos.FechaActual;
                CT.fecha_final = datos.FechaActual;
               
                
            })
            axios.get("Controllers/php/ctrlClientes.php?permiso").then((respuesta)=>{
                console.log(respuesta.data.permiso)
                if(respuesta.data.permiso =="No"){
                    CT.ListaClientes =false;
                    CT.mensajeNoLista =true
                }else{
                    CT.ListaClientes =true;
                    CT.mensajeNoLista =false
                }
            })
        },
     
        
    },



    created() {
      this.cargarFechaActual();
      
      this.MostrarClientes(false);
    },
})
function cargar(id_cliente){
    CT.MostrarClientes(id_cliente)
}
