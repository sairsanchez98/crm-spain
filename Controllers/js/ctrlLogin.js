var sair = new Vue({
    el:"#log-in",
    data: {
        user: "",
        pass: "",
        showpass:false,
        alert: false,
        Loader: false,
        ButtonColorlogin: "btn btn-primary",
        textoBtnLogin : "Iniciar sesión"
    },


    methods: {
        Login:function(){
                sair.alert= false;
                sair.Loader = true;
                var formdata = new FormData();
                formdata.append("login","ok" );
                formdata.append("user",sair.user );
                formdata.append("pass",sair.pass );

                axios.post("Controllers/php/ctrlLogin.php", formdata)
                .then(function(response){
                    let datos = response.data;

                    switch(datos.respuesta){
                        case "user_null": // usuario vacío
                            sair.Loader = false;
                            sair.alert = "Digite su usuario de acceso";         
                        break;

                        case "pass_null": //contraseña vacia
                           sair.Loader = false; 
                           sair.alert = "Digite su contraseña de acceso";         
                        break;

                        case "datanull":
                             // todos los campos vacíos
                             sair.Loader = false;
                             sair.alert = "Digite su usuario y contraseña de acceso";     
                        break; 

                        case "ok":
                            sair.Loader = false;
                            sair.ButtonColorlogin = "btn btn-success";
                            sair.textoBtnLogin = "Acceso aprobado";
                            formdata = new FormData();
                            formdata.append("SessionUsrOk", datos.user_session);
                            axios.post("index.php", formdata)
                            .then(function(response){
                                window.location="./";
                            })
                        break;

                        case "denegado":
                            sair.alert = "Acceso denegado"; 
                            sair.Loader = false;

                        break;

                        
                    }

                    console.log(datos);
                })
           
        },

        showPass:function(){
            if(sair.showpass){ //si showpass (la variable) es TRUE ...se está mostrando la contraseña
                $("#contrasena").attr("type","password"); // la cambiamos a typo password para que se oculte
                sair.showpass = !sair.showpass;
            }else{ // si  si showpass (la variable) es FALSE no se está mostrando la contraseña
                $("#contrasena").attr("type","text"); // la cambiamos a typo text para que se muestre
                sair.showpass = !sair.showpass;
            }
        }

    }
})