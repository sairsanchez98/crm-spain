var MARKETING = new Vue({
    el: "#marketing",
    data:{
     //LOADERS/
     loaderLeft:'none',
    //mensajes de error para mensaje masivos
     mensaje_error:"",

  //filtro fechas
  fecha_inicio:"",
  fecha_final:"",
       ///DATOS ENVIAR CORREO///
       asunto_mensaje:"",
       mensaje_mensaje:"",
       id_:null,
       ///DATOS WHATSAPP//
        w_mensaje:"",
        ///DATOS SMS//
        sms_mensaje:"",
        //HISTORIAL MARKETING
        historial_marketing:[],
        AlertEliminarMarketing:false,

        // FILTRADOS//
         filtros:[],
         filtro_s:"",
         filtrar_mensaje:"todos",
         filtros_mensaje_:[],
          marketing_seleccionado:[],


          infoMarketing:false,


        lists: [],

        modal:false,
        loaderLeft: "none",
     
        ClientesEncontrados: [],

        ///// CLIENTES ALMACENADOS EN LA DB//////7
        clientes:[],
        Cliente:[],
       
        //// ESTADOS ////
        loader: "none",
        correo:null,
        mensaje:null,
        asunto:null,
        buscar:"",
        ClientesQueMuestran:[],
        mensajeNoLista:true,
        ///  COMPONENTES /////
        ListaClientes: true,
        ListaMarketing:true,
        whatsapp:false,
        MensajeSms:false,
        infoCliente: false,
        correo:false,
        filtrar_estado:"",
       
    },
  /*  computed: {
        searchUser: function () {
            return this.lists.filter((item) => item.dni.includes(this.dni));
        }
    },*/
    mounted() {
      // this.HistorialMarketing()
      this.cargarFechaActual()
    },
    methods: 
    
    {
        cargarFechaActual(){
            axios.get("Controllers/php/ctrlMarketing.php?FechaActual")
            .then(function(response){
                let datos = response.data;
              console.log(response.data)
                MARKETING.fecha_inicio = datos.FechaActual;
                MARKETING.fecha_final = datos.FechaActual;
               
            })
            axios.get("Controllers/php/ctrlClientes.php?permiso").then((respuesta)=>{
                console.log(respuesta.data.permiso)
                if(respuesta.data.permiso =="No"){
                    MARKETING.ListaMarketing =false;
                    MARKETING.mensajeNoLista =true
                }else{
                    MARKETING.ListaMarketing =true;
                    MARKETING.mensajeNoLista =false
                }
            })
        },
        filtrarEstado(dato){
            this.filtrar_estado =dato
            this.filtrar()
          },
         filtrar(){
                var formdata = new FormData();  
                console.log(formdata)
               formdata.append("filtro_marketing", "ok");
                this.loaderLeft ="block"
                formdata.append("fecha_inicial", this.fecha_inicio);
                formdata.append("fecha_final", this.fecha_final);
                formdata.append("filtro_estado", this.filtrar_estado)
                var vm = this;
              console.log(this.filtrar_estado)
                axios.post("Controllers/php/ctrlMarketing.php", formdata).then((respuesta)=>{
                    if ( $.fn.dataTable.isDataTable( '#datable_1' ) ) {
                        MARKETING.ClientesQueMuestran = $('#datable_1').DataTable();
                    }
                    else {
                        MARKETING.ClientesQueMuestran = $('#datable_1').DataTable( {
                            paging: false
                        } );
                    }
                    MARKETING.ClientesQueMuestran.clear().draw()
                     console.log(respuesta)
    
                    if(respuesta.data.registros =="No"){
                        vm.loaderLeft="none"
                      
                        vm.ListaMarketing=false;
                        MARKETING.ClientesQueMuestran.row.add([
                            `
                            <a href="javascript:void(0);"  class="media">
                       
                          </a>`
                           ]).draw(false)
                    
                        vm.mensajeNoLista =true;
                        MARKETING.ShowAlerts("marketing", "no");
                    }else{
                        vm.loaderLeft="none"
                        var datos  = respuesta.data;
                        MARKETING.servicios = datos.registros;
                        MARKETING.lists =datos.registros;
                        MARKETING.filtros= respuesta.data.filtros
                    
                        MARKETING.historial_marketing = respuesta.data.registros;
                     
                        datos.registros.forEach(user=>{
                            MARKETING.ClientesQueMuestran.row.add([
                             `
                             <a href="javascript:void(0);"  onclick="cargar(${user.id})" class="media">
                             <div class="media-body">
                              <div>
                                  <div class="email-head">Tipo: ${user.tipo} </div>
                                  <div class="email-subject"> Mensaje ${user.mensaje} </div>
                                  <div class="email-subject"> Fecha ${user.fecha_envio} </div>

                              </div>
                              <div>
                                  <div class="last-email-details">
                                  ${user.antiguedad}
                                  </div>
                                  </div>
                              </div>
                           </a>`
                            ]).draw(false)
                            })
                    }
                })
             
             
          
            
         },
        HistorialMarketing(){
        
            var formdata =  new FormData();
             this.loaderLeft ="block"
            formdata.append("buscar","ok")
            axios.post("Controllers/php/ctrlMarketing.php",formdata).then((respuesta)=>{
                this.loaderLeft="none"
                if(respuesta.data.registros=="No"){
                    MARKETING.ShowAlerts("marketing", "no");
                    MARKETING.ListaClientes =false;
                    MARKETING.mensajeNoLista =true;
                }else{
                    console.log(respuesta.data.filtros)
                      MARKETING.filtros= respuesta.data.filtros
                      MARKETING.historial_marketing = respuesta.data.registros;
                      MARKETING.ClientesQueMuestran= respuesta.data.registros
                   
                      MARKETING.ListaClientes =true;
                      MARKETING.mensajeNoLista =false;
                }
               
            })
        },
        MostrarMarketing(id){
            
            this.marketing_seleccionado.splice(0); // formateamos el array del especialista
            //traemos la info del especialista solicitado
            for(let index = 0; index < this.historial_marketing.length; index++){
                if(this.historial_marketing[index]['id'] == id){
                    this.marketing_seleccionado.push(
                        {
                          id: this.historial_marketing[index]['id'],
                          mensaje: this.historial_marketing[index]['mensaje'],
                          tipo: this.historial_marketing[index]['tipo'],
                          categoria: this.historial_marketing[index]['catgoria_mensaje'],
                          fechaenvio: this.historial_marketing[index]['fecha_envio'],
                          alcance: this.historial_marketing[index]['alcance']
    
                        }
                       
                    );

                }
               
            }
            
            this.id_ =this.marketing_seleccionado[0].id
            MARKETING.HabilitarComponente('infoMarketing');
        },
    
        EnviarMensajeAlSmsMasivo(action){
            switch(action)
            {
                case "sms": /// mostrar formulario de registro:
             MARKETING.HabilitarComponente("sms");
                    
                   
                break;
            

                case "enviar" : 
                  if(MARKETING.sms_mensaje != ""  )
                  {
                      
                    
                    var formdata = new FormData();
                    formdata.append("sms_masivo", "ok");
                    formdata.append("mensaje", MARKETING.sms_mensaje);
                    
                    formdata.append("filtro",MARKETING.filtro_s);
                    formdata.append("tipo", "sms");
                    
                    axios.post("Controllers/php/ctrlMarketing.php", formdata)
                    .then(function(response){
                        var  datos =  response.data;
                        console.log(datos);
                       if(datos == "ok"){
                        MARKETING.ShowAlerts("marketing", "enviado");
                      
                           
                        MARKETING.filtrar()
                            /// formateamos: 
                            MARKETING.w_mensaje = "";
                            MARKETING.w_numero= "";
                         
                        
                        }else if(datos == "500"){
                            MARKETING.ShowAlerts("marketing", "500");
                        }else if(datos == "No"){
                            MARKETING.ShowAlerts("marketing", "no");
                        }else{
                            MARKETING.ShowAlerts("marketing", "noexiste");
                            MARKETING.mensaje_error = datos;
                        }
                    });
                  }else{
                    MARKETING.ShowAlerts("marketing", "DatosIncompletos_m");
                  }
                break;
            }
        },
        EnviarMensajeAlWhastappMasivo:function(action){
            switch(action)
            {
                case "whatsapp": /// mostrar formulario de registro:
                    MARKETING.HabilitarComponente("whatsapp");
                    
                  
                break;
            

                case "enviar" : 
                  if(MARKETING.w_mensaje != ""  )
                  {
                   
                    var formdata = new FormData();
                    formdata.append("whatsapp_masivo", "ok");
               
                    formdata.append("mensaje", MARKETING.w_mensaje);
                    formdata.append("filtro", MARKETING.filtro_s);
                    formdata.append("tipo", "whatsapp")
                    
                    axios.post("Controllers/php/ctrlMarketing.php", formdata)
                    .then(function(response){
                        var  datos =  response.data;
                        console.log(datos);
                       if(datos == "ok"){
                        MARKETING.ShowAlerts("marketing", "enviado");
                        
                           
   
                        MARKETING.filtrar()
                            /// formateamos: 
                            MARKETING.w_mensaje = "";
                            MARKETING.w_numero= "";
                         
                        
                        }else if(datos == "00"){
                            MARKETING.ShowAlerts("markeing", "DatosIncompletos_m");
                        }else if(datos == "500"){
                            MARKETING.ShowAlerts("markeing", "500");
                        }else if(datos == "No"){
                            MARKETING.ShowAlerts("markeing", "no");
                        }else if(datos!="500"){
                            MARKETING.ShowAlerts("markeing", "noexiste");
                            MARKETING.mensaje_error = datos;
                        }
                    });
                  }else{
                    MARKETING.ShowAlerts("marketing", "DatosIncompletos_m");
                  }
                break;
            }
        },
        EnviarMensajeCorreoMasivo: function(action)
        {
            

            switch(action)
            {
                case "correo": /// mostrar formulario de registro:
                    MARKETING.HabilitarComponente("correo");
                    
                    
                break;

                case "enviar" : 
                  if(MARKETING.mensaje_mensaje!= "" &&  MARKETING.asunto_mensaje != "")
                  {
                   
                    var formdata = new FormData();
                    formdata.append("email_masivo", "ok");
                    formdata.append("asunto", MARKETING.asunto_mensaje);
                    formdata.append("mensaje", MARKETING.mensaje_mensaje);
                    formdata.append("filtro", MARKETING.filtro_s);
                    formdata.append("tipo", "email")
                    axios.post("Controllers/php/ctrlMarketing.php", formdata)
                    .then(function(response){
                        var  datos =  response.data;
                        console.log(datos);
                       if(datos== "ok"){
                        MARKETING.ShowAlerts("marketing", "enviado");
                        MARKETING.HistorialMarketing()
                           

                            /// formateamos: 
                           
                            MARKETING.asunto_mensaje = "";
                            MARKETING.mensaje_mensaje = ""; 
                        
                        }else if(datos == "00"){
                           
                        }else if(datos == "email_err"){
                            MARKETING.ShowAlerts("marketing", "EmailInvalido");
                        }else{
                            MARKETING.ShowAlerts("marketing", "noexiste");
                            MARKETING.mensaje_error = datos;
                        }
                        
                        
                    });
                  }else{
                    MARKETING.ShowAlerts("marketing", "DatosIncompletos_m");
                  }
                break;
            }
        },
        
   



        MoverAlaPapelera(){
    
            var esto = this;
          if(this.marketing_seleccionado.length >0){       
               var formdata = new FormData();
               formdata.append("eliminar", MARKETING.id_)
               axios.post("Controllers/php/ctrlMarketing.php", formdata).then((respuesta)=>
                { 
                    console.log(respuesta.data.respuesta)
                    if(respuesta.data.respuesta ==true){
                        
                        MARKETING.HabilitarComponente("cero")
                       
                        MARKETING.ShowAlerts("marketing", "eliminar");
                      
                        MARKETING.HistorialMarketing()
                
                    }else{
                        MARKETING.ShowAlerts("marketing", "eliminar_error");                    
                    }
                  //  
                })
          
          }else{
            MARKETING.ShowAlerts("cliente", "No existe");
          } 
        },
  

        /////// HABILITAR O DESHABILITAR COMPONENTES /////
        HabilitarComponente: function(component) 
        {
            switch(component)
            {
              
                case "lista": //habilitar la lista de clientes
               
                MARKETING.ListaMarketing = true;
                MARKETING.infoCliente = false;
                MARKETING.correo =false;
                MARKETING.whatsapp =false;
                break;
                case "correo":
                    MARKETING.correo =true;
               
                    MARKETING.infoMarketing=false;
                    MARKETING.whatsapp =false;
                    MARKETING.MensajeSms=false;
                  break;
                case "infoMarketing": //habilitar la info de los clientes
               
               
                MARKETING.infoMarketing = true;
                MARKETING.correo =false;
                MARKETING.whatsapp =false;
                MARKETING.MensajeSms=false;
                break;
                case "cero":
                    
                    MARKETING.correo =false;
                    MARKETING.editar_=false;
           
                    MARKETING.MensajeSms=false;
                    MARKETING.AlertEliminarMarketing=false;
                    MARKETING.infoMarketing = false;
                    MARKETING.whatsapp =false;
            break;
                case "whatsapp": //habilitar la info de los clientes
                MARKETING.whatsapp =true;
              
             
                MARKETING.infoMarketing = false;
                MARKETING.correo =false;
                MARKETING.MensajeSms=false;
                
                break;
                case "sms": //habilitar la info de los clientes
                MARKETING.infoMarketing =false;
                MARKETING.MensajeSms=true;
                MARKETING.whatsapp =false;
                MARKETING.correo =false;
                
                break;
            }
            
        },



        //////////MOSTRAR NOTIFICACIONES O ALERTS DE EXITO Y ADVERTENCIA O ERROR //////////////
        ShowAlerts: function(type, status){
            switch(type)
            {
                case "marketing": 
                    switch(status)
                    {
                        case "ok":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Ok!',
                                    text: '<p>Se guardó correctamente el cliente.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-success',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });                                        
                        break;
                        case "mensaje_enviado":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Ok!',
                                    text: '<p>Se Envio Correctamente el correo</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-success',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });                                        
                        break;
                        case "eliminar":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Ok!',
                                text: '<p>Se Ha Enviado a la Papelera este registro de marketing</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-success',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                    break;
                        case "DatosIncompletos":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Llene todos los campos de este formulario.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                        case "noexiste":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: `<p>'${MARKETING.mensaje_error}'</p>`,
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                }); 
                                MARKETING.mensaje_error ="";      
                        break;
                        case "no":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>No tienes permiso para realizar este proceso</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });       
                    break;
                        case "DatosIncompletos_m":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>Llene todos los campos</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });       
                    break;

                        case "EmailInvalido":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>El correo electrónico no es válido.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                        case "enviado":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Ok!',
                                text: '<p>Se Envio Correctamente el mensaje</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-success',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });  
                           break;
                        case "EmailInvalido":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Fue imposible terminar el proceso, compruebe su conexión y vuelva a intentarlo o verifique que no haya digitado caracteres especiales.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                               

                        case "cancelar_e":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡cancelaste!',
                                    text: '<p>Cancelastes la eliminaión del cliente</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                    }
                break;
            }
        }

        
    },



    
})
function cargar(id_marketing){
    MARKETING.MostrarMarketing(id_marketing)
}
