var sair = new Vue({
    el: "#servicios",
    data:{
        /// DATOS PARA UN NUEVO CLIENTE////
        dni: "",
        mensajeNoLista:true,
        nombre: "",
        telefono: "",
        servicio_ofrecido:"",
        lists: [],
        name: '',
        fecha_servicio:"",
        precio:"",
        
        modal:false,
        loaderLeft: "none",
        loader:"none",
        cliente_s:"",
        ClientesEncontrados: [],
        AlertEliminarCliente:false,
        ClienteHistorial:false,
        ///// CLIENTES ALMACENADOS EN LA DB//////7
        servicios:[],
        Cliente:null,
        Alert_telefono :'border-default' ,

        HistorialCliente:[],
        ///DATOS ENVIAR CORREO///
        correo_mensaje:"",
        asunto_mensaje:"",
        mensaje_mensaje:"",
       ///DATOS WHATSAPP//
        w_numero:"",
        w_mensaje:"",
        w_numero_servidor:"",
        //DATOS DE ENVIAR SMS
        sms_numero:0,
        sms_mensaje:"",

        //// ESTADOS ////
        loader: "none",
        correo:null,
        mensaje:null,
        asunto:null,
        buscar:"",
        ClientesQueMuestran:[],

        ///  COMPONENTES /////
        MensajeSms:false,
        ListaServicios: true,
        whatsapp:false,
        formRegistrarClientes : false,
        infoServicio: false,
        correo:false,
        habilitar:false,
        editar_:false,
        //control de botones
        botonesclientes:true,
        botonessms:false
       
    },
    computed: {
        searchUser: function () {
            return this.lists.filter((item) => item.dni.includes(this.dni));
        }
    },

    methods: 
    
    {

        //// hacer busqueda de los tratamientos:
        Buscar: function()
        {
            
              if(this.buscar == "")
              {
                this.MostrarServicios(false,false,false);
                //console.log("vacio");
              }else{
                //convertimos el texto que está ingresando 
                //el usuario a minusculas para que sea más fácil realizar el filtro
                const texto = sair.buscar.toLowerCase();
                this.ClientesEncontrados.splice(0);
                
                for(let _clientes_ of this.servicios )
                 {
                     let _cliente_ = _clientes_["id"]+
                     _clientes_["nombre"]+
                     _clientes_["email"];
                     let _cliente = _cliente_.toLowerCase();
                     //console.log(_infoTratamiento);
                    
                    if(_cliente.indexOf(this.buscar) !== -1){
                         this.ClientesEncontrados.push(_clientes_);
                         this.ClientesQueMuestran = this.ClientesEncontrados;
                         console.log(this.ClientesEncontrados)
                    }
                 }
              }
                 
        },

        //////////// REGISTRAR CLIENTE ///////
        RegistrarServicio: function(action)
        {
            

            switch(action)
            {
                case "form": /// mostrar formulario de registro:
                    sair.HabilitarComponente("form");
                break;

                case "Procesar" : ///// Control de datos y registro en la base de datos
                  if( sair.telefono != "" )
                  {
                    var formdata = new FormData();
                    formdata.append("registrar", "ok");
               
                    
      
                    formdata.append("telefono", sair.telefono);
                    formdata.append("servicio_ofrecido", sair.servicio_ofrecido);
                    formdata.append("fecha", sair.fecha_servicio);
                    formdata.append("precio", sair.precio)
                    axios.post("Controllers/php/ctrlServicios.php", formdata)
                    .then(function(response){
                        var  datos =  response.data;
                        console.log(response.data);
                        if(datos.respuesta == "ok"){
                            sair.ShowAlerts("cliente", "ok");
                            sair.ShowAlerts("cliente", "ok_");
                           
                            sair.HabilitarComponente("lista");
                            sair.MostrarServicios(false);

                            /// formateamos: 
                            sair.dni = "";
                            sair.nombre = "";
                            sair.telefono = ""; 
                            sair.email = ""; 
                            sair.direccion = "";
                        }else if(datos.respuesta == "00"){
                            sair.ShowAlerts("cliente", "DatosIncompletos");
                        }else if(datos.respuesta == "email_err"){
                            sair.ShowAlerts("cliente", "EmailInvalido");
                        }else if(datos.respuesta == "500"){
                            sair.ShowAlerts("cliente", "500");
                        }else if(datos.respuesta == "No"){
                            sair.ShowAlerts("cliente", "no");}
                    });
                  }else{
                    this.Alert_telefono= "border-danger"
                      sair.ShowAlerts("cliente", "DatosIncompletos");
                  }
               
                break;
            }
        },



        //////////MOSTRAR CLIENTES //////////////
        MostrarServicios: function(id, id_agente,id_)
        {
            if(!id &!id_agente){
                formdata = new FormData();
                formdata.append("listarTodo", "ok");
                var vm = this;
                this.loaderLeft ="block"
                axios.post("Controllers/php/ctrlServicios.php", formdata)
                .then(function(response){
                    //$("#datable_1").dataTable();
                    vm.loaderLeft="none"
                    console.log(response.data.servicios)
                   if(response.data.servicios=="No"){
                    sair.ShowAlerts("cliente", "no");
                    vm.ListaServicios =false;
                    vm.mensajeNoLista =true;
                   }else{
                    var datos  = response.data;
                    sair.servicios = datos.servicios;
                    sair.lists =datos.servicios;
                    vm.ListaServicios =true;
                    vm.mensajeNoLista =false;
                    sair.ClientesQueMuestran = datos.servicios;
                    
                   }
                   
                    
                    
                })


            }else {
                sair.HabilitarComponente('infoServicio');
               
                //traemos la info del especialista solicitado
                if(id>0){
                    var formdata = new FormData();
                    formdata.append("id_cliente", id);
                    formdata.append("id_agente", id_agente);
                    formdata.append("InfoServicio", "ok")
                    var datos =null;
                    axios.post("Controllers/php/ctrlServicios.php", formdata).then((res)=>{
                        datos =res.data
                    })


                    for(let index = 0; index < this.servicios.length; index++){
                        if(this.servicios[index]['id'] == id_){
                            alert("hola")
                            this.Cliente =
                                {
                                id: this.servicios[index]['id'],
                                asunto: this.servicios[index]['asunto'],
                                id_agente:this.servicios[index]['id_agente'],
                                id_cliente: this.servicios[index]['id_cliente'],
                                cantidad:this.servicios[index]["cantidad"],
                                nombre_agente:datos.agente.nombre,
                                nombre_cliente:datos.cliente.nombre,
                                telefono_cliente:dato.cliente.telefono,
                                fecha:this.servicios[index].fecha_creacion
                              
                                }
                               console.log(Cliente)
                            
                           /* this.HistoRial()   
                            this.cliente_s =this.Cliente[0].nombre
                            this.correo_mensaje= this.[index]['email'] ;
                            this.w_numero =this.clientes[index]['telefono'];   
                            this.sms_numero = this.clientes[index]['telefono']; */         
                        }
                    }
                }
            
                

               
            }
  
        },

        HistoRial(){
            sair.habilitar =true
            
            var formdata = new FormData();
            formdata.append("historial_cliente", "ok");
            formdata.append("id_cliente",this.Cliente[0]['id'])
            axios.post("Controllers/php/ctrlHistorial.php", formdata).then((respuesta)=>{
                console.log(respuesta.data)
                sair.HistorialCliente=respuesta.data
            })         
        },

        ///////////EDITAR CLIENTE /////////////
        EditarCliente: function(dato)
        {
              if( dato=="editar"){
                this.loader = "block";
                formdata = new FormData();
                formdata.append("edita", "editar")
                formdata.append("dni", this.Cliente[0].dni)
                formdata.append("nombre", this.Cliente[0].nombre)
                formdata.append("email", this.Cliente[0].email)
                formdata.append("id", this.Cliente[0].id)
                formdata.append("telefono", this.Cliente[0].telefono)
                formdata.append("direccion",this.Cliente[0].direccion )
                formdata.append("fecha_registro",this.Cliente[0].fecha_registro )
                var vm =this;
                
                axios.post("Controllers/php/ctrlClientes.php", formdata).then((respuesta)=>{
                    var  datos =  respuesta.data;
                    vm.loader = "none";
                        if(datos.respuesta == "ok"){
                            sair.ShowAlerts("cliente", "actualizar");
                            sair.MostrarServicios(false)
                            sair.HabilitarComponente("infoServicio")
                             sair.cliente_s =this.Cliente[0].nombre
                            /// formateamos: 
                            sair.dni = "";
                            sair.nombre = "";
                            sair.telefono = ""; 
                            sair.email = ""; 
                            sair.direccion = "";
                        }else if(datos.respuesta == "00"){
                            sair.ShowAlerts("cliente", "DatosIncompletos");
                        }else if(datos.respuesta == "email_err"){
                            sair.ShowAlerts("cliente", "EmailInvalido");
                        }else if(datos.respuesta == "500"){
                            sair.ShowAlerts("cliente", "500");
                        }else if(datos.respuesta == "No"){
                            sair.ShowAlerts("cliente", "no");}
                    });
                  
               
             }
        },




        ///////////ELIMINAR CLIENTE /////////////////
        MoverAlaPapelera: function()
        {
            var esto = this;
          if(this.Cliente.length >0){       
               var formdata = new FormData();
               formdata.append("eliminar", sair.Cliente[0].id)
               axios.post("Controllers/php/ctrlClientes.php", formdata).then((respuesta)=>
                { 
                    console.log(respuesta.data.respuesta)
                    if(respuesta.data.respuesta ==true){
                        sair.ShowAlerts("cliente", "eliminar");
                        sair.MostrarServicios(false);
                        sair.HabilitarComponente("cero")
                        sair.AlertEliminarCliente=false;
                        if(respuesta.data.respuesta==true){
                           sair.Cliente =[];
                           sair.sms_numero=null;
                           sair.w_numero=null;
                           sair.correo_mensaje=null;
                           sair.HistorialCliente=[];
                           sair.cliente_s=
                           this.habilitar="";
                        }
                    }else{
                        sair.ShowAlerts("cliente", "eliminar_error");                    
                    }
                  //  
                })
          
          }else{
            sair.ShowAlerts("cliente", "No existe");
          }
        },
        EnviarMensajeSms(){

        },




        /////// HABILITAR O DESHABILITAR COMPONENTES /////
        HabilitarComponente: function(component) 
        {
            switch(component)
            {
                case "form": // habilitar formulario de registro de clientes
                sair.formRegistrarClientes = true;
                sair.ListaServicios = false;
                sair.infoServicio = false;
                sair.correo =false;
                sair.whatsapp =false;
                sair.MensajeSms=false;
                break;

                case "lista": //habilitar la lista de clientes
                sair.formRegistrarClientes = false;
                sair.ListaServicios = true;
                sair.MostrarServicios(false);
                sair.infoServicio = false;
                sair.correo =false;
                sair.whatsapp =false;
                sair.botonessms =false;
                sair.botonesclientes =true
                sair.MensajeSms=false;
                break;
                
                case "cero":
                        sair.ClienteHistorial=false;
                        sair.correo =false;
                        sair.editar_=false;
                        //sair.ListaClientes =false;
                        sair.MensajeSms=false;
                        sair.infoServicio=false;
                        sair.whatsapp =false;
                break;
                case "historial":
                sair.ClienteHistorial=true;
                sair.correo =false;
                sair.editar_=false;
                //sair.ListaClientes =false;
                sair.MensajeSms=false;
                sair.infoServicio=false;
                sair.whatsapp =false;
                break
                case"editar":
                sair.correo =false;
                sair.editar_=true;
                sair.ClienteHistorial=false;

                //sair.ListaClientes =false;
                sair.MensajeSms=false;
                sair.infoServicio=false;
                sair.whatsapp =false;
                break;
                
                case "infoServicio": //habilitar la info de los clientes
                sair.editar_=false;
                sair.ClienteHistorial=false;
                sair.infoServicio = true;
                sair.correo =false;
                sair.botonessms=true;    
                sair.whatsapp =false;
                sair.MensajeSms=false;
                break;
            }
            
        },



        //////////MOSTRAR NOTIFICACIONES O ALERTS DE EXITO Y ADVERTENCIA O ERROR //////////////
        ShowAlerts: function(type, status){
            switch(type)
            {
                case "cliente": 
                    switch(status)
                    {
                        case "ok":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Ok!',
                                    text: '<p>Se guardó correctamente el cliente.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-success',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });                                        
                        break;
                        case "actualizar":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Ok!',
                                text: '<p>Se guardó correctamente la actualización de los datos del cliente.</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-success',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                    break;
                    case "No existe":
                        $.toast().reset('all');
                        $("body").removeAttr('class');
                        $.toast({
                            heading: '¡Error!',
                            text: '<p>ERROR escoge a un cliente para poder mover ala papeleria</p>',
                            position: 'bottom-right',
                            loaderBg:'#ffffff',
                            class: 'jq-toast-success',
                            hideAfter: 4500, 
                            stack: 6,
                            showHideTransition: 'fade'
                        });                                        
                break;
                    
                        case "mensaje_enviado":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Ok!',
                                    text: '<p>Se Envio Correctamente el correo</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-success',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });                                        
                        break;
                        case "mensaje_enviado_sms":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Ok!',
                                text: '<p>Se Envio Correctamente el mensaje sms</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-success',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                    break;
                        case "eliminar":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Ok!',
                                text: '<p>Se Ha Eliminado Correctamente el Cliente</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-success',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                    break;
                    case "eliminar":
                        $.toast().reset('all');
                        $("body").removeAttr('class');
                        $.toast({
                            heading: '¡Error!',
                            text: '<p>Ocurrio un error al eliminar el Cliente</p>',
                            position: 'bottom-right',
                            loaderBg:'#ffffff',
                            class: 'jq-toast-success',
                            hideAfter: 4500, 
                            stack: 6,
                            showHideTransition: 'fade'
                        });                                        
                break;
                        case "DatosIncompletos":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Llene todos los campos de este formulario.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                        case "no":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>No tienes permiso para realizar este proceso</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });       
                    break;
                        case "DatosIncompletos_m":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>Llene todos los campos</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });       
                    break;

                        case "EmailInvalido":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>El correo electrónico no es válido.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                       

                        case "EmailInvalido":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Fue imposible terminar el proceso, compruebe su conexión y vuelva a intentarlo o verifique que no haya digitado caracteres especiales.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                               

                        case "cancelar_e":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡cancelaste!',
                                    text: '<p>Cancelastes la eliminaión del cliente</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                    }
                break;
            }
        }

        
    },



    mounted() {
        this.MostrarServicios(false, false,false); 
       
    },
})