var LEADS = new Vue({
    el: "#leads",
    data:{
        /// DATOS PARA UN NUEVO CLIENTE////
        
        dni: "",
        mensajeNoLista:false,
        nombre: "",
        telefono: "",
        email: "",
        direccion: "",
        lists: [],
        tabla:false,
        name: '',
        filtros:[{nombre:"Numero erroneo"},{nombre:"No Intersado"},{nombre:"Citado"},{nombre:"Citado: Recordatorio 1"},{nombre:"Citado Recordatorio 2"},{nombre:"Citado: Sin Comfirmar"},{nombre:"Confirmado"},{nombre:"Fallida"},{nombre:"En futuro"},{nombre:"Pendiente: No Contesta"},{nombre:"Petición de Contacto"}],
        //filtro fechas
        fecha_inicio:"",
        fecha_final:"",

        loaderLeft: "none",
        loader:"none",
        cliente_s:"",
        ClientesEncontrados: [],
        AlertEliminarCliente:false,
        ClienteHistorial:false,
        ///// CLIENTES ALMACENADOS EN LA DB//////7
        servicios:[],
        Cliente:[],
        Alert_telefono :'border-default' ,

        HistorialCliente:[],
        ///DATOS ENVIAR CORREO///
        correo_mensaje:"",
        asunto_mensaje:"",
        mensaje_mensaje:"",
       ///DATOS WHATSAPP//
        w_numero:"",
        w_mensaje:"",
        w_numero_servidor:"",
        //DATOS DE ENVIAR SMS
        sms_numero:0,
        sms_mensaje:"",
        //// ESTADOS ////
        loader: "none",
        correo:null,
        mensaje:null,
        asunto:null,
        buscar:"",
        ClientesQueMuestran:[],
        ///  COMPONENTES /////
        MensajeSms:false,
        ListaLeads: true,
        whatsapp:false,
        formRegistrarClientes : false,
        infoCliente: false,
        correo:false,
        habilitar:false,
        editar_:true,
        //control de botones
        botonesclientes:true,
        botonessms:false,
        leads:null,
        filtrar_estado:"",
        filtros_estdo_:null 
    },
    computed: {
        searchUser: function () {
            return this.lists.filter((item) => item.dni.includes(this.dni));
        }
    },
    methods: 
    {
        filtrar_leads(){
            this.ClientesQueMuestran =[]
            var arreglo=[];
            if(this.filtrar_estado!="todos"){
                for(let i=0; i< this.leads.length;i++){
                  if(this.filtrar_estado ==this.leads[i].estado){
                    console.log(1)
                    arreglo.push(this.leads[i])
                  }
                }
                this.filtros_estado_ =arreglo;
                this.ClientesQueMuestran =arreglo;

            }else{
                this.MostrarLeads(false)
            }
        },     
        //////////MOSTRAR CLIENTES //////////////

      ///////FILTRO FECHAS//
      filtrarEstado(dato){
        this.filtrar_estado =dato
        this.filtrar()
      },
     filtrar(){
            var formdata = new FormData();  
            console.log(formdata)
           formdata.append("filtro_leads", "ok");
            this.loaderLeft ="block"
            formdata.append("fecha_inicial", this.fecha_inicio);
            formdata.append("fecha_final", this.fecha_final);
            formdata.append("filtro_estado", this.filtrar_estado)
            var vm = this;
          console.log(this.filtrar_estado)
            axios.post("Controllers/php/ctrlLeads.php", formdata).then((respuesta)=>{
                if ( $.fn.dataTable.isDataTable( '#datable_1' ) ) {
                    LEADS.ClientesQueMuestran = $('#datable_1').DataTable();
                }
                else {
                    LEADS.ClientesQueMuestran = $('#datable_1').DataTable( {
                        paging: false
                    } );
                }
                LEADS.ClientesQueMuestran.clear().draw()
                 console.log(respuesta)

                if(respuesta.data.leads =="No"){
                    vm.loaderLeft="none"
                  
                    vm.ListaLeads=false;
                    LEADS.ClientesQueMuestran.row.add([
                        `
                        <a href="javascript:void(0);"  class="media">
                   
                      </a>`
                       ]).draw(false)
                
                    vm.mensajeNoLista =true;
                    LEADS.ShowAlerts("leads", "no");
                }else{
                    vm.loaderLeft="none"
                    var datos  = respuesta.data;
                    LEADS.servicios = datos.leads;
                    LEADS.lists =datos.leads;
                  
                    vm.ListaLeads =true;
                    vm.mensajeNoLista =false;
                    vm.leads =datos.leads; 
                    datos.leads.forEach(user=>{
                        LEADS.ClientesQueMuestran.row.add([
                         `
                         <a href="javascript:void(0);"  onclick="cargar(${user.id_leads})" class="media">
                         <div class="media-body">
                          <div>
                              <div class="email-head">Fecha: ${user.fecha} </div>
                              <div class="email-subject"> Estado ${user.estado} </div>
                          </div>
                          <div>
                              <div class="last-email-details">
                              </div>
                              </div>
                          </div>
                       </a>`
                        ]).draw(false)
                        })
                }
            })
         
         
      
        
     },

      //////FILTRO FECHAS//



        MostrarLeads: function(id)
        {            
        
            if(!id){
                this.tabla =false
                formdata = new FormData();
                formdata.append("ListarTodo", "ok");
                formdata.append("filtro", this.filtrar_estado);
                var vm = this;
                console.log(this.filtrar_estado)
                this.loaderLeft ="block"
                axios.post("Controllers/php/ctrlLeads.php", formdata)
                .then(function(response){
                
                    vm.loaderLeft="none"
                    //SI RETORNA NO ES PORQUE NO TIENE PERMISO PARA ESE PROCESO O TAREA
                   if(response.data.leads=="No"){
                    LEADS.ShowAlerts("leads", "no");
                    vm.ListaLeads=false;
                    vm.mensajeNoLista =true;
                   }else{    
                    var datos  = response.data;
                    LEADS.servicios = datos.leads;
                    LEADS.lists =datos.leads;
                   
                    vm.ListaLeads =true;
                    vm.mensajeNoLista =false;
                    vm.leads =datos.leads;
                    LEADS.ClientesQueMuestran = datos.leads;
                   }
                })
            }else {
                 this.Cliente.splice(0); // formateamos el array del especialista
                //traemos la info del especialista solicitado
                for(let index = 0; index < this.servicios.length; index++){
                    if(this.servicios[index]['id_leads'] == id){
                        this.Cliente.push(
                            {
                            id: this.servicios[index]['id_leads'],
                            telefono_cl: this.servicios[index]['cliente'].telefono,
                            email_cl:this.servicios[index]["cliente"].email,
                            nombre: this.servicios[index]["cliente"].nombre,
                            estado:this.servicios[index]['estado'],      
                            }
                        );
                    }
                }
                console.log(this.Cliente)
                this.editar_ =true
            }
  
        },

        HistoRial(){
            LEADS.habilitar =true
            
            var formdata = new FormData();
            formdata.append("historial_cliente", "ok");
            formdata.append("id_cliente",this.Cliente[0]['id'])
            axios.post("Controllers/php/ctrlHistorial.php", formdata).then((respuesta)=>{
                console.log(respuesta.data)
                LEADS.HistorialCliente=respuesta.data
            })         
        },

        ///////////EDITAR CLIENTE /////////////
        EditarEstadoLeads: function(dato)
        {
              if( dato=="editar"){
       
                formdata = new FormData();
                formdata.append("edita", "editar")
                formdata.append("id", this.Cliente[0].id)
                formdata.append("estado", this.Cliente[0].estado)
    
                var vm =this;
                
                axios.post("Controllers/php/ctrlLeads.php", formdata).then((res)=>{
                
                 
                        if(res.data.respuesta==="ok"){
                           LEADS.editar_ =false;
                           LEADS.ShowAlerts("leads", "actualizar");
                           LEADS.filtrar(this.filtrar_estado)
                           LEADS.HabilitarComponente("infoCliente")
               
                        }else if(datos.respuesta == "00"){
                           LEADS.ShowAlerts("leads", "DatosIncompletos");
                        }else if(datos.respuesta == "email_err"){
                           LEADS.ShowAlerts("leads", "EmailInvalido");
                        }else if(datos.respuesta == "500"){
                           LEADS.ShowAlerts("leads", "500");
                        }else if(datos.respuesta == "No"){
                           LEADS.ShowAlerts("leads", "no");}
                    });
                  
               
             }
        },




        ///////////ELIMINAR CLIENTE /////////////////
        MoverAlaPapelera: function()
        {
            var esto = this;
          if(this.Cliente.length >0){       
               var formdata = new FormData();
               formdata.append("eliminar", LEADS.Cliente[0].id)
               axios.post("Controllers/php/ctrlClientes.php", formdata).then((respuesta)=>
                { 
                    console.log(respuesta.data.respuesta)
                    if(respuesta.data.respuesta ==true){
                        LEADS.ShowAlerts("leads", "eliminar");
                        LEADS.MostrarLeads(false);
                        LEADS.HabilitarComponente("cero")
                        LEADS.AlertEliminarCliente=false;
                        if(respuesta.data.respuesta==true){
                           LEADS.Cliente =[];
                           LEADS.sms_numero=null;
                           LEADS.w_numero=null;
                           LEADS.correo_mensaje=null;
                           LEADS.HistorialCliente=[];
                           LEADS.cliente_s=
                           this.habilitar="";
                        }
                    }else{
                        LEADS.ShowAlerts("leads", "eliminar_error");                    
                    }
                  //  
                })
          
          }else{
            LEADS.ShowAlerts("leads", "No existe");
          }
        },
        cargarFechaActual(){
            axios.get("Controllers/php/ctrlLeads.php?FechaActual")
            .then(function(response){
                let datos = response.data;
              
                LEADS.fecha_inicio = datos.FechaActual;
                LEADS.fecha_final = datos.FechaActual;
               
            })
            axios.get("Controllers/php/ctrlClientes.php?permiso").then((respuesta)=>{
                console.log(respuesta.data.permiso)
                if(respuesta.data.permiso =="No"){
                    LEADS.ListaClientes =false;
                    LEADS.mensajeNoLista =true
                }else{
                    LEADS.ListaClientes =true;
                    LEADS.mensajeNoLista =false
                }
            })
        },



        /////// HABILITAR O DESHABILITAR COMPONENTES /////
        HabilitarComponente: function(component) 
        {
            switch(component)
            {
                case "form": // habilitar formulario de registro de clientes
                LEADS.formRegistrarClientes = true;
                LEADS.ListaLeads = false;
                LEADS.infoCliente = false;
                LEADS.correo =false;
                LEADS.whatsapp =false;
                LEADS.MensajeSms=false;
                break;
                case "lista": //habilitar la lista de clientes
                LEADS.formRegistrarClientes = false;
                LEADS.ListaLeads = true;
                LEADS.MostrarLeads(false);
                LEADS.infoCliente = false;
                LEADS.correo =false;
                LEADS.whatsapp =false;
                LEADS.botonessms =false;
                LEADS.botonesclientes =true
                LEADS.MensajeSms=false;
                break;
                case"editar":
                LEADS.correo =false;
                LEADS.editar_=true;
                LEADS.ClienteHistorial=false;
                LEADS.MensajeSms=false;
                LEADS.infoCliente=false;
                LEADS.whatsapp =false;
                break;
                

            }
            
        },



        //////////MOSTRAR NOTIFICACIONES O ALERTS DE EXITO Y ADVERTENCIA O ERROR //////////////
        ShowAlerts: function(type, status){
            switch(type)
            {
                case "leads": 
                    switch(status)
                    {
                        case "ok":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Ok!',
                                    text: '<p>Se guardó correctamente el cliente.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-success',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });                                        
                        break;
                        case "actualizar":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Ok!',
                                text: '<p>Se guardó correctamente la actualización del estado del leads</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-success',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                    break;
                    case "No existe":
                        $.toast().reset('all');
                        $("body").removeAttr('class');
                        $.toast({
                            heading: '¡Error!',
                            text: '<p>ERROR escoge a un cliente para poder mover ala papeleria</p>',
                            position: 'bottom-right',
                            loaderBg:'#ffffff',
                            class: 'jq-toast-success',
                            hideAfter: 4500, 
                            stack: 6,
                            showHideTransition: 'fade'
                        });                                        
                break;
                    
                        case "mensaje_enviado":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Ok!',
                                    text: '<p>Se Envio Correctamente el correo</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-success',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });                                        
                        break;
                        case "mensaje_enviado_sms":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Ok!',
                                text: '<p>Se Envio Correctamente el mensaje sms</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-success',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                    break;
                        case "eliminar":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Ok!',
                                text: '<p>Se Ha Eliminado Correctamente el Cliente</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-success',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });                                        
                    break;
                    case "eliminar":
                        $.toast().reset('all');
                        $("body").removeAttr('class');
                        $.toast({
                            heading: '¡Error!',
                            text: '<p>Ocurrio un error al eliminar el Cliente</p>',
                            position: 'bottom-right',
                            loaderBg:'#ffffff',
                            class: 'jq-toast-success',
                            hideAfter: 4500, 
                            stack: 6,
                            showHideTransition: 'fade'
                        });                                        
                break;
                        case "DatosIncompletos":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Llene todos los campos de este formulario.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                        case "no":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>No tienes permiso para realizar este proceso</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });       
                    break;
                        case "DatosIncompletos_m":
                            $.toast().reset('all');
                            $("body").removeAttr('class');
                            $.toast({
                                heading: '¡Error!',
                                text: '<p>Llene todos los campos</p>',
                                position: 'bottom-right',
                                loaderBg:'#ffffff',
                                class: 'jq-toast-danger',
                                hideAfter: 4500, 
                                stack: 6,
                                showHideTransition: 'fade'
                            });       
                    break;

                        case "EmailInvalido":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>El correo electrónico no es válido.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                       

                        case "EmailInvalido":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Fue imposible terminar el proceso, compruebe su conexión y vuelva a intentarlo o verifique que no haya digitado caracteres especiales.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                               

                        case "cancelar_e":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡cancelaste!',
                                    text: '<p>Cancelastes la eliminaión del cliente</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                        break;
                    }
                break;
            }
        },
       

        
    },



    mounted() {

    this.cargarFechaActual()
   
    },
})

function cargar(id_leads){
    LEADS.MostrarLeads(id_leads)
}

