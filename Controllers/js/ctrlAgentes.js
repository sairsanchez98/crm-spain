var sair = new Vue({
    el: "#agentes",
    data: {
        ///// datos de un agente /////
        usuario: "",
        nombre: "",
        contrasena: "",


        /// AGENTES EN LA DB:
        agentes: [],
        agente: [], // info de un agente en especifico .. solo cuando es requerido
        permisosAgentes: [],        

        /// COMPONENTES : 
        ListaAgentes: true,
        formRegistrarAgentes: false,
        infoAgente: false,

        /// / clases y estados :
        loaderLeft: "none",

        buscar: ""
        

    }, methods: {
         
        
         //////////// REGISTRAR CLIENTE ///////
         RegistrarAgente: function(action)
         {
             
 
             switch(action)
             {
                 case "form": /// mostrar formulario de registro:
                     sair.HabilitarComponente("form");
                 break;
 
                 case "Procesar" : ///// Control de datos y registro en la base de datos
                    var formdata = new FormData();
                    formdata.append("registrar",true);
                    formdata.append("usuario", sair.usuario);
                    formdata.append("nombre", sair.nombre);
                    formdata.append("contrasena", sair.contrasena);
                    axios.post("Controllers/php/ctrlAgentes.php", formdata)
                    .then(function(response){
                        var datos = response.data;
                        if(datos.respuesta == "ok"){
                            sair.MostrarAgentes(false); 
                            sair.HabilitarComponente("lista");
                            sair.ShowAlerts("registrar_agente", "ok");
                        }else{
                            sair.ShowAlerts("registrar_agente", "campos_vacios");
                        }
                    })
                 break; 
             }
         },


         Buscar: function (){

         },

         ////MOSTRAR AGENTES
         MostrarAgentes: function(id)
         {
             this.loaderLeft = "block";
             if(!id){ //mostrar todos
                 formdata = new FormData();
                 formdata.append("listarTodo", "ok")
                 axios.post("Controllers/php/ctrlAgentes.php", formdata)
                 .then(function(response){
                     //$("#datable_1").dataTable();
                     var datos  = response.data;
                     sair.agentes = datos.agentes;
                     sair.loaderLeft ="none";
                     //console.log(sair.agentes);
                 })
 
             }else{ //mostrar uno en particular
                sair.HabilitarComponente("infoAgente");
                sair.agente.splice(0);// formateamos el array del agente
                for(let index = 0; index < sair.agentes.length; index++){
                    if(sair.agentes[index]['id'] == id){
                        sair.agente.push(
                            {
                                id: sair.agentes[index]['id'],
                                nombre: sair.agentes[index]['nombre'],
                                pass: sair.agentes[index]['pass'],
                                usuario: sair.agentes[index]['usuario']
                            }
                        );
                
                        sair.verServiciosDelAgente(sair.agentes[index]['id']);
                    }
                }
                this.loaderLeft = "none";
                //console.log(sair.agente);
             }
         },


        
        /////// HABILITAR O DESHABILITAR COMPONENTES /////
         HabilitarComponente: function(component) 
         {
             switch(component)
             {
                 case "form": // habilitar formulario de registro de agentes
                 sair.formRegistrarAgentes = true;
                 sair.ListaAgentes = false;
                 sair.infoAgente = false;
                 break;
 
                 case "lista": //habilitar la lista de agentes
                 sair.ListaAgentes = true;
                 sair.formRegistrarAgentes = false;
                 sair.infoAgente = false;
                 break;

                 case "infoAgente": //habilitar la información de un agente en particular
                 //sair.ListaAgentes = false;
                 sair.formRegistrarAgentes = false;
                 sair.infoAgente = true;
                 break;
             }
             
         },


         /////MOSTRAR ALERTAS ///
         ShowAlerts: function(type, status){
             switch(type){
                 case "registrar_agente":
                     switch(status){
                         case "campos_vacios":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Llene todos los campos de este formulario.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                            break;
                        
                        case "ok":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Ok!',
                                    text: '<p>Se guardó correctamente el agente.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-success',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });          
                            break;
                     }
                 break;
             }
         },


         verServiciosDelAgente: function(id){
            formdata = new FormData();
            formdata.append("id", id);
            formdata.append("traerServicios", id);
            axios.post("Controllers/php/ctrlAgentes.php",formdata)
            .then(function(response){
                datos = response.data;
                
                console.log(datos.PermisosAgente);
                //console.log(datos.permisosDesactivados);
                 sair.permisosAgentes = datos.PermisosAgente;
            })
         },

         ////Actualizar - Guardar cambios //////
         GuardarCambios: function(id_agente){
             
                // var formdata = new FormData();
                // formdata.append("GuardarCambios" , "permisos");
                // formdata.append("permisosAgente" , sair.permisosAgentes);
                // formdata.append("id_agente" , id_agente);

                // axios.post("Controllers/php/ctrlAgentes.php", formdata)
                // .then(function(response){
                //     var datos = response.data;
                //     console.log(datos.respuesta);
                // })

                sair.permisosAgentes.forEach(function(key, value){
                     var formdata = new FormData();
                     formdata.append("GuardarCambios" , "permisos");
                     formdata.append("id_agente" , id_agente);
                     formdata.append("permisoAgente" , key.id);
                     formdata.append("estado" , key.activado);

                     axios.post("Controllers/php/ctrlAgentes.php", formdata)
                     .then(function(response){
                      var datos = response.data;
                        console.log(datos.respuesta);
                     })
                     console.log(key.activado);
                })
         }



    },

    mounted() {
        this.MostrarAgentes(false); 
    },
})