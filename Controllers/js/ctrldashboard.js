var dash =new Vue({
    el:"#dashboard",
    data:{
        datos:null,
		total:0,
		nombre_agente:"",
		tipo_agente:"",
		citas_tomadas_hoy:0,
		citas_para_hoy:0,
		agentes_citas:[],
		titulo_noticia:"",
		mensaje_noticia:"",
        citas_vacias:false
    },
    methods: {
        Cantidad(){
           
             var formdata = new FormData();
             formdata.append("BuscarCantidad", "ok");
            
             axios.post("Controllers/php/ctrldashboard.php", formdata).then((respuesta)=>{
                 console.log(respuesta)
               
                 if(respuesta.data.consultacorrecta=="ok"){
                  //7 datos principales  [120, 200, 150, 80, 70, 110, 130];
                 
                    dash.datos = respuesta.data.datos_cantidad
                    dash.total =respuesta.data.total;
                 }else{
                    
                    dash.datos= [0, 0, 0, 0, 0, 0, 0];
                 }
             })
             return dash.datos
          
          
        },
        titulos(){
           return ['leads', 'citas', 'Whatsapp', 'Sms', 'Email', 'Servicios', 'Clientes']
		},
		 DatosAgenteCitas(){
			var formdata = new FormData();
			formdata.append("datos_agentes_citas", "ok");
			axios.post("Controllers/php/ctrlAgentes.php", formdata).then((respuesta)=>{
	            if(respuesta.data.agentes_citas.length>0){
					dash.agentes_citas = respuesta.data.agentes_citas
					dash.citas_vacias =false;
				}else{
					dash.citas_vacias =true;
				}
			})
		 },
		 CrearNoticia(){
			if(this.mensaje_noticia!="" & this.titulo_noticia!=""){
				var formdata = new FormData();
			formdata.append("crear_noticia", "ok");
			formdata.append("titulo_noticia",  this.titulo_noticia);
			formdata.append("mensaje_noticia", this.mensaje_noticia)
			axios.post("Controllers/php/ctrldashboard.php", formdata).then((respuesta)=>{
	          console.log(respuesta)
			})
			}else{
				dash.ShowAlerts("datos_noticias", "datos_incompletos");
			}
			
		 },
		 DatosAgente(){
			var formdata = new FormData();
			formdata.append("DatosAgente", "ok");
			axios.post("Controllers/php/ctrlAgentes.php", formdata).then((respuesta)=>{
				console.log(respuesta)
			   if(respuesta.data.resultado =="ok"){
				dash.nombre_agente =respuesta.data.agente.nombre;
				dash.tipo_agente = respuesta.data.agente.tipo_agente;
			
			   }else if(respuesta.data.resultado=="error"){
				dash.ShowAlerts("datos_agente", "error");
			   }else{
				dash.ShowAlerts("datos_agente", "no");
			   }
			})
		 },
		 CitasParaHoy(){
			var formdata = new FormData();
			formdata.append("citasHoy_Tomadas", "ok");
			axios.post("Controllers/php/ctrlCitas.php", formdata).then((respuesta)=>{
              if(respuesta.data.citas_tomadas_resultado_hoy =="ok" ||respuesta.data.citas_resultado_hoy=="ok"){
				  dash.citas_para_hoy =respuesta.data.citas_para_hoy
				  dash.citas_tomadas_hoy = respuesta.data.citas_tomadas_hoy;
			  }else{

			  }
			})
		 },
		  
			
		 

         /////MOSTRAR ALERTAS ///
         ShowAlerts: function(type, status){
			switch(type){
				case "datos_agente":
					switch(status){
						case "error":
							   $.toast().reset('all');
							   $("body").removeAttr('class');
							   $.toast({
								   heading: '¡Error!',
								   text: '<p>Error al conectar a la base de datos</p>',
								   position: 'bottom-right',
								   loaderBg:'#ffffff',
								   class: 'jq-toast-danger',
								   hideAfter: 4500, 
								   stack: 6,
								   showHideTransition: 'fade'
							   });       
						   break;
						   case "no":
							$.toast().reset('all');
							$("body").removeAttr('class');
							$.toast({
								heading: '¡Error!',
								text: '<p>Error este usuario no esta en la base de datos</p>',
								position: 'bottom-right',
								loaderBg:'#ffffff',
								class: 'jq-toast-danger',
								hideAfter: 4500, 
								stack: 6,
								showHideTransition: 'fade'
							});       
						break;
				
					}
				break;
			}
		},

    },
     created() {
	 this.DatosAgenteCitas();
	 this.DatosAgente();
	 this.CitasParaHoy()
 /*Dashboard3 Init*/
 
"use strict"; 

/*****E-Charts function start*****/
var echartsConfig = function() { 
	
	if( $('#e_chart_11').length > 0 ){
		var eChart_11 = echarts.init(document.getElementById('e_chart_11'));
		var option10 = {
			color: ['#7a5449'],
			tooltip: {
				show: true,
				trigger: 'axis',
				backgroundColor: '#fff',
				borderRadius:6,
				padding:6,
				axisPointer:{
					lineStyle:{
						width:0,
					}
				},
				textStyle: {
					color: '#324148',
					fontFamily: '"Roboto", sans-serif',
					fontSize: 12
				}	
			},
			
			xAxis: [{
				type: 'category',
				data: dash.titulos(),
				axisLine: {
					show:false
				},
				axisTick: {
					show:false
				},
				axisLabel: {
					textStyle: {
						color: '#5e7d8a'
					}
				}
			}],
			yAxis: {
				type: 'value',
				axisLine: {
					show:false
				},
				axisTick: {
					show:false
				},
				axisLabel: {
					textStyle: {
						color: '#5e7d8a'
					}
				},
				splitLine: {
					lineStyle: {
						color: '#eaecec',
					}
				}
			},
			grid: {
				top: '3%',
				left: '3%',
				right: '3%',
				bottom: '3%',
				containLabel: true
			},
			series: [{
				data: dash.Cantidad(),
				type: 'bar',
				barMaxWidth: 30,
				itemStyle: {
					normal: {
						barBorderRadius: [6, 6, 0, 0] ,
					}
				},
				label: {
					normal: {
						show: true,
						position: 'inside'
					}
				},
			}]
		};
		eChart_11.setOption(option10);
		eChart_11.resize();
	}
}
/*****E-Charts function end*****/

/*****Resize function start*****/
var echartResize;
$(window).on("resize", function () {
	/*E-Chart Resize*/
	clearTimeout(echartResize);
	echartResize = setTimeout(echartsConfig, 200);
}).resize(); 
/*****Resize function end*****/

/*****Function Call start*****/
echartsConfig();
/*****Function Call end*****/
     },
})