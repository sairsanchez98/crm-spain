var sair = new Vue({
    el: "#historial",
    data: {
        ///// datos de un agente /////
        usuario: "",
        nombre: "",
        contrasena: "",
        detalle:false,
        detalles:[],
        //Historial agente//
        historial:[],
        asunto:"",
        /// AGENTES EN LA DB:
        agentes: [],
        agente: [], // info de un agente en especifico .. solo cuando es requerido
        permisosAgentes: [],        

        /// COMPONENTES : 
        ListaAgentes: true,
        formRegistrarAgentes: false,
        infoAgente: false,

        /// / clases y estados :
        loaderLeft: "none",

        buscar: ""
        

    }, methods: {
         
        
         //////////// REGISTRAR CLIENTE ///////
         RegistrarAgente: function(action)
         {
             
 
             switch(action)
             {
                 case "form": /// mostrar formulario de registro:
                     sair.HabilitarComponente("form");
                 break;
 
                 case "Procesar" : ///// Control de datos y registro en la base de datos
                    var formdata = new FormData();
                    formdata.append("registrar",true);
                    formdata.append("usuario", sair.usuario);
                    formdata.append("nombre", sair.nombre);
                    formdata.append("contrasena", sair.contrasena);
                    axios.post("Controllers/php/ctrlAgentes.php", formdata)
                    .then(function(response){
                        var datos = response.data;
                        if(datos.respuesta == "ok"){
                            sair.MostrarAgentes(false); 
                            sair.HabilitarComponente("lista");
                            sair.ShowAlerts("registrar_agente", "ok");
                        }else{
                            sair.ShowAlerts("registrar_agente", "campos_vacios");
                        }
                    })
                 break; 
             }
         },


         Buscar: function (){

         },

         ////MOSTRAR AGENTES
         MostrarAgentes: function(id)
         {
             this.loaderLeft = "block";
             if(!id){ //mostrar todos
                 formdata = new FormData();
                 formdata.append("listarTodo", "ok")
                 axios.post("Controllers/php/ctrlAgentes.php", formdata)
                 .then(function(response){
                     //$("#datable_1").dataTable();
                     var datos  = response.data;
                     sair.agentes = datos.agentes;
                     sair.loaderLeft ="none";
                     //console.log(sair.agentes);
                 })
 
             }else{ //mostrar uno en particular
                sair.HabilitarComponente("infoAgente");
                sair.agente.splice(0);// formateamos el array del agente
                for(let index = 0; index < sair.agentes.length; index++){
                    if(sair.agentes[index]['id'] == id){
                       
                        var formdata = new FormData();
                        formdata.append("BuscarHistorialAgente", "ok");
                        formdata.append("id_agente",id)
                        sair.agente.push(
                            {
                                id: sair.agentes[index]['id'],
                                nombre: sair.agentes[index]['nombre'],
                                pass: sair.agentes[index]['pass'],
                                usuario: sair.agentes[index]['usuario'], 
                            }
                        );
                        axios.post("Controllers/php/ctrlHistorial.php", formdata).then((res)=>{
                            console.log(res.data.resultado)
                            if(res.data.resultado){
                                sair.historial = res.data.datos;
                            }
                        })
                     
                    }
                   
                }
                this.loaderLeft = "none";
                //console.log(sair.agente);
             }
         },
         buscarInfo(i){
          var formdata = new FormData();
          formdata.append("id_agente", i.id_agente);
          formdata.append("tabla_afectada", i.tabla_afectada);
          formdata.append("id_afectado", i.id_objeto_afectado)
          formdata.append("fecha", i.fecha_registro)
          formdata.append("bucarInfo", "ok")
          axios.post("Controllers/php/ctrlHistorial.php", formdata).then((res)=>{
              console.log(res.data)
            console.log(res.data.datos)
            if(res.data.resultado =="ok"){
                sair.HabilitarComponente("form");
                sair.detalles = res.data.datos;
                sair.asunto =i.movimiento
            }
        })
         },

        
        /////// HABILITAR O DESHABILITAR COMPONENTES /////
         HabilitarComponente: function(component) 
         {
             switch(component)
             {
                 case "form": // habilitar formulario de registro de agentes
                 sair.detalle = true;
                 
                 sair.infoAgente = false;
                 break;
 
                 case "lista": //habilitar la lista de agentes
                 sair.ListaAgentes = true;
                 sair.formRegistrarAgentes = false;
                 sair.infoAgente = false;
                 sair.detalle = false;
                 break;

                 case "infoAgente": //habilitar la información de un agente en particular
                 //sair.ListaAgentes = false;
                 sair.formRegistrarAgentes = false;
                 sair.infoAgente = true;
                 sair.detalle = false;
                 break;
             }
             
         },


         /////MOSTRAR ALERTAS ///
         ShowAlerts: function(type, status){
             switch(type){
                 case "registrar_agente":
                     switch(status){
                         case "campos_vacios":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Error!',
                                    text: '<p>Llene todos los campos de este formulario.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-danger',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });       
                            break;
                        
                        case "ok":
                                $.toast().reset('all');
                                $("body").removeAttr('class');
                                $.toast({
                                    heading: '¡Ok!',
                                    text: '<p>Se guardó correctamente el agente.</p>',
                                    position: 'bottom-right',
                                    loaderBg:'#ffffff',
                                    class: 'jq-toast-success',
                                    hideAfter: 4500, 
                                    stack: 6,
                                    showHideTransition: 'fade'
                                });          
                            break;
                     }
                 break;
             }
         },


      
  
    



    },

    mounted() {
        this.MostrarAgentes(false); 
    },
})