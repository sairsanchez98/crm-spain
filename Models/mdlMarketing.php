<?php
require_once "conexion.php";


class MdlMarketing{

    static public function FiltroFechasMarketing($fecha_inicial, $fecha_final){
        if(($fecha_final!="" & $fecha_inicial!="") &  $fecha_final!= $fecha_inicial){
            $conn =Conection::conectar()->prepare("SELECT * FROM `marketing`    WHERE DATE(fecha_envio) BETWEEN '$fecha_inicial' AND '$fecha_final' ORDER BY id DESC");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return "ok";
            }
        }else if($fecha_final== $fecha_inicial){

                $conn =Conection::conectar()->prepare("SELECT * FROM `marketing`  WHERE fecha_envio like '%$fecha_final%' ORDER BY id  DESC");
                if($conn->execute()){
                 return $conn->fetchAll(PDO::FETCH_ASSOC);
                 
                }else{
                    return "ok";
                }
             
          
        }
     
       
    }
    static public function FiltroFechasEstadoMarketing($fecha_inicial, $fecha_final, $estado){
        if(($fecha_final!="" & $fecha_inicial!="") &  $fecha_final!= $fecha_inicial){
            $conn =Conection::conectar()->prepare("SELECT * FROM `marketing`  WHERE tipo='$estado' and fecha_envio BETWEEN '$fecha_inicial' AND '$fecha_final' ORDER BY `marketing`. id  DESC");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }else if($fecha_final==$fecha_inicial){
            $conn =Conection::conectar()->prepare("SELECT * FROM `marketing`  WHERE tipo='$estado' and fecha_envio like '%$fecha_final%' ORDER BY `marketing`. fecha_envio  DESC");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }
       
       
    }
    static public function MarktetingFiltroEstado($filtro){
        if($filtro!="todos"){
            $conn =Conection::conectar()->prepare("SELECT * FROM `marketing`  where tipo ='$filtro' ");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }else{
            $conn =Conection::conectar()->prepare("SELECT * FROM `marketing`  ");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }
       
    }

    static public function RegistrosMarketing($id){
      
            if (!$id) {
                $conn = Conection::conectar()->prepare("SELECT * FROM marketing ORDER BY id DESC ");
                $conn -> execute();
                return $conn->fetchAll();
            }
        
    }
    static public function FiltrosMarketing($id){
      
        if (!$id) {
            $conn = Conection::conectar()->prepare("SELECT * FROM tipos_de_filtros ORDER BY id DESC ");
            $conn -> execute();
            return $conn->fetchAll();
        }
    
      
    }
    static public function MensajeFiltroEmail($datos,$mensaje,$fecha,$tipo,$filtros){
        $cantidad =count($datos);
        $stm = Conection::conectar()->prepare("INSERT INTO marketing ( `mensaje`, `tipo`, `catgoria_mensaje`, `alcance`, `fecha_envio`) VALUES
        (:mensaje, :tipo, :categoria, :cantidad, :fecha_envio);");
        
        $stm->bindParam(":mensaje", $mensaje, PDO::PARAM_STR);
        $stm->bindParam(":tipo", $tipo, PDO::PARAM_STR);
        $stm->bindParam(":cantidad", $cantidad, PDO::PARAM_INT);
        $stm->bindParam(":categoria", $filtros, PDO::PARAM_STR);
        $stm->bindParam(":fecha_envio",$fecha , PDO::PARAM_STR);
        if($stm->execute()){
            return true;
        }else{
            return false;
        }
       // $stmt->bindParam(":alcance", 10);
    }
    static public function MensajesEmail($datos,$id_marketing,$asunto,$mensaje,$fecha){
        $id_agente =$_SESSION["user_logged"];
        foreach ($datos as $row){
              
            $stmt = Conection::conectar()->prepare("INSERT INTO email_registro ( id_cliente, id_agente, asunto, contenido, id_envio, fecha_envio) VALUES
                    (:id_cliente, $id_agente , :asunto, :mensaje,$id_marketing, :fecha_envio);");
                      $stmt->bindParam(":id_cliente", $row['id_cliente'], PDO::PARAM_INT);
                      $stmt->bindParam(":asunto", $asunto, PDO::PARAM_STR);
                      $stmt->bindParam(":mensaje", $mensaje, PDO::PARAM_STR);
                      $stmt->bindParam(":fecha_envio",$fecha , PDO::PARAM_STR);
                     $stmt->execute();
          }
          return "ok";
         

    }
    static public function MensajesWhatsapp($datos,$id_marketing,$mensaje,$fecha){
        $id_agente =$_SESSION["user_logged"];
        foreach ($datos as $row){
              
            $stmt = Conection::conectar()->prepare("INSERT INTO whatsapp_registros ( `id_cliente`, `id_agente`, `mensaje`, `id_envio`, `fecha_envio`) VALUES
            (:id_cliente,  $id_agente, :mensaje, $id_marketing, :fecha_envio);");
              $stmt->bindParam(":id_cliente", $row['id_cliente'], PDO::PARAM_INT);
              
              $stmt->bindParam(":mensaje", $mensaje, PDO::PARAM_STR);
              $stmt->bindParam(":fecha_envio", $fecha, PDO::PARAM_STR);
              $stmt->execute();    
                  }
                  return "ok";
    }
    static public function MensajesSms($datos,$id_marketing,$mensaje,$fecha){
        $id_agente =$_SESSION["user_logged"];
        foreach ($datos as $row){
              
            $stmt = Conection::conectar()->prepare("INSERT INTO sms_registros ( `id_cliente`, `id_agente`, `mensaje`, `id_envio`, `fecha_envio`) VALUES
            (:id_cliente, $id_agente, :mensaje, $id_marketing, :fecha_envio);");
              $stmt->bindParam(":id_cliente", $row['id_cliente'], PDO::PARAM_INT);
              
              $stmt->bindParam(":mensaje", $mensaje, PDO::PARAM_STR);
              $stmt->bindParam(":fecha_envio", $fecha, PDO::PARAM_STR);
              $stmt->execute();
          }
          return "ok";

         

    }
}




?>