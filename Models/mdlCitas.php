<?php
require_once("conexion.php");

class MdlCitas{




    static public function MostrarCitas(){
        $conn = Conection::conectar()->prepare("SELECT * FROM citas ORDER BY id DESC ");
        $conn -> execute();
        return $conn->fetchAll();

    }
    static public function CitaUnica($id){
        $conn = Conection::conectar()->prepare("SELECT * FROM citas WHERE id =$id");
        $conn -> execute();
        return $conn->fetchAll();
    }
    
    static public function EditarCita($title,$fecha1, $fecha2,$id_cita, $estado){
        $conn = Conection::conectar()->prepare("UPDATE citas SET title=:title , start_ = :fecha1 , end_= :fecha2, estado= :estado WHERE id=:id_cita");

        $conn->bindParam(":id_cita", $id_cita, PDO::PARAM_INT);
        $conn->bindParam(":title", $title, PDO::PARAM_STR);
        $conn->bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
        $conn->bindParam(":estado", $estado, PDO::PARAM_STR);
        $conn->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
        if($conn -> execute()){
            return true;
        }else{
            return false;
        }
       

    }

    static public function CitasParaHoy($fechaActual){
        $conn = Conection::conectar()->prepare("SELECT * FROM citas where DATE(start_) = '$fechaActual' ORDER BY id DESC ");
        if( $conn -> execute()){
           
        return $conn->fetchAll(PDO::FETCH_ASSOC);
        }else{
            return false;
        }
    }
    static public function CitastomadasHoy($fechaActual){
        $conn = Conection::conectar()->prepare("SELECT * FROM citas where DATE(start_) = '$fechaActual' and estado ='ok' ORDER BY id DESC ");
        if( $conn -> execute()){
           
        return $conn->fetchAll(PDO::FETCH_ASSOC);
        }else{
            return false;
        }
    }


    static public function ProgramarCita($id_cliente,$id_agente,$id_cita,$fecha,$url){
    
      
        $conn = Conection::conectar()->prepare("INSERT INTO `mensajes_programados` (`id`, `id_cliente`, `id_agente`,`id_cita` ,`fecha_envio`, `url`)
         VALUES (NULL, $id_cliente, $id_agente,$id_cita ,'$fecha', '$url')"); 
       
        if($conn -> execute()){

            return "ok";
        }

}
    static public function CrearCita($fecha,$fecha2,$title, $id_cliente){
             $id_agen = $_SESSION["user_logged"];
      
            $conn = Conection::conectar()->prepare("INSERT INTO citas 
            ( id_cliente, id_agente, title, start_ ,  end_, estado) 
            VALUES ( :id_cliente,  :id_agente, :title, :fecha, :fecha2, :estado)"); 
          $estado ="";
          $conn->bindParam(":id_cliente", $id_cliente, PDO::PARAM_INT);
          $conn->bindParam(":id_agente", $id_agen, PDO::PARAM_INT);
          $conn->bindParam(":title", $title, PDO::PARAM_STR);
          $conn->bindParam(":fecha", $fecha, PDO::PARAM_STR);
          $conn->bindParam(":estado", $estado, PDO::PARAM_STR);
          $conn->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
            if($conn->execute()){
                return true;
            }else{
                return false;
            }
        
        /*$conn = Conection::conectar()->prepare("INSERT INTO `citas` (`id`, `id_cliente`, `id_agente`, `asunto`, `fecha`, `hora_cita`, `horas`, `estado`) 
        VALUES ('2', '31', '10', 'dadsdada', '2019-11-05', '4', '4', 'dadsadasdas');");
        $conn -> execute();*/
        //return $conn->fetchAll()
  
    }



    static public function CitasFiltro($estado){
        
        $conn = Conection::conectar()->prepare("SELECT * FROM citas WHERE `estado` ='$estado'");
        if($conn->execute()){
            return $conn->fetchAll(PDO::FETCH_ASSOC);
        }
    }

}



?>