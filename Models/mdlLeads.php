<?php
require_once"conexion.php";
date_default_timezone_set('America/Bogota');

class MdlLeads{
    static public function Leads(){
        if(isset($_POST["leads"])){
           return $_POST["leads"];
        }
        /*//se conecta con la api y la api le manda un dato
        $arr_leads = array('telefono'=> '6767',
        'email'=> 'code@gmail.com');
        $arr_leads2 = array('telefono'=> '6868',
        'email'=> 'codecode@gmail.com');
        $content_leads =array($arr_leads,$arr_leads2);
        
        return $content_leads;*/

    }
    static public function FiltroFechasLeads($fecha_inicial, $fecha_final){
        if(($fecha_final!="" & $fecha_inicial!="") &  $fecha_final!= $fecha_inicial){
            $conn =Conection::conectar()->prepare("SELECT * FROM `lead`  WHERE DATE(fecha)  BETWEEN '$fecha_inicial' AND '$fecha_final' ORDER BY DATE(fecha)  desc");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }else if($fecha_final== $fecha_inicial){

                $conn =Conection::conectar()->prepare("SELECT * FROM `lead`  WHERE fecha like '%$fecha_final%' ORDER BY fecha desc");
                if($conn->execute()){
                 return $conn->fetchAll(PDO::FETCH_ASSOC);
                 
                }else{
                    return false;
                }
             
          
        }
     
       
    }
    
    static public function FiltroFechasEstadoLeads($fecha_inicial, $fecha_final, $estado){
        if(($fecha_final!="" & $fecha_inicial!="") &  $fecha_final!= $fecha_inicial){
            $conn =Conection::conectar()->prepare("SELECT * FROM `lead`  WHERE estado='$estado' and fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }else if($fecha_final==$fecha_inicial){
            $conn =Conection::conectar()->prepare("SELECT * FROM `lead`  WHERE estado='$estado' and fecha like '%$fecha_final%' ORDER BY fecha");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }
       
       
    }
    static public function LeadsFiltroEstado($filtro){
        if($filtro!="todos"){
            $conn =Conection::conectar()->prepare("SELECT * FROM `lead`  where estado ='$filtro' ");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }else{
            $conn =Conection::conectar()->prepare("SELECT * FROM `lead`  ");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }
       
    }
    static public function RegistrarLeads( $id_cliente,$origen,$nombre,$telefono,$email,$fechaActual){
       
       
           //conectarse a clientes y traer los clientes
           $conn = Conection::conectar()->prepare("INSERT INTO 
           lead (id_cliente, nombre_lead, telefono , email , estado, origen , fecha) 
           VALUES (:id_cliente, :nombre_lead, :telefono , :email, :estado ,  :origen , :fecha);");

            $estado = "";            

            $conn->bindParam(":id_cliente", $id_cliente, PDO::PARAM_INT);
            $conn->bindParam(":nombre_lead", $nombre, PDO::PARAM_STR);
            $conn->bindParam(":telefono", $telefono, PDO::PARAM_INT);
            $conn->bindParam(":email", $email, PDO::PARAM_STR);
            $conn->bindParam(":estado", $estado, PDO::PARAM_STR);
            $conn->bindParam(":origen", $origen, PDO::PARAM_STR);
            $conn->bindParam(":fecha", $fechaActual, PDO::PARAM_STR);


            if($conn->execute()){
                return true;
            }else{
                return $conn->execute();
            }
   
            
            //conectar se whatsapp y traer los datos de ese cliente y su historial con whatsapp
    }
    static public function Actualizar($estado,$id){
        $conn =Conection::conectar()->prepare(" UPDATE lead SET estado=:estado WHERE id =$id");
        
        $conn->bindParam(":estado", $estado, PDO::PARAM_STR);
        if($conn->execute()){
            return true;
        }else{
            return false;
        }
    }


    static public function BuscarLeads(){
           $conn =Conection::conectar()->prepare("SELECT * FROM `lead` ORDER BY id DESC ");
           if($conn->execute()){
            return $conn->fetchAll(PDO::FETCH_ASSOC);
            
           }else{
               return false;
           }
    }
    

}


?>