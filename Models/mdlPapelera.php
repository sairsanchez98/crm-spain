<?php
require_once "conexion.php";
class mdlPapelera{


    //// con esta función podremos saber si un objeto se encuentra o no en la papelera
    static public function ConsultarPapelera($id_obj,  $tabla)
    {
        $conn = Conection::conectar()->prepare("SELECT * FROM papelera
        WHERE tabla = '$tabla' AND id_afectado = '$id_obj' ORDER BY id DESC ");
        $conn -> execute();
        return $conn->fetchAll();
    }


    //////////////////////ELIMINAR O MOVER un objeto A LA PAPELERA 
    ///////////////////////7
    static public function MoverAlapapelera($id_obj, $tabla, $fecha)
    {
        /// las notas no se borran como tal .... solo son registrados en la 
		// tabla de la PAPELERA para que se desactiven sus funciones
		$stmt = Conection::conectar()->prepare("INSERT INTO papelera (tabla, id_afectado, fecha_eliminacion) 
		VALUES (:tabla, :id_obj, :fecha)");
		// nos referimos a la tabla de donde proviene el objeto que 
		// se está eliminando (en este caso es una nota)
        $stmt->bindParam(":tabla", $tabla, PDO::PARAM_STR);
        $stmt->bindParam(":fecha", $fecha, PDO::PARAM_STR);
		$stmt->bindParam(":id_obj", $id_obj, PDO::PARAM_INT);


        
        if($stmt->execute() ){
			return true;
		}else{
			return false;
		}
	/*	$stmt->close();
		$stmt = null;      */  
    }


}