<?php

require_once"conexion.php";
//$stmt = Conection::conectar()->prepare("INSERT INTO email_registro (id, id_cliente, id_agente, asunto, contenido, fecha_envio) VALUES (2, 1, 10, 'rwerr', 'werwerwerwer', 2019-10-08);");
//$stmt->execute();
//echo $stmt;
require_once"../../Models/mdlHistorial.php";
class MdlClientes
{
    ///// REGISTRAR NUEVO CLIENTES ////
    static public function RegistrarClicnte($datos)
    {
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        crm_clientes (nombre, dni, email, telefono, direccion, fecha_registro ) 
        VALUES (:nombre, :dni, :email, :telefono, :direccion, :fecha_registro)");

        $stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
        $stmt->bindParam(":dni", $datos["dni"], PDO::PARAM_INT);
        $stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
        $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
        $stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
        $stmt->bindParam(":fecha_registro", $datos["fecha_registro"], PDO::PARAM_STR);

        if($stmt->execute()){

			return true;

		}else{

			return false;
		
		}

		/*$stmt->close();
		
		$stmt = null;   */     
    }


    static public function FiltroFechasClientes($fecha_inicial, $fecha_final){
        if($fecha_final !== $fecha_inicial){
            //SELECT * FROM `crm_clientes` WHERE fecha_registro BETWEEN '2019-12-20' AND '2019-12-24' ORDER BY 'fecha_registro' DESC
            $conn =Conection::conectar()->prepare("SELECT * FROM crm_clientes WHERE DATE(fecha_registro) BETWEEN '$fecha_inicial' AND '$fecha_final' ORDER BY id DESC");
            if($conn->execute()){
             return $conn->fetchAll(PDO::FETCH_ASSOC);
             
            }else{
                return false;
            }
        }else if($fecha_final== $fecha_inicial){

                $conn =Conection::conectar()->prepare("SELECT * FROM `crm_clientes`  WHERE fecha_registro like '%$fecha_final%' ORDER BY id DESC");
                if($conn->execute()){
                 return $conn->fetchAll(PDO::FETCH_ASSOC);
                 
                }else{
                    return false;
                }
             
          
        }
     
       
    }
    
   

 






static public function Eliminar($id){
    $stmt = Conection::conectar()->prepare("DELETE FROM `crm_clientes` WHERE id =$id");
    if($stmt->execute()){

        return true;

    }else{

        return false;
    
    }

   /* $stmt->close();
    
    $stmt = null;  */

}
static public function Actualizar($datos){
    $id=$datos['id'];
    //$nombre= $datos["nombre"];
   
    $stmt = Conection::conectar()->prepare(" UPDATE crm_clientes SET nombre=:nombre,dni=:dni,email=:email,telefono=:telefono,direccion=:direccion,fecha_registro=:fecha_registro WHERE id =$id");

        $stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
        $stmt->bindParam(":dni", $datos["dni"], PDO::PARAM_INT);
        $stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
        $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
        $stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
        $stmt->bindParam(":fecha_registro", $datos["fecha_registro"], PDO::PARAM_STR);
   if($stmt->execute()){

        return true;

    }else{

        return false;
    
    }

    /*$stmt->close();
    
    $stmt = null;*/
}

///registrar correo
static public function RegistrarMensajeCorreo($datos)
{
    //enviar datos email,asunto y mensaje
   //digamos que ya valido la apire

       $repuesta ="ok";
       if($repuesta =="ok"){
           $id_use =  $_SESSION["user_logged"];
          $stmt = Conection::conectar()->prepare("INSERT INTO email_registro ( id_cliente, id_agente, asunto, contenido, fecha_envio) VALUES
           ( :id_cliente, $id_use, :asunto, :mensaje, :fecha_envio);");
       
          $stmt->bindParam(":id_cliente", $datos["id_cliente"], PDO::PARAM_INT);
        //  $stmt->bindParam(":id_agente", 10);
          $stmt->bindParam(":asunto", $datos["asunto"], PDO::PARAM_STR);
          $stmt->bindParam(":mensaje", $datos["mensaje"], PDO::PARAM_STR);
          $stmt->bindParam(":fecha_envio", $datos["fecha_envio"], PDO::PARAM_STR);
   
       if($stmt->execute()){
   
           return true;
   
       }else{
   
           return false;
       
       }
   
      /* $stmt->close();
       
       $stmt = null;  */
       }    
}
static public function MostrarCorreos(){
    $correos = Conection::conectar()->prepare("SELECT * FROM `email_registro`  ORDER BY id DESC ");
    if($correos->execute()){
        return $correos->fetchAll(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
} 
static public function MostrarWhatsapp(){
    $wha= Conection::conectar()->prepare("SELECT * FROM `whatsapp_registros`  ORDER BY id DESC ");
    if($wha->execute()){
        return $wha->fetchAll(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}
static public function MostrarSms(){
    $sms= Conection::conectar()->prepare("SELECT * FROM `sms_registros`  ORDER BY id DESC ");
    if($sms->execute()){
        return $sms->fetchAll(PDO::FETCH_ASSOC);
    }else{
        return false;
    }
}
static public function RegistrarMensajeWhatsapp($datos)
{
     
       
    $id_ =$_SESSION["user_logged"];
    $stmt = Conection::conectar()->prepare("INSERT INTO whatsapp_registros ( id_cliente, id_agente, mensaje, fecha_envio) VALUES
    ( :id_cliente,$id_ , :mensaje,  :fecha_envio);");

    $stmt->bindParam(":id_cliente", $datos["id_cliente"], PDO::PARAM_INT);
    //$stmt->bindParam(":id_agente", 10);
    $stmt->bindParam(":mensaje", $datos["mensaje"], PDO::PARAM_STR);
    // $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
    $stmt->bindParam(":fecha_envio", $datos["fecha_envio"], PDO::PARAM_STR);

    if($stmt->execute()){

        return true;
   
    }    
}
static public function RegistrarMensajeSms($datos)
{
  
       
       
    $id_ =$_SESSION["user_logged"];
          $stmt = Conection::conectar()->prepare("INSERT INTO sms_registros ( id_cliente, id_agente, mensaje, fecha_envio) VALUES
           ( :id_cliente, $id_, :mensaje,  :fecha_envio);");
       
          $stmt->bindParam(":id_cliente", $datos["id_cliente"], PDO::PARAM_INT);
          $stmt->bindParam(":mensaje", $datos["mensaje"], PDO::PARAM_STR);
          $stmt->bindParam(":fecha_envio", $datos["fecha_envio"], PDO::PARAM_STR);
   
    if($stmt->execute()){
           return true;
    }else{
        return false;
    }
   
      
}


  static public function UnicoCliente($datos){

        $id_ =$datos;
        $conn = Conection::conectar()->prepare("SELECT * FROM crm_clientes WHERE id='$id_' ");
        $conn -> execute();
        return $conn->fetchAll(PDO::FETCH_ASSOC);
    
  }
    //////////////LISTAR CLIENTES ///////////////
    static public function MostrarClientes($id)
    {

        if (!$id) {
            $conn = Conection::conectar()->prepare("SELECT * FROM crm_clientes ORDER BY id DESC ");
            if($conn -> execute()){
                return $conn->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
            
        }else if($id["categoria"]=="masivo" && $id["tipo"]=="email"){
            //este es para enviar email a todo los clientes pertenece a marketing


                $conn = Conection::conectar()->prepare("SELECT email, id FROM crm_clientes ORDER BY id DESC ");
                if($conn -> execute()){
                   $mensaje = $conn->fetchAll(PDO::FETCH_ASSOC);
                   $cantidad =count($mensaje);
                   if($cantidad>0){
                    $stm = Conection::conectar()->prepare("INSERT INTO marketing ( `mensaje`, `tipo`, `catgoria_mensaje`, `alcance`, `fecha_envio`) VALUES
                    (:mensaje, :tipo, :categoria, $cantidad, :fecha_envio);");  
                    $stm->bindParam(":mensaje", $id['mensaje'], PDO::PARAM_STR);
                    $stm->bindParam(":tipo", $id["tipo"], PDO::PARAM_STR);
                    $stm->bindParam(":categoria", $id["categoria"], PDO::PARAM_STR);
                    $stm->bindParam(":fecha_envio", $id["fecha_envio"], PDO::PARAM_STR);
                   // $stmt->bindParam(":alcance", 10);
                   $id_user = $_SESSION["user_logged"];
                    if($stm->execute()){
                        $marketing= MdlMarketing::RegistrosMarketing(false);
                        if($marketing){
                            $id_marketin =$marketing[0]["id"];
                            $movimiento  = mdlHistorial::RegistrarMovimiento(
                                $_SESSION["user_logged"],
                                "marketing", 
                                 $marketing[0]["id"], 
                                "Envio un mensaje masivo a " . $marketing[0]["alcance"], 
                                date("Y:m:d h:i:s")
                            );
                          
                            foreach ($mensaje as $row){
                           
                             $stmt = Conection::conectar()->prepare("INSERT INTO email_registro ( id_cliente, id_agente, asunto, contenido,id_envio ,fecha_envio) VALUES
                                     (:id_cliente,  $id_user, :asunto, :mensaje, $id_marketin, :fecha_envio);");
                                       $stmt->bindParam(":id_cliente", $row['id'], PDO::PARAM_INT);
                                       $stmt->bindParam(":asunto", $id["asunto"], PDO::PARAM_STR);
                                       $stmt->bindParam(":mensaje", $id["mensaje"], PDO::PARAM_STR);
                                       $stmt->bindParam(":fecha_envio", $id["fecha_envio"], PDO::PARAM_STR);
                                       if($stmt->execute()){
                                        $movimiento  = mdlHistorial::RegistrarMovimiento2(
                                            $_SESSION["user_logged"],
                                            "crm_clientes", 
                                             $row["id"], 
                                            "recibio un mensaje email",
                                            $id["asunto"], 
                                            date("Y:m:d h:i:s")
                                        );
                                       }
                           }
                           return "ok";
                        }
                     
                    }else{
                        return "Error con insertar en los correos";
                    }

                   }else{
                       return "No hay Usuarios";
                   }
             
              
                  
        
                }else {
                    return "error en busqueda clientes";
                }
            
           
            
   

        
        }else if($id["categoria"]=="masivo" && $id["tipo"]=="whatsapp"){
            
            $conn = Conection::conectar()->prepare("SELECT telefono, id FROM crm_clientes ORDER BY id DESC ");
           if($conn -> execute()){
            $mensaje = $conn->fetchAll(PDO::FETCH_ASSOC);
            $cantidad = count($mensaje);  
              if($cantidad>0){ 

                $id_user = $_SESSION["user_logged"];
                $stm = Conection::conectar()->prepare("INSERT INTO marketing ( `mensaje`, `tipo`, `catgoria_mensaje`, `alcance`, `fecha_envio`) VALUES
                (:mensaje, :tipo, :categoria, $cantidad, :fecha_envio);");
                
                $stm->bindParam(":mensaje", $id['mensaje'], PDO::PARAM_STR);
                $stm->bindParam(":tipo", $id["tipo"], PDO::PARAM_STR);
                $stm->bindParam(":categoria", $id["categoria"], PDO::PARAM_STR);
                $stm->bindParam(":fecha_envio", $id["fecha_envio"], PDO::PARAM_STR);
                // $stmt->bindParam(":alcance", 10);
                if($stm->execute()){
                    
                    $marketing= MdlMarketing::RegistrosMarketing(false);
                    $id_marketing =$marketing[0]["id"];
                    $movimiento  = mdlHistorial::RegistrarMovimiento(
                        $_SESSION["user_logged"],
                        "marketing", 
                         $marketing[0]["id"], 
                        "Envio un mensaje masivo a " . $marketing[0]["alcance"], 
                        date("Y:m:d h:i:s")
                    );
                    foreach ($mensaje as $row){
          
                        $stmt = Conection::conectar()->prepare("INSERT INTO whatsapp_registros ( `id_cliente`, `id_agente`, `mensaje`,`id_envio`, `fecha_envio`) VALUES
                                (:id_cliente,  $id_user, :mensaje, $id_marketing, :fecha_envio);");
                                  $stmt->bindParam(":id_cliente", $row['id'], PDO::PARAM_INT);
                                  
                                  $stmt->bindParam(":mensaje", $id["mensaje"], PDO::PARAM_STR);
                                  $stmt->bindParam(":fecha_envio", $id["fecha_envio"], PDO::PARAM_STR);
                                  if($stmt->execute()){
                                    $movimiento  = mdlHistorial::RegistrarMovimiento2(
                                        $_SESSION["user_logged"],
                                        "crm_clientes", 
                                         $row["id"], 
                                        "recibio un mensaje de whatsapp",
                                        $id["mensaje"], 
                                        date("Y:m:d h:i:s")
                                    );
                                  }
                      }
                      return "ok";
                }else{
                    return "No registro en Marketing";
                }
              
              }else{
                  return "Noa hay Usuarios Para este tipo de filtro";
              }
           }
        
          
      
        }else if($id["categoria"]=="masivo" && $id["tipo"]=="sms"){
            
            $conn = Conection::conectar()->prepare("SELECT telefono, id FROM crm_clientes ORDER BY id DESC ");
            if($conn -> execute()){
                $mensaje = $conn->fetchAll(PDO::FETCH_ASSOC);
                $cantidad =count($mensaje);
                $id_user = $_SESSION["user_logged"];
                if($cantidad>0){
                    $stm = Conection::conectar()->prepare("INSERT INTO marketing ( `mensaje`, `tipo`, `catgoria_mensaje`, `alcance`, `fecha_envio`) VALUES
                    (:mensaje, :tipo, :categoria, $cantidad, :fecha_envio);");
                    
                    $stm->bindParam(":mensaje", $id['mensaje'], PDO::PARAM_STR);
                    $stm->bindParam(":tipo", $id["tipo"], PDO::PARAM_STR);
                    $stm->bindParam(":categoria", $id["categoria"], PDO::PARAM_STR);
                    $stm->bindParam(":fecha_envio", $id["fecha_envio"], PDO::PARAM_STR);
                   // $stmt->bindParam(":alcance", 10);
                    if($stm->execute()){
                        $marketing= MdlMarketing::RegistrosMarketing(false);
                        $id_marketing =$marketing[0]["id"];
                        $movimiento  = mdlHistorial::RegistrarMovimiento(
                            $_SESSION["user_logged"],
                            "marketing", 
                             $marketing[0]["id"], 
                            "Envio un mensaje masivo a " . $marketing[0]["alcance"], 
                            date("Y:m:d h:i:s")
                        );
                        foreach ($mensaje as $row){
                            $stmt = Conection::conectar()->prepare("INSERT INTO sms_registros ( `id_cliente`, `id_agente`, `mensaje`, `id_envio` , `fecha_envio`) VALUES
                                    (:id_cliente, $id_user , :mensaje, $id_marketing, :fecha_envio);");
                                      $stmt->bindParam(":id_cliente", $row['id'], PDO::PARAM_INT);
                                      $stmt->bindParam(":mensaje", $id["mensaje"], PDO::PARAM_STR);
                                      $stmt->bindParam(":fecha_envio", $id["fecha_envio"], PDO::PARAM_STR);
                                      if($stmt->execute()){
                                        $movimiento  = mdlHistorial::RegistrarMovimiento2(
                                            $_SESSION["user_logged"],
                                            "crm_clientes", 
                                             $row["id"], 
                                            "recibio un sms", 
                                            $id["mensaje"],
                                            date("Y:m:d h:i:s")
                                        );
                                      }
                          }
                          return "ok";
                    }else{
                        return "No se pudo registrar en marketing el proceso";
                    }
                    
                }else{
                     return "No hay Cliente para enviar sms, registre clientes para poder eviar mensajes";
                }
            }
          
      
       
        }
    }
}