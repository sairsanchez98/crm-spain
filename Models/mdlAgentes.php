<?php
require_once"conexion.php";

class mdlAgentes{


      //////////////LISTAR AGENTES ///////////////
      static public function MostrarAgentes($fecha)
      {
          if (!$fecha) {
              $conn = Conection::conectar()->prepare("SELECT * FROM usuarios ORDER BY id DESC ");
              $conn -> execute();
              return $conn->fetchAll();
          }
      }
      //MOSTRAR UN AGENTE//
      static public function MostrarunAgente($id)
      {
              $conn = Conection::conectar()->prepare("SELECT * FROM usuarios WHERE id='$id' ");
              if($conn -> execute()){
              return $conn->fetchAll();
              }else{
                return false;
              }
      }
      

     

      //////// REGISTRAR AGENTES /////
      static public function RegistrarAgente($datos)
      {
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        usuarios(usuario, pass, nombre) VALUES (:usuario,  :pass, :nombre)");
        $stmt->bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);
        $stmt->bindParam(":pass", $datos["contrasena"], PDO::PARAM_STR);
        $stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
        if($stmt->execute()){
		  	return true;
        }else{
          return false;
        }
        $stmt->close();
        $stmt = null;        
      }


      ////////// TRAER AGENTES ////////////
      static public function VerPermisos(){
        $conn = Conection::conectar()->prepare("SELECT * FROM permisos ORDER BY id DESC ");
        $conn -> execute();
        return $conn->fetchAll();
      }


      //////////// TRAER PERMISOS DE UN AGENTE /////////7
      static public function verPermisosDelAgente($id_agente, $id_permiso){
        $conn = Conection::conectar()->prepare("SELECT * FROM permisos_usuarios 
        WHERE id_usuario = '$id_agente' AND id_permiso = '$id_permiso' ORDER BY id DESC ");
        $conn -> execute();
        return $conn->fetchAll();
      }


      /////// GUARDAR CAMBIOS ///////////
      static public function ActualizarPermisos($id_agente, $id_permiso, $estado)
      {
        if ($estado == true) // estado true indica ACTIVAR
        {
          $stmt = Conection::conectar()->prepare("INSERT INTO 
          permisos_usuarios(id_usuario, id_permiso) VALUES (:id_usuario,  :id_permiso)");
          $stmt->bindParam(":id_usuario", $id_agente, PDO::PARAM_INT);
          $stmt->bindParam(":id_permiso", $id_permiso, PDO::PARAM_INT);

          if($stmt->execute()){
            return true;
          }else{
            return false;
          }
          $stmt->close();
          $stmt = null;   
        }else { // de lo contrario DESACTIVAR
          $stmt = Conection::conectar()->prepare("DELETE FROM permisos_usuarios 
          WHERE id_usuario = '$id_agente' AND id_permiso = '$id_permiso' ");
          
          
          if($stmt -> execute()){
            return true;
          }else{
            return false;	
          }
          $stmt -> close();
          $stmt = null;
        }
      }






}