<?php

require_once"conexion.php";
class MdlServicios{
    static public function MostrarServicios($id)
    {

        if (!$id) {
            $conn = Conection::conectar()->prepare("SELECT * FROM servicios_contratados ORDER BY id DESC ");
            $conn -> execute();
            return $conn->fetchAll(PDO::FETCH_ASSOC);
        }
    }
    static public function RegistrarServicio($datos){
        if($datos){
            $registrar=Conection::conectar()->prepare("INSERT INTO `servicios_contratados`(`id_cliente`, `id_agente`, `asunto`, `precio`, `fecha_creacion`) 
            VALUES (:id_cliente,:id_agente,:asunto,:precio,:fecha)");
                
                $registrar->bindParam(":id_agente", $datos['id_agente'], PDO::PARAM_INT);
                $registrar->bindParam(":id_cliente", $datos["id_cliente"], PDO::PARAM_INT);
                $registrar->bindParam(":asunto", $datos["servicio"], PDO::PARAM_STR);
                $registrar->bindParam(":fecha", $datos["fecha"], PDO::PARAM_STR);
                $registrar->bindParam(":precio", $datos["precio"], PDO::PARAM_STR_CHAR);
                if($registrar->execute()){
                    return true;
                }else{
                    return false;
                }
        }
    }
}

?>