<?php



require_once"conexion.php";

class mdlHistorial{
  static public function MivimientosSemana($fecha_actual){

	$conn = Conection::conectar()->prepare("SELECT * FROM  `historial_agente` WHERE  DATE(fecha_registro)= :fecha_actual ");
	$conn->bindParam(":fecha_actual", $fecha_actual, PDO::PARAM_STR);
	if($conn -> execute()){
		return $conn->fetchAll(PDO::FETCH_ASSOC);
	}else{
		return false;
	}
  }
static public function HistorialCliente(){
	$conn = Conection::conectar()->prepare("SELECT * FROM  `historial_cliente` ORDER BY fecha_registro DESC ");
		if($conn -> execute()){
			return $conn->fetchAll(PDO::FETCH_ASSOC);
		}else{
			return false;
		}
}
  
    static public function RegistrarMovimiento($id_user, $tabla_afectada, $id_obj_afectado, $movimiento, $fecha_registro)
    { 

		$stmt = Conection::conectar()->prepare("INSERT INTO `historial_agente` ( `id_agente`,  `tabla_afectada`, `id_objeto_afectado`, `movimiento`, `fecha_registro`)
		 VALUES ( :id_user, :tabla_afectada, :id_obj_afectado, :movimiento, :fecha_registro)");
		 $stmt->bindParam(":id_user", $id_user, PDO::PARAM_INT);
		 $stmt->bindParam(":tabla_afectada", $tabla_afectada, PDO::PARAM_STR);
		 $stmt->bindParam(":id_obj_afectado", $id_obj_afectado, PDO::PARAM_INT);
		 $stmt->bindParam(":movimiento", $movimiento, PDO::PARAM_STR);
		 $stmt->bindParam(":fecha_registro", $fecha_registro, PDO::PARAM_STR);
		 
      

		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	/*	$stmt->close();
		$stmt = null;   */
		
	}
	static public function RegistrarMovimiento2($id_user, $tabla_afectada, $id_obj_afectado, $movimiento,$titulo ,$fecha_registro)
    { 
		if($titulo==""){
			$titulo ="sin titulo";
		}

		$stmt = Conection::conectar()->prepare("INSERT INTO `historial_cliente` ( `id_agente`,  `tabla_afectada`, `id_objeto_afectado`, `movimiento`,`titulo` , `fecha_registro`)
		 VALUES ( :id_user, :tabla_afectada, :id_obj_afectado, :movimiento, :titulo, :fecha_registro)");
		 $stmt->bindParam(":id_user", $id_user, PDO::PARAM_INT);
		 $stmt->bindParam(":tabla_afectada", $tabla_afectada, PDO::PARAM_STR);
		 $stmt->bindParam(":id_obj_afectado", $id_obj_afectado, PDO::PARAM_INT);
		 $stmt->bindParam(":movimiento", $movimiento, PDO::PARAM_STR);
		 $stmt->bindParam(":titulo", $titulo, PDO::PARAM_STR);
		 $stmt->bindParam(":fecha_registro", $fecha_registro, PDO::PARAM_STR);
		 
      

		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	/*	$stmt->close();
		$stmt = null;   */
		
	}

   static public function HistorialDeUnagentepordia($id, $fecha_actual, $tabla_afectada, $movimiento ){
	$stmt = Conection::conectar()->prepare("SELECT * FROM `historial_agente` where id_agente=:id and DATE(fecha_registro)= DATE(:fecha) and tabla_afectada =:tabla_afectada and movimiento=:movimiento ORDER BY id DESC ");
         $stmt->bindParam(":id", $id, PDO::PARAM_INT);
		 $stmt->bindParam(":fecha", $fecha_actual, PDO::PARAM_STR);
		 $stmt->bindParam(":tabla_afectada", $tabla_afectada, PDO::PARAM_STR);
		 $stmt->bindParam(":movimiento", $movimiento, PDO::PARAM_STR);
		
	if($stmt -> execute()){
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}else{
		return false;
	}
   }



	static public function HistorialAgente(){
		$conn = Conection::conectar()->prepare("SELECT * FROM  `historial_agente` ORDER BY id DESC ");
		if($conn -> execute()){
			return $conn->fetchAll(PDO::FETCH_ASSOC);
		}else{
			return false;
		}
	}
	static public function BuscarInfo($tabla_nombre, $id_agente, $id_afectado, $fecha_)
	{
		if($tabla_nombre =="crm_clientes"){
			$conn = Conection::conectar()->prepare("SELECT * FROM  `$tabla_nombre` WHERE id=$id_afectado ");
			if($conn -> execute()){
				$conn =$conn->fetchAll(PDO::FETCH_ASSOC);
				$arreglo =array("estado"=>"","titulo"=>"", "mensaje"=>"", "fecha"=>$fecha_, "cliente"=>$conn[0]["nombre"], "telefono"=>$conn[0]["telefono"], "email"=>"" );
				return $arreglo;
			}else{
				return false;
			}
		}else
			if($tabla_nombre== "citas"){
			
				$conn = Conection::conectar()->prepare("SELECT * FROM  `$tabla_nombre` WHERE id_agente=$id_agente and id=$id_afectado ");
				
				if($conn -> execute()){
					
					$datos = $conn->fetchAll(PDO::FETCH_ASSOC);
					$id_=$datos[0]["id_cliente"];
					$cliente  = Conection::conectar()->prepare("SELECT * FROM  `crm_clientes` WHERE  id=$id_");
					if($cliente->execute()){
						$cl =$cliente->fetchAll(PDO::FETCH_ASSOC);
						$arreglo =array("estado"=>"","titulo"=>$datos[0]["title"], "mensaje"=>"", "fecha"=>$fecha_, "cliente"=>$cl[0]["nombre"], "telefono"=>$cl[0]["telefono"] , "email"=>"");
					  return $arreglo;
					}
				}else{
					return false;
				}
			
		}else if($tabla_nombre =="sms_registros"){
			$conn = Conection::conectar()->prepare("SELECT * FROM  `$tabla_nombre` WHERE id_agente=$id_agente and id=$id_afectado ");
				
			if($conn -> execute()){
				
				$datos = $conn->fetchAll(PDO::FETCH_ASSOC);
				$id_=$datos[0]["id_cliente"];
				$cliente  = Conection::conectar()->prepare("SELECT * FROM  `crm_clientes` WHERE  id=$id_");
				if($cliente->execute()){
					$cl =$cliente->fetchAll(PDO::FETCH_ASSOC);
					$arreglo =array("estado"=>"","titulo"=>"", "mensaje"=>$datos[0]["mensaje"], "fecha"=>$fecha_, "cliente"=>$cl[0]["nombre"], "telefono"=>$cl[0]["telefono"], "email"=>"" );
				  return $arreglo;
				}
			}else{
				return false;
			}
		}else if($tabla_nombre =="whatsapp_registros"){
			$conn = Conection::conectar()->prepare("SELECT * FROM  `$tabla_nombre` WHERE id_agente=$id_agente and id=$id_afectado ");
				
			if($conn -> execute()){
				
				$datos = $conn->fetchAll(PDO::FETCH_ASSOC);
				$id_=$datos[0]["id_cliente"];
				$cliente  = Conection::conectar()->prepare("SELECT * FROM  `crm_clientes` WHERE  id=$id_");
				if($cliente->execute()){
					$cl =$cliente->fetchAll(PDO::FETCH_ASSOC);
					$arreglo =array("estado"=>"","titulo"=>"", "mensaje"=>$datos[0]["mensaje"], "fecha"=>$fecha_, "cliente"=>$cl[0]["nombre"], "telefono"=>$cl[0]["telefono"], "email"=>"" );
				  return $arreglo;
				}
			}else{
				return false;
			}
		}else if($tabla_nombre =="email_registro"){
			$conn = Conection::conectar()->prepare("SELECT * FROM  `$tabla_nombre` WHERE id_agente=$id_agente and id=$id_afectado ");

			if($conn -> execute()){
				
				$datos = $conn->fetchAll(PDO::FETCH_ASSOC);
				$id_=$datos[0]["id_cliente"];
				$cliente  = Conection::conectar()->prepare("SELECT * FROM  `crm_clientes` WHERE  id=$id_");
				if($cliente->execute()){
					$cl =$cliente->fetchAll(PDO::FETCH_ASSOC);
					$arreglo =array("estado"=>"","titulo"=>$datos[0]["asunto"], "mensaje"=>$datos[0]["contenido"], "fecha"=>$fecha_, "cliente"=>$cl[0]["nombre"], "telefono"=>"", "email"=>$cl[0]["email"] );
				  return $arreglo;
				}
			}else{
				return false;
			}
		}else if($tabla_nombre =="marketing"){
			$conn = Conection::conectar()->prepare("SELECT * FROM  `$tabla_nombre` WHERE  id=$id_afectado ");
				
			if($conn -> execute()){
				
				$datos = $conn->fetchAll(PDO::FETCH_ASSOC);
				$arreglo =array( "estado"=>"","titulo"=>$datos[0]["tipo"], "mensaje"=>$datos[0]["mensaje"], "fecha"=>$fecha_, "cliente"=>"", "telefono"=>"", "email"=>"" );
				  return $arreglo;
			}else{
				return false;
			}
		}else if($tabla_nombre =="lead"){
			$conn = Conection::conectar()->prepare("SELECT * FROM  `$tabla_nombre` WHERE id_agente=$id_agente and id=$id_afectado ");
				
			if($conn -> execute()){
				
				$datos = $conn->fetchAll(PDO::FETCH_ASSOC);
				$id_=$datos[0]["id_cliente"];
				$cliente  = Conection::conectar()->prepare("SELECT * FROM  `crm_clientes` WHERE  id=$id_");
				if($cliente->execute()){
					$cl =$cliente->fetchAll(PDO::FETCH_ASSOC);
					$arreglo =array( "estado"=>$datos[0]["estado"], "titulo"=>"", "mensaje"=>"", "fecha"=>$fecha_, "cliente"=>$cl[0]["nombre"], "telefono"=>"", "email"=>$cl[0]["email"] );
				  return $arreglo;
				}
			}else{
				return false;
			}
		}
	

	}
}

?>